$(function () {
  jQuery.validator.addMethod("noSpace", function(value, element) { 
    return value.indexOf(" ") < 0; 
  }, "Tidak boleh ada spasi!");
  $.validator.addMethod("lowercasesymbols", function(value, element) {
  return this.optional(element) || !/[A-Z]/.test(value); 
}, 'Jangan menggunakan huruf kapital!');
  $("#news-category-form").validate({
    rules: {
      description: {
        required: true,
      },
      name: {
        noSpace: true,
        lowercasesymbols: true,
      }
    },
    messages: {
      sort: {
        required: "Sort wajib diisi",
      },
       name: {
         required: "Name wajib diisi",
       },
      description: "Deskripsi wajib diisi",
      img_active: "Belum memilih gambar aktif",
      img_nonactive: "Belum memilih gambar nonaktif",
    },
    errorElement: "span",
    errorPlacement: function (error, element) {
      error.addClass("invalid-feedback");
      element.closest(".form-group").append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass("is-invalid");
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass("is-invalid");
    },
  });
});
