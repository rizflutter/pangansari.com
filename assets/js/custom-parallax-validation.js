$(function () {
    $("#parallax-form").validate({
      rules: {
        title: {
          required: true,
        },
        description: {
          required: true,
        },
        start_date: {
          required: true,
        },
        finish_date: {
          required: true,
        },
        link: {
          required: true,
          url: true,
        },
      },
      messages: {
        title: {
          required: "Title wajib diisi",
        },
        description: "Deskripsi wajib diisi",
        start_date: "Start Date wajib diisi",
        finish_date: "Finish Date wajib diisi",
        img: "Img wajib dipilih",
        link: {
          required: "Link wajib diisi",
          url: "Masukkan link yang valid",
        },
      },
      errorElement: "span",
      errorPlacement: function (error, element) {
        error.addClass("invalid-feedback");
        element.closest(".form-group").append(error);
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass("is-invalid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass("is-invalid");
      },
    });
  });
  