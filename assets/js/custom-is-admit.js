function is_admit(id, val) {
  $.ajax({
    type: "POST",
    url: base_url + uri + "/is_admit_update",
    data: {
      id: id,
      is_admit: val,
    },
    dataType: "json",
    beforeSend: function (e) {
      if (e && e.overrideMimeType) {
        e.overrideMimeType("application/json;charset=UTF-8"); // mekanisme pengiriman yang digunakan json
      }
    },
    success: function (response) {
        if(response.status){
             alert("Status approve berhasil diubah");
             reload_table();
        }
    },
    error: function (xhr, ajaxOptions, thrownError) {
      console.log(xhr.status + "\n" + xhr.responseText + "\n" + thrownError);
    },
  });
}
