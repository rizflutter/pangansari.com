$(function () {
  $("#about-us-form").validate({
    rules: {
      title1: {
        required: true,
      },
      description: {
        required: true,
      },
    },
    messages: {
      title1: {
        required: "Judul 1 wajib diisi",
      },
      description: {
        required: "Deskripsi wajib diisi",
      },
    },
    errorElement: "span",
    errorPlacement: function (error, element) {
      error.addClass("invalid-feedback");
      element.closest(".form-group").append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass("is-invalid");
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass("is-invalid");
    },
  });
});
