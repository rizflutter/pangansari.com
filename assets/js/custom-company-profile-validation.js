$(function () {
    $("#company-profile-form").validate({
      rules: {
        document_name: {
          required: true,
        },
      },
      messages: {
        document_name: {
          required: "Nama dokumen wajib diisi",
        },
        document_file: {
          required: "File dokumen wajib diupload",
        },
      },
      errorElement: "span",
      errorPlacement: function (error, element) {
        error.addClass("invalid-feedback");
        element.closest(".form-group").append(error);
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass("is-invalid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass("is-invalid");
      },
    });
  });
  