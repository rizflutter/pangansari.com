/* --------------------------------------

=========================================

Pixel | Agency - Responsive Mutipurpose HTML5 Template

Version: 1.0 (Initial Release)

Designed By: rkwebdesigns

=========================================

*/



(function ($) {

	"use strict";



    jQuery(document).ready(function($){

	    

		//carousel active

        $(".carousel-inner .item:first-child").addClass("active");

		

		//Fixed nav on scroll

		$(document).scroll(function(e){

			var scrollTop = $(document).scrollTop();

			if(scrollTop > $('nav').height()){

				//console.log(scrollTop);

				$('nav').addClass('navbar-fixed-top');

			}

			else {

				$('nav').removeClass('navbar-fixed-top');

			}

		});

			

		//Portfolio Popup

		$('.magnific-popup').magnificPopup({type:'image'});

		

		

		

			

    });



	



    jQuery(window).load(function(){

		

		//Numaric Counter

		$('.counter').counterUp({

          delay: 10,

          time: 1000

        });		

				

		//Portfolio container			

		var $container = $('.portfolioContainer');

		$container.isotope({

			filter: '*',

			animationOptions: {

				duration: 750,

				easing: 'linear',

				queue: false

			}

		});

 

		$('.portfolioFilter a').on('click', function(){

			$('.portfolioFilter a').removeClass('current');

			$(this).addClass('current');

	 

			var selector = $(this).attr('data-filter');

			$container.isotope({

				filter: selector,

				animationOptions: {

					duration: 750,

					easing: 'linear',

					queue: false

				}

			 });

			 return false;

		}); 



	});

	function jQueryTabs2() {
	    $(".tabs2").each(function() {
	        $(".tabs-panel2").not(":first").hide(), $("li", this).removeClass("active"), $("li:first-child", this).addClass("active"), $(".tabs-panel:first-child").show(), $("li", this).click(function(t) {
	            var i = $("a", this).attr("href");
	            $(this).siblings().removeClass("active"), $(this).addClass("active"), $(i).siblings().hide(), $(i).fadeIn(400), t.preventDefault()
	        }), $(window).width() < 100 && $(".tabs-panel2").show()
	    })
	}
	$(document).ready(function() {
	    jQueryTabs2(), $(".tabs2 li a").each(function() {
	        var t = $(this).attr("href"),
	            i = $(this).html();
	        $(t + " .tab-title2").prepend("<p><strong>" + i + "</strong></p>")
	    })
	}), $(window).resize(function() {
	    jQueryTabs2()
	});

	
	// Accordion
	
	(function() {

		var $container = $('.acc-container'),
			$trigger   = $('.acc-trigger');

		$container.hide();
		$trigger.first().addClass('active').next().show();

		var fullWidth = $container.outerWidth(true);
		$trigger.css('width', fullWidth);
		$container.css('width', fullWidth);
		
		$trigger.on('click', function(e) {
			if( $(this).next().is(':hidden') ) {
				$trigger.removeClass('active').next().slideUp(300);
				$(this).toggleClass('active').next().slideDown(300);
			}
			e.preventDefault();
		});

		// Resize
		$(window).on('resize', function() {
			fullWidth = $container.outerWidth(true)
			$trigger.css('width', $trigger.parent().width() );
			$container.css('width', $container.parent().width() );
		});

	})();

	

	//Wow js

	new WOW().init();

	 

}(jQuery));	