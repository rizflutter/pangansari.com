$("#bus_cat_id_sh").change(function() {
	$.ajax({
		type: "POST",
		url: base_url + 'slider_home/info_sort',
		data: {
			id: $("#bus_cat_id_sh").val(),
		},
		dataType: "json",
		beforeSend: function(e) {
			if (e && e.overrideMimeType) {
				e.overrideMimeType("application/json;charset=UTF-8"); // mekanisme pengiriman yang digunakan json
			}
		},
		success: function(response) {
			$("#sort_bus_cat_sh").html(response).show();
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert('Data masih kosong, silahkan gunakan nomer urut 1 !');
			// console.log(xhr.status + "\n" + xhr.responseText + "\n" + thrownError);
		},
	});
});
