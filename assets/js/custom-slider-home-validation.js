$(function () {
  $("#slider-home-form").validate({
    rules: {
      index: {
        required: true,
      },
      business_category_id: {
        required: true,
      },
      sort: {
        required: true,
      },
      title1: {
        required: true,
      },
      description: {
        required: true,
      },
      start_date: {
        required: true,
      },
      finish_date: {
        required: true,
      },
    },
    messages: {
      index: {
        required: "Index wajib diisi",
      },
      business_category_id: {
        required: "Business Category wajib dipilih",
      },
      sort: "Sort wajib diisi",
      title1: "Judul 1 wajib diisi",
      description: "Deskripsi wajib diisi",
      start_date: "Start Date wajib diisi",
      finish_date: "Finish Date wajib diisi",
    },
    errorElement: "span",
    errorPlacement: function (error, element) {
      error.addClass("invalid-feedback");
      element.closest(".form-group").append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass("is-invalid");
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass("is-invalid");
    },
  });
});
