var table;
$(document).ready(function () {
  //datatables
  table = $("#example1").DataTable({
    responsive: true,
    lengthChange: true,
    autoWidth: false,
    lengthMenu: [
      [10, 25, 50, -1],
      [10, 25, 50, "All"],
    ],
    pageLength: 10,
    processing: true, //Feature control the processing indicator.
    serverSide: true, //Feature control DataTables' server-side processing mode.
    order: [], //Initial no order.

    // Load data for the table's content from an Ajax source
    ajax: {
      url: base_url + uri + "/Ajax_List",
      type: "POST",
    },

    //Set column definition initialisation properties.
    columnDefs: [
      {
        targets: [-1], //last column
        orderable: false, //set not orderable
      },
    ],
  });
});

function reload_table() {
  table.ajax.reload(null, false); //reload datatable ajax
}
