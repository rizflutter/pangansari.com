$(function () {
  $("#career-backend-form").validate({
    rules: {
      title: {
        required: true,
      },
      link: {
        required: true,
        url: true,
      },
      start_date: {
        required: true,
      },
      finish_date: {
        required: true,
      },
    },
    messages: {
      title: {
        required: "Title wajib diisi",
      },
      link: {
        required: "Link wajib diisi",
        url: "Masukkan link yang valid",
        start_date: "Start Date wajib diisi",
        finish_date: "Finish Date wajib diisi",
      },
    },
    errorElement: "span",
    errorPlacement: function (error, element) {
      error.addClass("invalid-feedback");
      element.closest(".form-group").append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass("is-invalid");
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass("is-invalid");
    },
  });
});
