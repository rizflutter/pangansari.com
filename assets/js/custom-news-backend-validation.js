$(function () {
  //add this custom methid
  $.validator.addMethod(
    "alphanumeric",
    function (value, element) {
      //test user value with the regex
      return this.optional(element) || /^[\w ]+$/i.test(value);
    },
    "Letters, numbers only please"
  );
  $("#news-backend-form").validate({
    rules: {
      title: {
        alphanumeric: true,
      },
    },
    messages: {
      title: {
        required: "Judul wajib diisi",
        alphanumeric: "Hanya boleh huruf A-z dan angka 0-9",
      },
    },
    errorElement: "span",
    errorPlacement: function (error, element) {
      error.addClass("invalid-feedback");
      element.closest(".form-group").append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass("is-invalid");
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass("is-invalid");
    },
  });
});
