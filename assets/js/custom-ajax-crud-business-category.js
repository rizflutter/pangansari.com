var save_method; //for save method string

function add_bc() {
  save_method = "add";
  $("#business-category-form")[0].reset(); // reset form on modals
  $(".form-group").removeClass("has-error"); // clear error class
  $(".help-block").empty(); // clear error string
  $("#modal-business-category").modal("show"); // show bootstrap modal
  $(".modal-title").text("Tambah Business Category"); // Set Title to Bootstrap modal title
}

function edit_bc(id) {
  save_method = "update";
  $("#business-category-form")[0].reset(); // reset form on modals
  $(".form-group").removeClass("has-error"); // clear error class
  $(".help-block").empty(); // clear error string

  //Ajax Load data from ajax
  $.ajax({
    url: base_url + "Business_Category/ajax_edit/" + id,
    type: "GET",
    dataType: "JSON",
    success: function (data) {
      $('[name="id"]').val(data.id);
      $('[name="name"]').val(data.name);
      $('[name="description"]').val(data.description);
      $('[name="link"]').val(data.link);
      $("#modal-business-category").modal("show"); // show bootstrap modal when complete loaded
      $(".modal-title").text("Edit Business Category"); // Set title to Bootstrap modal title
    },
    error: function (jqXHR, textStatus, errorThrown) {
      // console.log(jqXHR);
      alert("Error get data from ajax business category");
    },
  });
}

function save() {
  $("#btnSave").text("saving..."); //change button text
  $("#btnSave").attr("disabled", true); //set button disable
  var url;

  if (save_method == "add") {
    url = base_url + "Business_Category/ajax_add";
  } else {
    url = base_url + "Business_Category/ajax_update";
  }
  $.validator.setDefaults({
    submitHandler: function () {
      var form = $("#business-category-form")[0];
      var formData = new FormData(form);
      console.log(form);
      $.ajax({
        url: url,
        type: "POST",
        data: formData, //this is formData
        processData: false,
        contentType: false,
        cache: false,
        async: false,
        success: function (data) {
          clean1 = data.replace(/"/g, "");
          clean2 = clean1.replace(/\\/g, "");
          if (clean2.includes("<p>")) {
            $("#img-error").html(clean2).show();
          } else {
            //if success close modal and reload ajax table
            alert("Data berhasil ditambah / diupdate");
            $("#modal-business-category").modal("hide");
            // reload_table();
            $("#btnSave").text("save"); //change button text
            $("#btnSave").attr("disabled", false); //set button enable
            location.reload();
          }
        },
        error: function (jqXHR, textStatus, errorThrown) {
          alert("Error adding / update data");
          $("#btnSave").text("save"); //change button text
          $("#btnSave").attr("disabled", false); //set button enable
        },
      });
    },
  });

  $("#business-category-form").validate({
    rules: {
      name: {
        required: true,
      },
      description: {
        required: true,
      },
      link: {
        required: true,
      },
    },
    messages: {
      name: {
        required: "Nama business category wajib diisi",
      },
      description: {
        required: "Deskripsi wajib diisi",
      },
      link: {
        required: "Link wajib diisi",
      },
    },
    errorElement: "span",
    errorPlacement: function (error, element) {
      error.addClass("invalid-feedback");
      element.closest(".form-group").append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass("is-invalid");
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass("is-invalid");
    },
  });
}

// function delete_bc(id, name) {
//   if (confirm(`Anda yakin ingin menghapus ${name}?`)) {
//     // ajax delete data to database
//     $.ajax({
//       url: base_url + "business_category/ajax_delete/" + id,
//       type: "POST",
//       dataType: "JSON",
//       success: function (data) {
//         alert("Data berhasil dihapus");
//         //if success reload ajax table
//         $("#modal-business-category").modal("hide");
//         // reload_table();
//         location.reload();
//       },
//       error: function (jqXHR, textStatus, errorThrown) {
//         alert("Error deleting data");
//       },
//     });
//   }
// }
