<?php
defined('BASEPATH') or exit('No direct script access allowed');

class News_Category extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_news_category', 'news_category');
        $this->load->library(['form_validation', 'ion_auth']);
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('flash', 'deny');
            redirect('auth/login', 'refresh');
        }
    }

    // reuseable group
    private function _groups()
    {
        return $groups = ['admin', 'managers'];
    }

    public function index()
    {
        $data['groups'] = $this->_groups();
        $data['pg_title'] = 'News Category';
        $data['pg_menu'] = 'News';
        $this->template->load('Admin', 'news_category/index', $data);
    }

    public function ajax_list()
    {
        $list = $this->news_category->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $nc) {
            $no++;
            $row = array();
            $row[] = $nc->sort;
            $row[] = $nc->name;
            $row[] = $nc->description;
            $row[] = $nc->link;
            $row[] = '<img src="' . base_url($nc->img_active) . '" alt="" height="50">';
            $row[] = '<img src="' . base_url($nc->img_nonactive) . '" alt="" height="50">';
            $row[] = $nc->full_name;
            $row[] = ($nc->is_active == 1) ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Disable</span>';
            $row[] = ($nc->is_admit == 1) ? '<span class="badge badge-success">Approved</span>' : '<span class="badge badge-warning">Waiting</span>';
            $row[] = $nc->created_at;
            if ($this->ion_auth->in_group($this->_groups())) :
            $row[] = ($nc->is_admit == 1) ? "<input type='checkbox' onclick='is_admit($nc->id".','."0)' checked>" : "<input type='checkbox' onclick='is_admit($nc->id".','."1)'>";
            endif;
            //add html for action
            $row[] = '<div class="btn-group"><a class="btn btn-sm btn-warning" href="' . base_url('News_Category/edit/' . $nc->id) . '">Edit</a>
                  <a class="btn btn-sm btn-danger" href="' . base_url('News_Category/delete/' . $nc->id) . '" onclick="return confirm(' . "'Anda yakin ingin menghapus " . $nc->name . "?'" . ')">Delete</a></div>';
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->news_category->count_all(),
            "recordsFiltered" => $this->news_category->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function add()
    {
        $data['groups'] = $this->_groups();
        $data['sort'] = $this->news_category->get_all('news_category');
        $data['pg_menu'] = 'Home';
        $data['pg_title'] = 'News Category';
        $this->template->load('Admin', 'news_category/add', $data);
    }

    public function add_act()
    {
        $sort = $this->input->post('sort');
        $name = $this->input->post('name');
        $description = $this->input->post('description');
        $is_active = $this->input->post('is_active');

        // $is_admit = $this->input->post('is_admit');
        // if ($is_admit == NULL) {
        //     $is_admit = 0;
        // }


        $this->form_validation->set_rules('sort', 'Sort', 'required|is_unique[news_category.sort]', array(
            'required' => '%s wajib diisi',
            'is_unique' => '%s sudah ada, silahkan gunakan yang lain!',
        ));

        $this->form_validation->set_rules('name', 'Name', 'required|is_unique[news_category.name]', array(
            'required' => '%s wajib diisi',
            'is_unique' => '%s sudah ada, silahkan gunakan yang lain!',
        ));

        $this->form_validation->set_rules('description', 'Description', 'required', array(
            'required' => '%s wajib diisi',
        ));

        if ($this->form_validation->run() != FALSE) {

            $upload_image_active = $_FILES['img_active']['name'];
            $upload_image_nonactive = $_FILES['img_nonactive']['name'];

            $file_active['result'] = '';
            $file_nonactive['result'] = '';

            if ($upload_image_active) {

                $file_active = $this->news_category->upload_active();

                if ($file_active['result'] == "failed") {
                    $this->session->set_flashdata('file_active_result', $file_active['error']);
                    $data['img_active'] = 'assets/images/news/default_nc_active.png';
                } else {

                    $data['img_active'] = 'assets/images/news/' . $file_active['file']['file_name'];
                }
            }

            if ($upload_image_nonactive) {

                $file_nonactive = $this->news_category->upload_nonactive();
                if ($file_nonactive['result'] == "failed") {
                    $this->session->set_flashdata('file_nonactive_result', $file_nonactive['error']);
                    $data['img_nonactive'] = 'assets/images/news/default_nc_nonactive';
                } else {
                    $data['img_nonactive'] = 'assets/images/news/' . $file_nonactive['file']['file_name'];
                }
            }

            $data['sort'] = $sort;
            $data['name'] = $name;
            $data['description'] = $description;
            $data['link'] = "news?cat=" . strtolower($name);
            $data['post_by'] = $this->ion_auth->get_user_id();
            $data['is_active'] = $is_active;
            // $data['is_admit'] = $is_admit;
            $data['created_at'] = date("Y-m-d H:i:s");

            $this->news_category->save($data);
            $this->session->set_flashdata('flash', 'add_success');
            redirect('News_Category/add');
        } else {
            $data['groups'] = $this->_groups();
            $data['sort'] = $this->news_category->get_all('news_category');
            $data['pg_menu'] = 'Home';
            $data['pg_title'] = 'News Category';
            $this->template->load('Admin', 'news_category/add', $data);
        }
    }

    public function edit($id)
    {
        $data['groups'] = $this->_groups();
        $data['news_category_row'] = $this->news_category->get_by_id($id);
        $data['sort'] = $this->news_category->get_all('news_category');
        $data['pg_menu'] = 'Home';
        $data['pg_title'] = 'News Category';
        $this->template->load('Admin', 'news_category/edit', $data);
    }

    public function edit_act()
    {
        $id = $this->input->post('id');
        $where = array(
            'id' => $id
        );
        $news_category_row = $this->news_category->get_by_id($id);
        $sort = $this->input->post('sort');
        $name = $this->input->post('name');
        $description = $this->input->post('description');
        $is_active = $this->input->post('is_active');
        // $is_admit = $this->input->post('is_admit');
        // if ($is_admit == NULL) {
        //     $is_admit = 0;
        // }

        $this->form_validation->set_rules('sort', 'Sort', 'is_unique[news_category.sort]', array(
            'required' => '%s wajib diisi',
            'is_unique' => '%s sudah ada, silahkan gunakan yang lain!',
        ));

        $this->form_validation->set_rules('name', 'Name', 'is_unique[news_category.name]', array(
            'required' => '%s wajib dipilih',
            'is_unique' => '%s sudah ada, silahkan gunakan yang lain!',
        ));

        $this->form_validation->set_rules('description', 'Description', 'required', array(
            'required' => '%s wajib diisi',
        ));

        if ($this->form_validation->run() != FALSE) {

            $upload_image_active = $_FILES['img_active']['name'];
            $upload_image_nonactive = $_FILES['img_nonactive']['name'];

            $file_active['result'] = '';
            $file_nonactive['result'] = '';

            if ($upload_image_active) {

                $file_active = $this->news_category->upload_active();

                if ($file_active['result'] == "failed") {
                    $this->session->set_flashdata('file_active_result', $file_active['error']);
                } else {
                    $old_image_active = $news_category_row->img_active;
                    unlink(FCPATH . $old_image_active);
                    $data['img_active'] = 'assets/images/news/' . $file_active['file']['file_name'];
                }
            }

            if ($upload_image_nonactive) {

                $file_nonactive = $this->news_category->upload_nonactive();
                if ($file_nonactive['result'] == "failed") {
                    $this->session->set_flashdata('file_nonactive_result', $file_nonactive['error']);
                } else {
                    $old_image_nonactive = $news_category_row->img_nonactive;
                    unlink(FCPATH . $old_image_nonactive);
                    $data['img_nonactive'] = 'assets/images/news/' . $file_nonactive['file']['file_name'];
                }
            }

            $data['description'] = $description;
            $data['post_by'] = $this->ion_auth->get_user_id();
            $data['is_active'] = $is_active;
            // $data['is_admit'] = $is_admit;

            if (!empty($sort)) {
                $data['sort'] = $sort;
            }

            if (!empty($name)) {
                $data['name'] = $name;
                $data['link'] = "news?cat=" . strtolower($name);
            }

            $this->news_category->update($where, $data);
            $this->session->set_flashdata('flash', 'edit_success');
            redirect('News_Category/edit/' . $id);
        } else {
            if (empty($id)) {
                $this->session->set_flashdata('flash', 'edit_failed');
                redirect('News_Category');
            }
            $data['groups'] = $this->_groups();
            $data['news_category_row'] = $this->news_category->get_by_id($id);
            $data['sort'] = $this->news_category->get_all('news_category');
            $data['pg_menu'] = 'Home';
            $data['pg_title'] = 'News Category';
            $this->template->load('Admin', 'news_category/edit', $data);
        }
    }

    function delete($id)
    {
        $image = $this->news_category->get_by_id($id);
        $hapus_active = $image->img_active;
        $hapus_nonactive = $image->img_nonactive;

        if (file_exists(FCPATH . $hapus_active)) {
            if ($hapus_active != 'assets/images/news/default_nc_active.png') {
                unlink(FCPATH . $hapus_active);
            }
        }
        if (file_exists(FCPATH . $hapus_nonactive)) {
            if ($hapus_nonactive != 'assets/images/news/default_nc_nonactive.png') {
                unlink(FCPATH . $hapus_nonactive);
            }
        }

        $this->news_category->delete_by_id($id);
        $this->session->set_flashdata('flash', 'delete_success');
        redirect('News_Category');
    }

    function is_admit_update()
    {
        $id = $this->input->post('id');
        $data = $this->input->post('is_admit');

        $data = array(
            'is_admit' => $data,
        );

        if($this->news_category->update(array('id' => $id), $data)){

            echo json_encode(array("status" => TRUE));
        }
    
    }
}
