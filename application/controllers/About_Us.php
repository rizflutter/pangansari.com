<?php
defined('BASEPATH') or exit('No direct script access allowed');

class About_Us extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_about_us', 'about_us');
        $this->load->library(['form_validation', 'ion_auth']);
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('flash', 'deny');
            redirect('auth/login', 'refresh');
        }
    }

    // reuseable group
    private function _groups()
    {
        return $groups = ['admin', 'managers'];
    }

    public function index()
    {
        $data['groups'] = $this->_groups();
        $data['pg_menu'] = 'Home';
        $data['pg_title'] = 'About Us';
        $this->template->load('Admin', 'about_us/index', $data);
    }

    public function ajax_list()
    {
        $list = $this->about_us->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $au) {
            $no++;
            $row = array();
            $row[] = $au->title1;
            $row[] = substr($au->description, 0, 25) . '...';
            $row[] = $au->full_name;
            $row[] = ($au->is_active == 1) ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Disable</span>';
            $row[] = ($au->is_admit == 1) ? '<span class="badge badge-success">Approved</span>' : '<span class="badge badge-warning">Waiting</span>';
            $row[] = ($au->is_posting == 1) ? '<span class="badge badge-success">Posted</span>' : '<span class="badge badge-warning">Waiting</span>';
            $row[] = $au->created_at;
            if ($this->ion_auth->in_group($this->_groups())) :
            $row[] = ($au->is_admit == 1) ? "<input type='checkbox' onclick='is_admit($au->id".','."0)' checked>" : "<input type='checkbox' onclick='is_admit($au->id".','."1)'>";
            endif;
            //add html for action
            $row[] = '<div class="btn-group"><a class="btn btn-sm btn-info" href="' . base_url('About_Us/show/' . $au->id) . '">Preview</a><a class="btn btn-sm btn-warning" href="' . base_url('About_Us/edit/' . $au->id) . '">Edit</a>
                  <a class="btn btn-sm btn-danger" href="' . base_url('About_Us/delete/' . $au->id) . '" onclick="return confirm(' . "'Anda yakin ingin menghapus " . $au->title1 . "?'" . ')">Delete</a></div>';
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->about_us->count_all(),
            "recordsFiltered" => $this->about_us->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function show($id)
	{
        $data['pg_title']	='About Us';
        $data['about_us'] = $this->about_us->get_by_id($id);
		$this->template->load('Container', 'about_us/show', $data);
	}

    public function add()
    {
        $data['groups'] = $this->_groups();
        $data['pg_menu'] = 'Home';
        $data['pg_title'] = 'About Us';
        $this->template->load('Admin', 'about_us/add', $data);
    }

    public function add_act()
    {
        $title1 = $this->input->post('title1');
        $title2 = $this->input->post('title2');
        $title3 = $this->input->post('title3');
        $title4 = $this->input->post('title4');
        $description = $this->input->post('description');
        $is_active = $this->input->post('is_active');
        $is_posting = $this->input->post('is_posting');
        // $is_admit = $this->input->post('is_admit');
        // if ($is_admit == NULL) {
        //     $is_admit = 0;
        // }
        if ($is_posting == NULL) {
            $is_posting = 0;
        }
        
        if($is_posting == 1){
            $where = ['is_posting' => 1];
            $data = ['is_posting' => 0];
            $this->about_us->update($where,$data);
        }

        $this->form_validation->set_rules('title1', 'Judul 1', 'required', array(
            'required' => '%s wajib diisi',
        ));

        $this->form_validation->set_rules('description', 'Deskripsi', 'required', array(
            'required' => '%s wajib diisi',
        ));

        if ($this->form_validation->run() != FALSE) {

            $data = array(
                'title1' => $title1,
                'title2' => $title2,
                'title3' => $title3,
                'title4' => $title4,
                'description' => $description,
                'post_by' => $this->ion_auth->get_user_id(),
                'is_active' => $is_active,
                // 'is_admit' => $is_admit,
                'is_posting' => $is_posting,
                'created_at' => date("Y-m-d H:i:s"),
            );

            $this->about_us->save($data);
            $this->session->set_flashdata('flash', 'add_success');
            redirect('About_Us/add');
        } else {     
            $data['groups'] = $this->_groups();      
            $data['pg_menu'] = 'Home';
            $data['pg_title'] = 'Slider Product';
            $this->template->load('Admin', 'about_us/add', $data);
        }
    }

    public function edit($id)
    {
        $data['groups'] = $this->_groups();
        $data['about_us'] = $this->about_us->get_by_id($id);
        $data['pg_menu'] = 'Home';
        $data['pg_title'] = 'About Us';
        $this->template->load('Admin', 'about_us/edit', $data);
    }

    public function edit_act()
    {
        $id = $this->input->post('id');
        $title1 = $this->input->post('title1');
        $title2 = $this->input->post('title2');
        $title3 = $this->input->post('title3');
        $title4 = $this->input->post('title4');
        $description = $this->input->post('description');
        $is_active = $this->input->post('is_active');
        // $is_admit = $this->input->post('is_admit');
        $is_posting = $this->input->post('is_posting');
        // if ($is_admit == NULL) {
        //     $is_admit = 0;
        // }
        if ($is_posting == NULL) {
            $is_posting = 0;
        }
        
        if($is_posting == 1){
            $where = ['is_posting' => 1];
            $data = ['is_posting' => 0];
            $this->about_us->update($where,$data);
        }

        $this->form_validation->set_rules('title1', 'Judul 1', 'required', array(
            'required' => '%s wajib diisi',
        ));
        $this->form_validation->set_rules('description', 'Deskripsi', 'required', array(
            'required' => '%s wajib diisi',
        ));

        if ($this->form_validation->run() != FALSE) {

            $data = array(
                'title1' => $title1,
                'title2' => $title2,
                'title3' => $title3,
                'title4' => $title4,
                'description' => $description,
                'is_active' => $is_active,
                // 'is_admit' => $is_admit,
                'post_by' => $this->ion_auth->get_user_id(),
                'is_posting' => $is_posting,
            );

            $where = array(
                'id' => $id
            );

            $this->about_us->update($where, $data);
            $this->session->set_flashdata('flash', 'edit_success');
            redirect('About_Us/edit/' . $id);
        } else {
            if (empty($id)) {
                $this->session->set_flashdata('flash', 'edit_failed');
                redirect('About_Us');
            }
            $data['groups'] = $this->_groups();
            $data['about_us'] = $this->about_us->get_by_id($id);
            $data['pg_menu'] = 'Home';
            $data['pg_title'] = 'About Us';
            $this->template->load('Admin', 'about_us/edit', $data);
        }
    }

    function delete($id)
    {
        $this->about_us->delete_by_id($id);
        $this->session->set_flashdata('flash', 'delete_success');
        redirect('About_Us');
    }

    function is_admit_update()
    {
        $id = $this->input->post('id');
        $data = $this->input->post('is_admit');

        $data = array(
            'is_admit' => $data,
        );

        if($this->about_us->update(array('id' => $id), $data)){

            echo json_encode(array("status" => TRUE));
        }
    
    }
}
