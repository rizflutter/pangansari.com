<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Parallax extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_parallax', 'parallax');
        $this->load->library(['form_validation', 'ion_auth']);
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('flash', 'deny');
            redirect('auth/login', 'refresh');
        }
    }

    private function _groups()
    {
        return $groups = ['admin', 'managers'];
    }

    public function index()
    {
        $data['groups'] = $this->_groups();
        $data['pg_title'] = 'Parallax';
        $data['pg_menu'] = 'News';
        $this->template->load('Admin', 'parallax/index', $data);
    }

    public function ajax_list()
    {
        $list = $this->parallax->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $p) {
            $no++;
            $row = array();
            $row[] = $p->id;
            $row[] = $p->index;
            $row[] = $p->title;
            $row[] = $p->description;
            $row[] = $p->start_date;
            $row[] = $p->finish_date;
            $row[] = '<img src="' . base_url($p->img) . '" alt="" height="50">';
            $row[] = $p->link;
            $row[] = $p->full_name;
            $row[] = ($p->is_active == 1) ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Disable</span>';
            $row[] = ($p->is_admit == 1) ? '<span class="badge badge-success">Approved</span>' : '<span class="badge badge-warning">Waiting</span>';
            $row[] = $p->created_at;
            if ($this->ion_auth->in_group($this->_groups())) :
                $row[] = ($p->is_admit == 1) ? "<input type='checkbox' onclick='is_admit($p->id" . ',' . "0)' checked>" : "<input type='checkbox' onclick='is_admit($p->id" . ',' . "1)'>";
            endif;
            //add html for action
            $row[] = '<div class="btn-group"><a class="btn btn-sm btn-warning" href="' . base_url('Parallax/edit/' . $p->id) . '">Edit</a>
                  <a class="btn btn-sm btn-danger" href="' . base_url('Parallax/delete/' . $p->id) . '" onclick="return confirm(' . "'Anda yakin ingin menghapus " . $p->title . "?'" . ')">Delete</a></div>';
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->parallax->count_all(),
            "recordsFiltered" => $this->parallax->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function add()
    {
        $data['parallax_exists'] = $this->parallax->get_all('parallax');
        $data['groups'] = $this->_groups();
        $data['pg_menu'] = 'News';
        $data['pg_title'] = 'Parallax';
        $this->template->load('Admin', 'parallax/add', $data);
    }

    public function add_act()
    {
        $title = $this->input->post('title');
        $index = $this->input->post('index');
        $description = $this->input->post('description');
        $start_date = date_create($this->input->post('start_date'));
        $finish_date = date_create($this->input->post('finish_date'));
        $link = $this->input->post('link');
        $is_active = $this->input->post('is_active');

        // $is_admit = $this->input->post('is_admit');
        // if ($is_admit == NULL) {
        //     $is_admit = 0;
        // }

        $this->form_validation->set_rules('title', 'Judul', 'required', array(
            'required' => '%s wajib diisi',
        ));

        $this->form_validation->set_rules('index', 'Urutan', 'required', array(
            'required' => '%s wajib diisi',
        ));

        $this->form_validation->set_rules('description', 'Deskripsi', 'required', array(
            'required' => '%s wajib diisi',
        ));

        $this->form_validation->set_rules('start_date', 'Start Date', 'required', array(
            'required' => '%s wajib diisi',
        ));

        $this->form_validation->set_rules('finish_date', 'Finish Date', 'required', array(
            'required' => '%s wajib diisi',
        ));

        $this->form_validation->set_rules('link', 'Link', 'required', array(
            'required' => '%s wajib diisi',
        ));

        if ($this->form_validation->run() != FALSE) {

            $upload_image = $_FILES['img']['name'];

            if ($upload_image) {

                $file = $this->parallax->upload();

                if ($file['result'] == "failed") {
                    $this->session->set_flashdata('img', $file['error']);
                    $data['img'] = 'assets/images/parallax/default-parallax.jpg';
                } else {
                    $data['img'] = 'assets/images/parallax/' . $file['file']['file_name'];
                }
            }

            $data['title'] = $title;
            $data['index'] = $index;
            $data['description'] = $description;
            $data['start_date'] = date_format($start_date, "Y-m-d H:i:s");
            $data['finish_date'] = date_format($finish_date, "Y-m-d H:i:s");
            $data['link'] = $link;
            $data['post_by'] = $this->ion_auth->get_user_id();
            $data['is_active'] = $is_active;
            // $data['is_admit'] = $is_admit;
            $data['created_at'] = date("Y-m-d H:i:s");

            $this->parallax->save($data);
            $this->session->set_flashdata('flash', 'add_success');
            redirect('Parallax/add');
        } else {
            $data['groups'] = $this->_groups();
            $data['pg_menu'] = 'News';
            $data['pg_title'] = 'Parallax';
            $this->template->load('Admin', 'parallax/add', $data);
        }
    }

    public function edit($id)
    {
        $data['groups'] = $this->_groups();
        $data['parallax_row'] = $this->parallax->get_by_id($id);
        $data['pg_menu'] = 'News';
        $data['pg_title'] = 'Parallax';
        $this->template->load('Admin', 'parallax/edit', $data);
    }

    public function edit_act()
    {
        $id = $this->input->post('id');
        $where = array(
            'id' => $id
        );
        $parallax_row = $this->parallax->get_by_id($id);
        $title = $this->input->post('title');
        $index = $this->input->post('index');
        $description = $this->input->post('description');
        $start_date = date_create($this->input->post('start_date'));
        $finish_date = date_create($this->input->post('finish_date'));
        $link = $this->input->post('link');
        $is_active = $this->input->post('is_active');
        // $is_admit = $this->input->post('is_admit');
        // if ($is_admit == NULL) {
        //     $is_admit = 0;
        // }

        $this->form_validation->set_rules('title', 'Judul', 'required', array(
            'required' => '%s wajib diisi',
        ));

        $this->form_validation->set_rules('index', 'Urutan', 'required', array(
            'required' => '%s wajib diisi',
        ));

        $this->form_validation->set_rules('description', 'Deskripsi', 'required', array(
            'required' => '%s wajib diisi',
        ));

        $this->form_validation->set_rules('start_date', 'Start Date', 'required', array(
            'required' => '%s wajib diisi',
        ));

        $this->form_validation->set_rules('finish_date', 'Finish Date', 'required', array(
            'required' => '%s wajib diisi',
        ));

        $this->form_validation->set_rules('link', 'Link', 'required', array(
            'required' => '%s wajib diisi',
        ));

        if ($this->form_validation->run() != FALSE) {

            $upload_image = $_FILES['img']['name'];

            if ($upload_image) {

                $file = $this->parallax->upload();

                if ($file['result'] == "failed") {
                    $this->session->set_flashdata('img', $file['error']);
                    $data['img'] = 'assets/images/parallax/default-parallax.jpg';
                } else {
                    $old_image = $parallax_row->img;
                    if ($old_image != 'assets/images/parallax/default-parallax.png') {
                        unlink(FCPATH . $old_image);
                    }
                    $data['img'] = 'assets/images/parallax/' . $file['file']['file_name'];
                }
            }

            $data['title'] = $title;
            $data['index'] = $index;
            $data['description'] = $description;
            $data['start_date'] = date_format($start_date, "Y-m-d H:i:s");
            $data['finish_date'] = date_format($finish_date, "Y-m-d H:i:s");
            $data['link'] = $link;
            $data['is_active'] = $is_active;
            // $data['is_admit'] = $is_admit;

            $this->parallax->update($where, $data);
            $this->session->set_flashdata('flash', 'edit_success');
            redirect('Parallax/edit/' . $id);
        } else {
            if (empty($id)) {
                $this->session->set_flashdata('flash', 'edit_failed');
                redirect('Parallax');
            }
            $data['groups'] = $this->_groups();
            $data['parallax_row'] = $parallax_row;
            $data['pg_menu'] = 'Parallax';
            $data['pg_title'] = 'News';
            $this->template->load('Admin', 'parallax/edit', $data);
        }
    }

    function delete($id)
    {
        $image = $this->parallax->get_by_id($id);
        $hapus_image = $image->img;

        if (file_exists(FCPATH . $hapus_image)) {
            if ($hapus_image != 'assets/images/parallax/parallax_default.png') {
                unlink(FCPATH . $hapus_image);
            }
        }

        $this->parallax->delete_by_id($id);
        $this->session->set_flashdata('flash', 'delete_success');
        redirect('Parallax');
    }

    function is_admit_update()
    {
        $id = $this->input->post('id');
        $data = $this->input->post('is_admit');

        $data = array(
            'is_admit' => $data,
        );

        if ($this->parallax->update(array('id' => $id), $data)) {

            echo json_encode(array("status" => TRUE));
        }
    }
}
