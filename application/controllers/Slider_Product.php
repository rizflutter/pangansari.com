<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Slider_Product extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_slider_product', 'slider_product');
        $this->load->library(['form_validation', 'ion_auth']);
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('flash', 'deny');
            redirect('auth/login', 'refresh');
        }
    }

    // reuseable group
    private function _groups()
    {
        return $groups = ['admin', 'managers'];
    }

    public function index()
    {
        $data['groups'] = $this->_groups();
        $data['pg_menu'] = 'Home';
        $data['pg_title'] = 'Slider Product';
        $this->template->load('Admin', 'slider_product/index', $data);
    }

    public function ajax_list()
    {
        $list = $this->slider_product->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $sp) {
            $no++;
            $row = array();
            $row[] = $sp->index;
            $row[] = $sp->title1;
            $row[] =  substr($sp->description, 0, 25) . '...';
            $row[] = $sp->start_date;
            $row[] = $sp->finish_date;
            $row[] = $sp->full_name;
            $row[] = '<img src="' . base_url($sp->img) . '" alt="" height="50">';
            $row[] = ($sp->is_active == 1) ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Disable</span>';
            $row[] = ($sp->is_admit == 1) ? '<span class="badge badge-success">Approved</span>' : '<span class="badge badge-warning">Waiting</span>';
            if ($this->ion_auth->in_group($this->_groups())) :
            $row[] = ($sp->is_admit == 1) ? "<input type='checkbox' onclick='is_admit($sp->id".','."0)' checked>" : "<input type='checkbox' onclick='is_admit($sp->id".','."1)'>";
            endif;
            //add html for action
            $row[] = '<div class="btn-group"><a class="btn btn-sm btn-warning" href="' . base_url('Slider_Product/edit/' . $sp->id) . '">Edit</a>
                  <a class="btn btn-sm btn-danger" href="' . base_url('Slider_Product/delete/' . $sp->id) . '" onclick="return confirm(' . "'Anda yakin ingin menghapus " . $sp->title1 . "?'" . ')">Delete</a></div>';
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->slider_product->count_all(),
            "recordsFiltered" => $this->slider_product->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function add()
    {
        $data['groups'] = $this->_groups();
        $data['product_exists'] = $this->slider_product->get_all('slider_product');
        $data['pg_menu'] = 'Home';
        $data['pg_title'] = 'Slider Product';
        $this->template->load('Admin', 'slider_product/add', $data);
    }

    public function add_act()
    {
        $index = $this->input->post('index');
        $title1 = $this->input->post('title1');
        $title2 = $this->input->post('title2');
        $title3 = $this->input->post('title3');
        $title4 = $this->input->post('title4');
        $description = $this->input->post('description');
        $start_date = date_create($this->input->post('start_date'));
        $finish_date = date_create($this->input->post('finish_date'));
        $is_active = $this->input->post('is_active');

        // $is_admit = $this->input->post('is_admit');
        // if ($is_admit == NULL) {
        //     $is_admit = 0;
        // }

        $this->form_validation->set_rules('index', 'Index', 'required|is_unique[slider_product.index]', array(
            'required' => '%s wajib diisi',
            'is_unique' => '%s sudah ada, silahkan gunakan yang lain!',
        ));

        $this->form_validation->set_rules('title1', 'Judul 1', 'required', array(
            'required' => '%s wajib diisi',
        ));

        $this->form_validation->set_rules('description', 'Deskripsi', 'required', array(
            'required' => '%s wajib diisi',
        ));

        $this->form_validation->set_rules('start_date', 'Tanggal Mulai', 'required', array(
            'required' => '%s wajib diisi',
        ));

        $this->form_validation->set_rules('finish_date', 'Tanggal Selesai', 'required', array(
            'required' => '%s wajib diisi',
        ));

        if ($this->form_validation->run() != FALSE) {

            $upload_image = $_FILES['img']['name'];

            if ($upload_image) {
                $file = $this->slider_product->upload();

                if ($file['result'] == "failed") {
                    $this->session->set_flashdata('img', $file['error']);
                    $data['img'] = 'assets/images/slide/default_sp.png';
                } else {
                    $data['img'] = 'assets/images/slide/' . $file['file']['file_name'];
                }
            } else {
                $data['img'] = 'assets/images/slide/default_sp.png';
            }

            $data['index'] = $index;
            $data['business_category_id'] = 2;
            $data['sort'] = $index + 1;
            $data['title1'] = $title1;
            $data['title2'] = $title2;
            $data['title3'] = $title3;
            $data['title4'] = $title4;
            $data['description'] = $description;
            $data['start_date'] = date_format($start_date, "Y-m-d H:i:s");
            $data['finish_date'] = date_format($finish_date, "Y-m-d H:i:s");
            $data['post_by'] = $this->ion_auth->get_user_id();
            $data['is_active'] = $is_active;
            // $data['is_admit'] = $is_admit;
            $data['created_at'] = date("Y-m-d H:i:s");

            $this->slider_product->save($data);
            $this->session->set_flashdata('flash', 'add_success');
            redirect('Slider_Product/add');
        } else {
            $data['groups'] = $this->_groups();
            $data['product_exists'] = $this->slider_product->get_all('slider_product');
            $data['pg_menu'] = 'Home';
            $data['pg_title'] = 'Slider Product';
            $this->template->load('Admin', 'slider_product/add', $data);
        }
    }

    public function edit($id)
    {
        $data['groups'] = $this->_groups();
        $data['slider_row'] = $this->slider_product->get_by_id($id);
        $data['product_exists'] = $this->slider_product->get_all('slider_product');
        $data['pg_menu'] = 'Home';
        $data['pg_title'] = 'Slider Product';
        $this->template->load('Admin', 'slider_product/edit', $data);
    }

    public function edit_act()
    {
        $id = $this->input->post('id');
        $slide_row =  $this->slider_product->get_by_id($id);
        $index = $this->input->post('index');
        $title1 = $this->input->post('title1');
        $title2 = $this->input->post('title2');
        $title3 = $this->input->post('title3');
        $title4 = $this->input->post('title4');
        $description = $this->input->post('description');
        $start_date = date_create($this->input->post('start_date'));
        $finish_date = date_create($this->input->post('finish_date'));
        $is_active = $this->input->post('is_active');
        // $is_admit = $this->input->post('is_admit');
        // if ($is_admit == NULL) {
        //     $is_admit = 0;
        // }

        $this->form_validation->set_rules('index', 'Index', 'required', array(
            'required' => '%s wajib diisi',
        ));
        $this->form_validation->set_rules('title1', 'Judul 1', 'required', array(
            'required' => '%s wajib diisi',
        ));
        $this->form_validation->set_rules('description', 'Deskripsi', 'required', array(
            'required' => '%s wajib diisi',
        ));
        $this->form_validation->set_rules('start_date', 'Tanggal Mulai', 'required', array(
            'required' => '%s wajib diisi',
        ));
        $this->form_validation->set_rules('finish_date', 'Tanggal Selesai', 'required', array(
            'required' => '%s wajib diisi',
        ));

        if ($this->form_validation->run() != FALSE) {

            $upload_image = $_FILES['img']['name'];

            if ($upload_image) {

                $file = $this->slider_product->upload();

                if ($file['result'] == "failed") {
                    $this->session->set_flashdata('img', $file['error']);
                } else {
                    $old_img = $slide_row->img;
                    if ($old_img != 'assets/images/slide/default_sp.png') {
                        unlink(FCPATH . $old_img);
                    }
                    $data['img'] = 'assets/images/slide/' . $file['file']['file_name'];
                }
            }

            $data['index'] = $index;
            $data['business_category_id'] = 2;
            $data['sort'] = $index + 1;
            $data['title1'] = $title1;
            $data['title2'] = $title2;
            $data['title3'] = $title3;
            $data['title4'] = $title4;
            $data['description'] = $description;
            $data['start_date'] = date_format($start_date, "Y-m-d H:i:s");
            $data['finish_date'] = date_format($finish_date, "Y-m-d H:i:s");
            $data['post_by'] = $this->ion_auth->get_user_id();
            $data['is_active'] = $is_active;
            // $data['is_admit'] = $is_admit;

            $where = array(
                'id' => $id
            );

            $this->slider_product->update($where, $data);
            $this->session->set_flashdata('flash', 'edit_success');
            redirect('Slider_Product/edit/' . $id);
        } else {
            if (empty($id)) {
                $this->session->set_flashdata('flash', 'edit_failed');
                redirect('slider_product');
            }
            $data['groups'] = $this->_groups();
            $data['slider_row'] = $this->slider_product->get_by_id($id);
            $data['product_exists'] = $this->slider_product->get_all('slider_product');
            $data['pg_title'] = 'Slider Product';
            $data['pg_menu'] = 'Home';
            $this->template->load('Admin', 'Slider_Product/edit', $data);
        }
    }

    function delete($id)
    {
        $image = $this->slider_product->get_by_id($id);
        $hapus = $image->img;

        if (file_exists(FCPATH . $hapus)) {
            if ($hapus != 'assets/images/slide/default_sp.png') {
                unlink(FCPATH . $hapus);
            }
        }

        $this->slider_product->delete_by_id($id);
        $this->session->set_flashdata('flash', 'delete_success');
        redirect('Slider_Product');
    }

    function is_admit_update()
    {
        $id = $this->input->post('id');
        $data = $this->input->post('is_admit');

        $data = array(
            'is_admit' => $data,
        );

        if ($this->slider_product->update(array('id' => $id), $data)) {

            echo json_encode(array("status" => TRUE));
        }
    }
}
