<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		$this->load->model('m_news');
	}

    public function index(){
        $data['pg_title']	= 'News';

    	if(isset($_GET['cat'])){
    		$cat = $_GET['cat'];
    	} else {
    		$cat = 'news';
    	}

		$s=$this->input->get('s');

		if($this->input->get('s')){
			$config['suffix'] = "?cat=".$cat."&&s=".$s.'#news-list';
		} else {
			$config['suffix'] = "?cat=".$cat.'#news-list';
		}

		$config['base_url'] = base_url()."news/index/";
		$config['first_url'] = $config['base_url'].'0?'.http_build_query($_GET).'#news-list';

		if($this->input->get('s')){
			$config['total_rows'] = $this->m_news->news_all($cat, $s)->num_rows();
		} else {
			$config['total_rows'] = $this->m_news->news_all($cat)->num_rows();
		}

        $config['per_page']		= 1;
        $config['num_links'] 	= 5;
        $from	= $this->uri->segment(3, 0);

        //Tambahan untuk styling
        $config['first_link']	= "« Pertama";
        $config['last_link']	= "Terakhir »";
        $config['next_link']	= '»';
        $config['prev_link']	= '«';
        
        $this->pagination->initialize($config);

        if($this->input->get('s')){
        	$data['news']		= $this->m_news->news($cat, $from, $config['per_page'], $s)->result();
		} else {
       		$data['news']		= $this->m_news->news($cat, $from, $config['per_page'])->result();
		}

		$data['num_news']		= $config['total_rows'];
		$sort = "ORDER BY sort ASC";
		$data['news_category']	= $this->m_news->news_category($sort)->result();
		$data['latest_news']	= $this->m_news->latest_news($cat)->result();
		$data['news_archive']	= $this->m_news->news_archive($cat)->result();
		$data['parallax1']		= $this->m_ads->parallax1()->row_array();
		$data['parallax2']		= $this->m_ads->parallax2()->row_array();
		$data['parallax3']		= $this->m_ads->parallax3()->row_array();
		$data['parallax4']		= $this->m_ads->parallax4()->row_array();

		$this->template->load('Container', 'news/index', $data);
    }

    public function archive($year){
        $data['pg_title']	= 'News';

    	if(isset($_GET['cat'])){
    		$cat = $_GET['cat'];
    	} else {
    		$cat = 'news';
    	}

		$s=$this->input->get('s');

		if($this->input->get('s')){
			$config['suffix'] = "?cat=".$cat."&&s=".$s.'#news-list';
		} else {
			$config['suffix'] = "?cat=".$cat.'#news-list';
		}

		$config['base_url'] = base_url()."news/archive/".$year."/";
		$config['first_url'] = $config['base_url'].'0?'.http_build_query($_GET).'#news-list';

		if($this->input->get('s')){
			$config['total_rows'] = $this->m_news->news_archives_all($cat, $year, $s)->num_rows();
		} else {
			$config['total_rows'] = $this->m_news->news_archives_all($cat, $year)->num_rows();
		}

        $config['per_page']		= 1;
        $config['num_links'] 	= 5;
        $from					= $this->uri->segment(4, 0);
		
        //Tambahan untuk styling
        $config['first_link']	= "« Pertama";
        $config['last_link']	= "Terakhir »";
        $config['next_link']	= '»';
        $config['prev_link']	= '«';
        

        if($this->input->get('s')){
        	$data['news']		= $this->m_news->news_archives($cat, $year, $from, $config['per_page'], $s)->result();
		} else {
       		$data['news']		= $this->m_news->news_archives($cat, $year, $from, $config['per_page'])->result();
		}
        $this->pagination->initialize($config);

		$data['num_news']		= $config['total_rows'];
		$sort = "ORDER BY sort ASC";
		$data['news_category']	= $this->m_news->news_category($sort)->result();
		$data['latest_news']	= $this->m_news->latest_news($cat)->result();
		$data['news_archive']	= $this->m_news->news_archive($cat)->result();
		$data['parallax1']		= $this->m_ads->parallax1()->row_array();
		$data['parallax2']		= $this->m_ads->parallax2()->row_array();
		$data['parallax3']		= $this->m_ads->parallax3()->row_array();
		$data['parallax4']		= $this->m_ads->parallax4()->row_array();

		$this->template->load('Container', 'news/index', $data);
    }

    public function detail($link_post)
	{	
		if($link_post == TRUE){
			if(isset($_GET['cat'])){
	    		$cat = $_GET['cat'];
	    	} else {
	    		$cat = 'news';
	    	}

        	$data['news_detail']	= $this->m_news->news_detail($link_post)->row_array();
			$sort = "ORDER BY sort ASC";
			$data['news_category']	= $this->m_news->news_category($sort)->result();
			$data['latest_news']	= $this->m_news->latest_news($cat)->result();
			$data['news_archive']	= $this->m_news->news_archive($cat)->result();
			$data['parallax1']		= $this->m_ads->parallax1()->row_array();
			$data['parallax2']		= $this->m_ads->parallax2()->row_array();
			$data['parallax3']		= $this->m_ads->parallax3()->row_array();
			$data['parallax4']		= $this->m_ads->parallax4()->row_array();

        	$data['pg_title']	= $data['news_detail']['title'];
			$this->template->load('Container', 'news/detail', $data);
		} else {
			redirect(base_url().'news', 'refresh');
		}
	}
}
