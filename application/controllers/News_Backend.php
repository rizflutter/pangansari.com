<?php
defined('BASEPATH') or exit('No direct script access allowed');

class News_Backend extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_news_backend', 'news_backend');
        $this->load->library(['form_validation', 'ion_auth']);
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('flash', 'deny');
            redirect('auth/login', 'refresh');
        }
    }

    private function _groups()
    {
        return $groups = ['admin', 'managers'];
    }

    public function index()
    {
        // echo FCPATH . 'application/system';
        // echo '<br>';
        // echo dirname(__FILE__) . DIRECTORY_SEPARATOR . 'system';
        // die;
        $data['groups'] = $this->_groups();
        $data['pg_title'] = 'Add News';
        $data['pg_menu'] = 'News';
        $this->template->load('Admin', 'news_backend/index', $data);
    }

    public function ajax_list()
    {
        $list = $this->news_backend->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $n) {
            $no++;
            $row = array();
            $row[] = $n->category_name;
            $row[] = $n->title;
            $row[] = $n->link_post;
            $row[] = '<img src="' . base_url($n->img) . '" alt="" height="50">';
            $row[] = $n->full_name;
            $row[] = ($n->is_active == 1) ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Disable</span>';
            $row[] = ($n->is_admit == 1) ? '<span class="badge badge-success">Approved</span>' : '<span class="badge badge-warning">Waiting</span>';
            $row[] = $n->start_date;
            $row[] = $n->finish_date;
            $row[] = $n->created_at;
            if ($this->ion_auth->in_group($this->_groups())) :
            $row[] = ($n->is_admit == 1) ? "<input type='checkbox' onclick='is_admit($n->id".','."0)' checked>" : "<input type='checkbox' onclick='is_admit($n->id".','."1)'>";
            endif;
            //add html for action
            $row[] = '<div class="btn-group"><a class="btn btn-sm btn-info" href="' . base_url('News_Backend/show/' . $n->id) . '">Preview</a><a class="btn btn-sm btn-warning" href="' . base_url('News_Backend/edit/' . $n->id) . '">Edit</a>
                  <a class="btn btn-sm btn-danger" href="' . base_url('News_Backend/delete/' . $n->id) . '" onclick="return confirm(' . "'Anda yakin ingin menghapus " . $n->title . "?'" . ')">Delete</a></div>';
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->news_backend->count_all(),
            "recordsFiltered" => $this->news_backend->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function show($id)
	{
        $data['pg_title']	='News';
        $data['news'] = $this->news_backend->get_by_id($id);
		$this->template->load('Container', 'news_backend/show', $data);
	}

    public function add()
    {
        $data['groups'] = $this->_groups();
        $data['news_category'] = $this->news_backend->get_all('news_category');
        $data['pg_menu'] = 'News';
        $data['pg_title'] = 'Add News';
        $this->template->load('Admin', 'news_backend/add', $data);
    }

    public function add_act()
    {
        $news_category_id = $this->input->post('news_category_id');
        $title = $this->input->post('title');
        $description = htmlentities(htmlspecialchars($_POST['description']));
        $video_embed =  htmlentities(htmlspecialchars($_POST['video_embed']));
        $is_active = $this->input->post('is_active');
        $start_date = date_create($this->input->post('start_date'));
        $finish_date = date_create($this->input->post('finish_date'));

        // $is_admit = $this->input->post('is_admit');
        // if ($is_admit == NULL) {
        //     $is_admit = 0;
        // }

        $this->form_validation->set_rules('news_category_id', 'News Category', 'required', array(
            'required' => '%s wajib dipilih',
        ));

        $this->form_validation->set_rules('title', 'Judul', 'required|is_unique[news.title]', array(
            'required' => '%s wajib diisi',
            'is_unique' => '%s sudah ada, silahkan gunakan yang lain!',
        ));

        $this->form_validation->set_rules('start_date', 'Start Date', 'required', array(
            'required' => '%s wajib diisi',
        ));

        $this->form_validation->set_rules('finish_date', 'Finish Date', 'required', array(
            'required' => '%s wajib diisi',
        ));

        if ($this->form_validation->run() != FALSE) {

            $upload_image = $_FILES['img']['name'];

            if ($upload_image) {

                $file = $this->news_backend->upload();

                if ($file['result'] == "failed") {
                    $this->session->set_flashdata('img', $file['error']);
                    $data['img'] = 'assets/images/news/news_default.png';
                } else {
                    $data['img'] = 'assets/images/news/' . $file['file']['file_name'];
                }
            } else {
                $data['img'] = 'assets/images/news/news_default.png';
            }

            $data['news_category_id'] = $news_category_id;
            $data['title'] = $title;
            $data['description'] = $description;
            $data['link_post'] = strtolower(str_replace(" ","-",$title));
            $data['video_embed'] = $video_embed;
            $data['post_by'] = $this->ion_auth->get_user_id();
            $data['is_active'] = $is_active;
            // $data['is_admit'] = $is_admit;
            $data['start_date'] = date_format($start_date, "Y-m-d H:i:s");
            $data['finish_date'] = date_format($finish_date, "Y-m-d H:i:s");
            $data['created_at'] = date("Y-m-d H:i:s");

            $this->news_backend->save($data);
            $this->session->set_flashdata('flash', 'add_success');
            redirect('News_Backend/add');
        } else {
            $data['groups'] = $this->_groups();
            $data['news_category'] = $this->news_backend->get_all('news_category');
            $data['pg_menu'] = 'News';
            $data['pg_title'] = 'Add News';
            $this->template->load('Admin', 'news_backend/add', $data);
        }
    }

    public function edit($id)
    {
        $data['groups'] = $this->_groups();
        $data['news_row'] = $this->news_backend->get_by_id($id);
        $data['news_category'] = $this->news_backend->get_all('news_category');
        $data['pg_menu'] = 'News';
        $data['pg_title'] = 'Edit News';
        $this->template->load('Admin', 'news_backend/edit', $data);
    }

    public function edit_act()
    {
        $id = $this->input->post('id');
        $where = array(
            'id' => $id
        );
        $news_row = $this->news_backend->get_by_id($id);
        $news_category_id = $this->input->post('news_category_id');
        $title = $this->input->post('title');
        $description = htmlentities(htmlspecialchars($_POST['description']));
        $video_embed = htmlentities(htmlspecialchars($_POST['video_embed']));
        $is_active = $this->input->post('is_active');
        $start_date = date_create($this->input->post('start_date'));
        $finish_date = date_create($this->input->post('finish_date'));
        // $is_admit = $this->input->post('is_admit');
        // if ($is_admit == NULL) {
        //     $is_admit = 0;
        // }

        $this->form_validation->set_rules('news_category_id', 'News Category', 'required', array(
            'required' => '%s wajib dipilih',
        ));

        $this->form_validation->set_rules('title', 'Judul', 'is_unique[news.title]', array(
            'is_unique' => '%s sudah ada, silahkan gunakan yang lain!',
        ));

        if ($this->form_validation->run() != FALSE) {

            $upload_image = $_FILES['img']['name'];

            if ($upload_image) {

                $file = $this->news_backend->upload();

                if ($file['result'] == "failed") {
                    $this->session->set_flashdata('img', $file['error']);
                } else {
                    $old_image = $news_row->img;
                    if($old_image != 'assets/images/news/news_default.png'){
                        unlink(FCPATH . $old_image);
                    }
                    $data['img'] = 'assets/images/news/' . $file['file']['file_name'];
                }
            }

            $data['news_category_id'] = $news_category_id;
            $data['description'] = $description;
            $data['video_embed'] = $video_embed;
            $data['post_by'] = $this->ion_auth->get_user_id();
            $data['is_active'] = $is_active;
            $data['start_date'] = date_format($start_date, "Y-m-d H:i:s");
            $data['finish_date'] = date_format($finish_date, "Y-m-d H:i:s");
            // $data['is_admit'] = $is_admit;

            if (!empty($title)) {
                $data['title'] = $title;
                $data['link_post'] = strtolower(str_replace(" ","-",$title));
            }

            $this->news_backend->update($where, $data);
            $this->session->set_flashdata('flash', 'edit_success');
            redirect('News_Backend/edit/' . $id);
        } else {
            if (empty($id)) {
                $this->session->set_flashdata('flash', 'edit_failed');
                redirect('News_Backend');
            }
            $data['groups'] = $this->_groups();
            $data['news_row'] = $news_row;
            $data['news_category'] = $this->news_backend->get_all('news_category');
            $data['pg_menu'] = 'News';
            $data['pg_title'] = 'Edit News';
            $this->template->load('Admin', 'news_backend/edit', $data);
        }
    }

    function delete($id)
    {
        $image = $this->news_backend->get_by_id($id);
        $hapus_image = $image->img;

        if (file_exists(FCPATH . $hapus_image)) {
            if ($hapus_image != 'assets/images/news/news_default.png') {
                unlink(FCPATH . $hapus_image);
            }
        }

        $this->news_backend->delete_by_id($id);
        $this->session->set_flashdata('flash', 'delete_success');
        redirect('News_Backend');
    }

    function is_admit_update()
    {
        $id = $this->input->post('id');
        $data = $this->input->post('is_admit');

        $data = array(
            'is_admit' => $data,
        );

        if($this->news_backend->update(array('id' => $id), $data)){

            echo json_encode(array("status" => TRUE));
        }
    
    }
}
