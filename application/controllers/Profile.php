<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profile extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_profile', 'profile');
        $this->load->library(['form_validation', 'ion_auth']);
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('flash', 'deny');
            redirect('auth/login', 'refresh');
        }
    }

    public function index()
    {
        $data['user'] = $this->ion_auth->user()->row();
        $data['pg_menu'] = 'User';
        $data['pg_title'] = 'Profile';
        $this->template->load('Admin', 'profile/index', $data);
    }

    public function update_img_profile()
    {
        $id = $this->input->post('id');
        $profile_row =  $this->profile->get_by_id($id);
        $upload_image = $_FILES['img']['name'];

        if ($upload_image) {

            $file = $this->profile->upload();
            
            if ($file['result'] == "failed") {
                $this->session->set_flashdata('img', $file['error']);
            } else {
                $old_img = $profile_row->img;
                if ($old_img != 'assets/images/profiles/default_profile.png') {
                    unlink(FCPATH . $old_img);
                }
                $data['img'] = 'assets/images/profiles/' . $file['file']['file_name'];
                $where = array(
                    'id' => $id
                );
                $this->session->set_flashdata('flash', 'edit_success');
                $this->profile->update($where, $data);
            }
        }
        redirect('profile');
    }

    public function edit_act()
    {
        $id = $this->input->post('id');
        $profile_row = $this->profile->get_by_id($id);
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $password = $this->input->post('password');

        if (!empty($first_name)) {
            $data['first_name'] = $first_name;
        } else {
            $data['first_name'] = $profile_row->first_name;
        }
        if (!empty($last_name)) {
            $data['last_name'] = $last_name;
        } else {
            $data['last_name'] = $profile_row->last_name;
        }
        if (!empty($password)) {
            $data['password'] = password_hash($password, PASSWORD_DEFAULT);
        } else {
            $data['password'] = $profile_row->password;
        }


        $where = array(
            'id' => $id
        );

        $this->profile->update($where, $data);
        $this->session->set_flashdata('flash', 'edit_success');
        redirect('Profile');
    }
}
