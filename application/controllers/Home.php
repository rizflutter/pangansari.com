<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		$this->load->model('m_home');
		$this->load->model('m_news');
		$this->load->model('m_company_profile','company_profile');
	}

	public function index()
	{
		$data['pg_title']			= 'True Quality of Life';
		$data['about_us']			= $this->m_home->about_us()->row();
		
		for($i = 1; $i <= $this->m_home->business_category_count();  $i++){
			$data["data_slide_to_box_$i"] = $this->m_home->data_slide_to($i)->row();
		}
		
		for($i = 1; $i <= $this->m_home->business_category_count();  $i++){
			$data["business_category_$i"] = $this->m_home->business_category($i);
		}
		
		$data['company_profile_row'] = $this->company_profile->download();
		$data['slider_home']		= $this->m_home->slider_home()->result();
		$data['slider_product']		= $this->m_home->slider_product()->result();
		$data['slider_product_count']		= $this->m_home->slider_product()->num_rows();
		$data['business_category']	= $this->m_home->business_category()->result();
		$data['latest_news']		= $this->m_home->latest_news('news')->result();
		$sort = "ORDER BY sort DESC";
		$data['news_category']		= $this->m_news->news_category($sort)->result();

		$this->template->load('Container', 'home/index', $data);
	}
}
