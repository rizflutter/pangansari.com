<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Business_Category extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_business_category', 'business_category');
        $this->load->library(['form_validation', 'ion_auth']);
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('flash', 'deny');
            redirect('auth/login', 'refresh');
        }
    }

    public function index()
    {
        $data['pg_menu'] = 'Home';
        $data['pg_title'] = 'Business Category';
        $this->template->load('Admin', 'business_category/index', $data);
    }

    public function ajax_list()
    {
        $list = $this->business_category->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $bc) {
            $no++;
            $row = array();
            $row[] = $bc->id;
            $row[] = $bc->name;
            $row[] = $bc->description;
            $row[] = $bc->link;
            $row[] = '<img src="' . base_url($bc->img) . '" alt="" height="50">';
            $row[] = $bc->created_at;

            //add html for action
            $row[] = '<div class="btn-group"><a class="btn btn-sm btn-warning" href="javascript:void(0)" title="Edit" onclick="edit_bc(' . "'" . $bc->id . "'" . ')">Edit</a></div>';
            // edit and delete
            // $row[] = '<div class="btn-group"><a class="btn btn-sm btn-warning" href="javascript:void(0)" title="Edit" onclick="edit_bc(' . "'" . $bc->id . "'" . ')">Edit</a>
            //       <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_bc(' . "'" . $bc->id . "'" . "," . "'" . $bc->name . "'" . ')">Delete</a></div>';
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->business_category->count_all(),
            "recordsFiltered" => $this->business_category->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_edit($id)
    {
        $data = $this->business_category->get_by_id($id);
        echo json_encode($data);
    }

    // public function ajax_add()
    // {
    //     $upload_image = $_FILES['img']['name'];
    //     $message = [];

    //     if ($upload_image) {
    //         $file = $this->business_category->upload();
    //         if ($file['result'] == "failed") {
    //             $message = $file['error'];
    //         } else {
    //             $data['img'] = 'assets/images/slide/' . $file['file']['file_name'];
    //             $data['name'] = $this->input->post('name');
    //             $data['description'] = $this->input->post('description');
    //             $data['created_at'] = date("Y-m-d H:i:s");
    //             $this->business_category->save($data);
    //         }
    //     } else {
    //         $message = '<p>Anda belum memilih gambar<p>';
    //     }

    //     echo json_encode($message);
    // }

    public function ajax_update()
    {
        $id = $this->input->post('id');
        $business_category_row =  $this->business_category->get_by_id($id);
        $old_img = $business_category_row->img;
        $upload_image = $_FILES['img']['name'];
        $message = [];

        if ($upload_image) {
            $file = $this->business_category->upload();
            if ($file['result'] == "failed") {
                $message = $file['error'];
            } else {
                if (file_exists(FCPATH . $old_img)) {
                    if ($old_img != 'icon_business_category_default.png') {
                        unlink(FCPATH . $old_img);
                    }
                }
                $data['img'] = 'assets/images/slide/' . $file['file']['file_name'];
            }
        }

        if ($message == FALSE) {
            $data['name'] = $this->input->post('name');
            $data['description'] = $this->input->post('description');
            $data['link'] = $this->input->post('link');

            $this->business_category->update(array('id' => $id), $data);

            echo json_encode(array("status" => TRUE));
        } else {

            echo json_encode($message);
        }
    }

    // public function ajax_delete($id)
    // {
    //     $image = $this->business_category->get_by_id($id);
    //     $hapus = $image->img;

    //     if (file_exists(FCPATH . $hapus)) {
    //         if ($hapus != 'icon_business_category_default.png') {
    //             unlink(FCPATH . $hapus);
    //         }
    //     }
    //     $this->business_category->delete_by_id($id);
    //     echo json_encode(array("status" => TRUE));
    // }
}
