<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Slider_Home extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_slider_home', 'slider_home');
        $this->load->library(['form_validation', 'ion_auth']);
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('flash', 'deny');
            redirect('auth/login', 'refresh');
        }
    }

    // reuseable group
    private function _groups()
    {
        return $groups = ['admin', 'managers'];
    }

    public function index()
    {
        $data['groups'] = $this->_groups();
        $data['pg_title'] = 'Slider Home';
        $data['pg_menu'] = 'Home';
        $this->template->load('Admin', 'slider_home/index', $data);
    }

    public function ajax_list()
    {
        $list = $this->slider_home->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $sh) {
            $no++;
            $row = array();
            $row[] = $sh->business_category_description;
            $row[] = $sh->sort;
            $row[] = $sh->title1;
            $row[] =  substr($sh->description, 0, 25) . '...';
            $row[] = $sh->start_date;
            $row[] = $sh->finish_date;
            $row[] = $sh->full_name;
            $row[] = '<img src="' . base_url($sh->img) . '" alt="" height="50">';
            $row[] = ($sh->is_active == 1) ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Disable</span>';
            $row[] = ($sh->is_admit == 1) ? '<span class="badge badge-success">Approved</span>' : '<span class="badge badge-warning">Waiting</span>';
            //add html for action
            if ($this->ion_auth->in_group($this->_groups())) :
                $row[] = ($sh->is_admit == 1) ? "<input type='checkbox' onclick='is_admit($sh->id" . ',' . "0)' checked>" : "<input type='checkbox' onclick='is_admit($sh->id" . ',' . "1)'>";
            endif;
            // edit only
            $row[] = '<div class="btn-group"><a class="btn btn-sm btn-warning" href="' . base_url('Slider_Home/edit/' . $sh->id) . '">Edit</a></div>';
            // delete and edit
            // $row[] = '<div class="btn-group"><a class="btn btn-sm btn-warning" href="' . base_url('slider_home/edit/' . $sh->id) . '">Edit</a><a class="btn btn-sm btn-danger" href="' . base_url('slider_home/delete/' . $sh->id) . '" onclick="return confirm(' . "'Anda yakin ingin menghapus " . $sh->title1 . "?'" . ')">Delete</a></div>';
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->slider_home->count_all(),
            "recordsFiltered" => $this->slider_home->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function edit($id)
    {
        $data['groups'] = $this->_groups();
        $data['slider_row'] = $this->slider_home->get_by_id($id);
        $business_category_id = $data['slider_row']->business_category_id;
        $data['get_business_category_by_id'] = $this->slider_home->get_business_category_by_id($business_category_id);
        $data['pg_menu'] = 'Home';
        $data['pg_title'] = 'Slider Home';
        $this->template->load('Admin', 'slider_home/edit', $data);
    }

    public function edit_act()
    {
        $id = $this->input->post('id');
        $slide_row =  $this->slider_home->get_by_id($id);
        $title1 = $this->input->post('title1');
        $title2 = $this->input->post('title2');
        $title3 = $this->input->post('title3');
        $title4 = $this->input->post('title4');
        $description = $this->input->post('description');
        $start_date = date_create($this->input->post('start_date'));
        $finish_date = date_create($this->input->post('finish_date'));
        $is_active = $this->input->post('is_active');
        // $is_admit = $this->input->post('is_admit');
        // if ($is_admit == NULL) {
        //     $is_admit = 0;
        // }

        $this->form_validation->set_rules('title1', 'Judul 1', 'required', array(
            'required' => '%s wajib diisi',
        ));
        $this->form_validation->set_rules('description', 'Deskripsi', 'required', array(
            'required' => '%s wajib diisi',
        ));
        $this->form_validation->set_rules('start_date', 'Tanggal Mulai', 'required', array(
            'required' => '%s wajib diisi',
        ));
        $this->form_validation->set_rules('finish_date', 'Tanggal Selesai', 'required', array(
            'required' => '%s wajib diisi',
        ));

        if ($this->form_validation->run() != FALSE) {

            $upload_image = $_FILES['img']['name'];

            if ($upload_image) {
                $file = $this->slider_home->upload();

                if ($file['result'] == "failed") {
                    $this->session->set_flashdata('img', $file['error']);
                } else {
                    $old_img = $slide_row->img;
                    if ($old_img != 'assets/images/slide/default_sh.png') {
                        unlink(FCPATH . $old_img);
                    }
                    $data['img'] = 'assets/images/slide/' . $file['file']['file_name'];
                }
            }

            $data['title1'] = $title1;
            $data['title2'] = $title2;
            $data['title3'] = $title3;
            $data['title4'] = $title4;
            $data['description'] = $description;
            $data['start_date'] = date_format($start_date, "Y-m-d H:i:s");
            $data['finish_date'] = date_format($finish_date, "Y-m-d H:i:s");
            $data['is_active'] = $is_active;
            $data['post_by'] = $this->ion_auth->get_user_id();
            // $data['is_admit'] = $is_admit;

            $where = array(
                'id' => $id
            );

            $this->slider_home->update($where, $data);

            $this->session->set_flashdata('flash', 'edit_success');
            redirect('Slider_Home/edit/' . $id);
        } else {
            if (empty($id)) {
                $this->session->set_flashdata('flash', 'edit_failed');
                redirect('Slider_Home');
            }
            $data['groups'] = $this->_groups();
            $data['slider_row'] = $this->slider_home->get_by_id($id);
            $business_category_id = $data['slider_row']->business_category_id;
            $data['get_business_category_by_id'] = $this->slider_home->get_business_category_by_id($business_category_id);
            $data['pg_menu'] = 'Home';
            $data['pg_title'] = 'Slider Home';
            $this->template->load('Admin', 'slider_home/edit', $data);
        }
    }

    function is_admit_update()
    {
        $id = $this->input->post('id');
        $data = $this->input->post('is_admit');

        $data = array(
            'is_admit' => $data,
        );

        if ($this->slider_home->update(array('id' => $id), $data)) {

            echo json_encode(array("status" => TRUE));
        }
    }

    // function delete($id)
    // {
    //     $image = $this->slider_home->get_by_id($id);
    //     $hapus = $image->img;

    //     if (file_exists(FCPATH . $hapus)) {
    //         if ($hapus != 'assets/images/slide/default_sh.png') {
    //             unlink(FCPATH . $hapus);
    //         }
    //     }

    //     $this->slider_home->delete_by_id($id);
    //     $this->session->set_flashdata('flash', 'delete_success');
    //     redirect('Slider_Home');
    // }
}
