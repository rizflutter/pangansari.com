<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Career extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		$this->load->model('m_career');
	}

	public function index()
	{
		$s=$this->input->get('s');

		if($this->input->get('s')){
			$config['suffix'] = "?s=".$s;
		} else {
			$config['suffix'] = "";
		}

		$config['base_url'] = base_url()."career/index/";
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
	
		if($this->input->get('s')){
			$config['total_rows'] = $this->m_career->career_all($s)->num_rows();
		} else {
			$config['total_rows'] = $this->m_career->career_all()->num_rows();
		}
	
        $config['per_page']		= 5;
        $config['num_links'] 	= 5;
        $from					= $this->uri->segment(3, 0);

        //Tambahan untuk styling
        $config['first_link']	= "« Pertama";
        $config['last_link']	= "Terakhir »";
        $config['next_link']	= '»';
        $config['prev_link']	= '«';
        
        $this->pagination->initialize($config);

        if($this->input->get('s')){
        	$data['career']		= $this->m_career->career($from, $config['per_page'], $s)->result();
		} else {
       		$data['career']		= $this->m_career->career($from, $config['per_page'])->result();
		}

		$data['num_career']		= $config['total_rows'];
// 		$file_url = "http://43.225.65.102/careerpsu/api/vacancys/view";
// 		$data_careers = json_decode(file_get_contents($file_url));
// 		$data['career'] = $data_careers->records;
		$data['pg_title']	='Career';
		$this->template->load('Container', 'career/index', $data);
	}
}
