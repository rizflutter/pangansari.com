<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Career_Backend extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_career_backend', 'career_backend');
        $this->load->library(['form_validation', 'ion_auth']);
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('flash', 'deny');
            redirect('auth/login', 'refresh');
        }
    }

    // reuseable group
    private function _groups()
    {
        return $groups = ['admin', 'managers'];
    }

    public function index()
    {
        $data['groups'] = $this->_groups();
        $data['pg_title'] = 'Add Career';
        $data['pg_menu'] = 'Career';
        $this->template->load('Admin', 'career_backend/index', $data);
    }

    public function ajax_list()
    {
        $list = $this->career_backend->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $c) {
            $no++;
            $row = array();
            $row[] = $c->id;
            $row[] = $c->title;
            $row[] = $c->start_date;
            $row[] = $c->finish_date;
            $row[] = $c->full_name;
            $row[] = ($c->is_active == 1) ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Disable</span>';
            $row[] = ($c->is_admit == 1) ? '<span class="badge badge-success">Approved</span>' : '<span class="badge badge-warning">Waiting</span>';
            $row[] = $c->created_at;
            if ($this->ion_auth->in_group($this->_groups())) :
            $row[] = ($c->is_admit == 1) ? "<input type='checkbox' onclick='is_admit($c->id".','."0)' checked>" : "<input type='checkbox' onclick='is_admit($c->id".','."1)'>";
            endif;
            //add html for action
            $row[] = '<div class="btn-group"><a class="btn btn-sm btn-info" href="' . base_url('Career_Backend/show/' . $c->id) . '">Preview</a><a class="btn btn-sm btn-warning" href="' . base_url('Career_Backend/edit/' . $c->id) . '">Edit</a>
                  <a class="btn btn-sm btn-danger" href="' . base_url('Career_Backend/delete/' . $c->id) . '" onclick="return confirm(' . "'Anda yakin ingin menghapus " . $c->title . "?'" . ')">Delete</a></div>';
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->career_backend->count_all(),
            "recordsFiltered" => $this->career_backend->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function show($id)
	{
        $data['pg_title']	='Career';
        $data['career'] = $this->career_backend->get_by_id($id);
		$this->template->load('Container', 'career_backend/show', $data);
	}

    public function add()
    {
        $data['groups'] = $this->_groups();
        $data['pg_menu'] = 'Career';
        $data['pg_title'] = 'Add Career';
        $this->template->load('Admin', 'career_backend/add', $data);
    }

    public function add_act()
    {
        $title = $this->input->post('title');
        $description = htmlentities(htmlspecialchars($_POST['description']));
        $link = $this->input->post('link');
        $start_date = date_create($this->input->post('start_date'));
        $finish_date = date_create($this->input->post('finish_date'));
        $is_active = $this->input->post('is_active');

        // $is_admit = $this->input->post('is_admit');
        // if ($is_admit == NULL) {
        //     $is_admit = 0;
        // }

        $this->form_validation->set_rules('title', 'Judul', 'required', array(
            'required' => '%s wajib diisi',
        ));

        $this->form_validation->set_rules('link', 'Link', 'required', array(
            'required' => '%s wajib diisi',
        ));

        $this->form_validation->set_rules('start_date', 'Start Date', 'required', array(
            'required' => '%s wajib diisi',
        ));

        $this->form_validation->set_rules('finish_date', 'Finish Date', 'required', array(
            'required' => '%s wajib diisi',
        ));

        if ($this->form_validation->run() != FALSE) {

            $data['title'] = $title;
            $data['description'] = $description;
            $data['link'] = $link;
            $data['start_date'] = date_format($start_date, "Y-m-d H:i:s");
            $data['finish_date'] = date_format($finish_date, "Y-m-d H:i:s");
            $data['post_by'] = $this->ion_auth->get_user_id();
            $data['is_active'] = $is_active;
            // $data['is_admit'] = $is_admit;
            $data['created_at'] = date("Y-m-d H:i:s");

            $this->career_backend->save($data);
            $this->session->set_flashdata('flash', 'add_success');
            redirect('Career_Backend/add');
        } else {
            $data['groups'] = $this->_groups();
            $data['pg_menu'] = 'Career';
            $data['pg_title'] = 'Add Career';
            $this->template->load('Admin', 'career_backend/add', $data);
        }
    }

    public function edit($id)
    {
        $data['groups'] = $this->_groups();
        $data['career_row'] = $this->career_backend->get_by_id($id);
        $data['pg_menu'] = 'Career';
        $data['pg_title'] = 'Edit Career';
        $this->template->load('Admin', 'career_backend/edit', $data);
    }

    public function edit_act()
    {
        $id = $this->input->post('id');
        $where = array(
            'id' => $id
        );
        $title = $this->input->post('title');
        $description = htmlentities(htmlspecialchars($_POST['description']));
        $link = $this->input->post('link');
        $start_date = date_create($this->input->post('start_date'));
        $finish_date = date_create($this->input->post('finish_date'));
        $is_active = $this->input->post('is_active');

        // $is_admit = $this->input->post('is_admit');
        // if ($is_admit == NULL) {
        //     $is_admit = 0;
        // }

        $this->form_validation->set_rules('title', 'Judul', 'required', array(
            'required' => '%s wajib diisi',
        ));

        $this->form_validation->set_rules('link', 'Link', 'required', array(
            'required' => '%s wajib diisi',
        ));

        $this->form_validation->set_rules('start_date', 'Start Date', 'required', array(
            'required' => '%s wajib diisi',
        ));

        $this->form_validation->set_rules('finish_date', 'Finish Date', 'required', array(
            'required' => '%s wajib diisi',
        ));

        if ($this->form_validation->run() != FALSE) {

            $data['title'] = $title;
            $data['description'] = $description;
            $data['link'] = $link;
            $data['start_date'] = date_format($start_date, "Y-m-d H:i:s");
            $data['finish_date'] = date_format($finish_date, "Y-m-d H:i:s");
            $data['post_by'] = $this->ion_auth->get_user_id();
            $data['is_active'] = $is_active;
            // $data['is_admit'] = $is_admit;

            $this->career_backend->update($where, $data);
            $this->session->set_flashdata('flash', 'edit_success');
            redirect('Career_Backend/edit/' . $id);
        } else {
            if (empty($id)) {
                $this->session->set_flashdata('flash', 'edit_failed');
                redirect('Career_Backend');
            }
            $data['groups'] = $this->_groups();
            $data['pg_menu'] = 'Career';
            $data['pg_title'] = 'Edit Career';
            $this->template->load('Admin', 'career_backend/edit', $data);
        }
    }

    function delete($id)
    {
        $this->career_backend->delete_by_id($id);
        $this->session->set_flashdata('flash', 'delete_success');
        redirect('Career_Backend');
    }

    function is_admit_update()
    {
        $id = $this->input->post('id');
        $data = $this->input->post('is_admit');

        $data = array(
            'is_admit' => $data,
        );

        if($this->career_backend->update(array('id' => $id), $data)){

            echo json_encode(array("status" => TRUE));
        }
    
    }
}
