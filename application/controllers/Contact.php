<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		$this->load->library('Recaptcha');
	}

	public function index()
	{
		$data['pg_title']	='Contact';
		$data['captcha']            = $this->recaptcha->getWidget();
        $data['script_captcha']     = $this->recaptcha->getScriptTag();
		$this->template->load('Container', 'contact/index', $data);
	}

	public function send_message(){
		$recaptcha          = $this->input->post('g-recaptcha-response');
        $response           = $this->recaptcha->verifyResponse($recaptcha);

        $name 				= $this->input->post('cf-name');
        $email 				= $this->input->post('cf-email');
        $subject			= $this->input->post('cf-subject');
        $message			= $this->input->post('cf-message');

        if (!isset($response['success']) || $response['success'] <> true) {
            $this->session->set_flashdata('error', 'Silahkan Verifikai Captcha Terlebih Dahulu');
    		redirect($_SERVER['HTTP_REFERER']);
        } else {
    		$config = Array(       
	            'protocol'  => 'smtp',
	            'smtp_host'  => 'mail.pangansari.co.id',
	            'smtp_port'  => '587',
	            'smtp_user'  => 'webpsu@pangansari.co.id',
	            'smtp_pass'  => 'webpsu17102019',
	            'mailtype'  => 'html',
	            'charset'   => 'iso-8859-1'
	        );

	        $this->load->library('email', $config);
	    	$this->email->set_newline("\r\n");

            $this->email->from('webpsu@pangansari.co.id', '[no-reply]-Web PSU');      
            $this->email->to('marketing-ho@pangansari.co.id');
            //$this->email->to('ahmad.adam@pangansari.co.id');
            //$this->email->cc($email);
            $this->email->subject($subject);

            $data['message']	= $message;
            $data['subject']	= $subject;
            $data['email']		= $email;
            $data['name']		= $name;

            $body_email			= $this->load->view('email/email_contact.php', $data, TRUE);
            $this->email->message($body_email);
            
            if($this->email->send()){
            	$this->session->set_flashdata('success', 'Message sent');
				redirect($_SERVER['HTTP_REFERER']);
            } else {
            	$this->session->set_flashdata('error', 'Failed to send message ');
				redirect($_SERVER['HTTP_REFERER']);
            }
        }
	}
}
