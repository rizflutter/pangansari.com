<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Company_Profile extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_company_profile', 'company_profile');
        $this->load->library(['form_validation', 'ion_auth']);
        if (!$this->ion_auth->logged_in()) {
            $this->session->set_flashdata('flash', 'deny');
            redirect('auth/login', 'refresh');
        }
    }

    // reuseable group
    private function _groups()
    {
        return $groups = ['admin', 'managers'];
    }

    public function index()
    {
        $data['groups'] = $this->_groups();
        $data['pg_menu'] = 'Home';
        $data['pg_title'] = 'Company Profile';
        $this->template->load('Admin', 'company_profile/index', $data);
    }

    public function ajax_list()
    {
        $list = $this->company_profile->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $cp) {
            $no++;
            $row = array();
            $row[] = $cp->document_name;
            $row[] = $cp->document_file;
            $row[] = $cp->full_name;
            $row[] = ($cp->is_active == 1) ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Disable</span>';
            $row[] = ($cp->is_admit == 1) ? '<span class="badge badge-success">Approved</span>' : '<span class="badge badge-warning">Waiting</span>';
            $row[] = ($cp->is_posting == 1) ? '<span class="badge badge-success">Posted</span>' : '<span class="badge badge-warning">Waiting</span>';
            $row[] = $cp->created_at;
            if ($this->ion_auth->in_group($this->_groups())) :
            $row[] = ($cp->is_admit == 1) ? "<input type='checkbox' onclick='is_admit($cp->id".','."0)' checked>" : "<input type='checkbox' onclick='is_admit($cp->id".','."1)'>";
            endif;
            //add html for action
            $row[] = '<div class="btn-group"><a class="btn btn-sm btn-warning" href="' . base_url('Company_Profile/edit/' . $cp->id) . '">Edit</a>
                  <a class="btn btn-sm btn-danger" href="' . base_url('Company_Profile/delete/' . $cp->id) . '" onclick="return confirm(' . "'Anda yakin ingin menghapus " . $cp->document_name . "?'" . ')">Delete</a></div>';
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->company_profile->count_all(),
            "recordsFiltered" => $this->company_profile->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    // public function show($id)
	// {
    //     $data['pg_title']	='Company Profile';
    //     $data['about_us'] = $this->company_profile->get_by_id($id);
	// 	$this->template->load('Container', 'company_profile/show', $data);
	// }

    public function add()
    {
        $data['groups'] = $this->_groups();
        $data['pg_menu'] = 'Company Profile';
        $data['pg_title'] = 'Home';
        $this->template->load('Admin', 'company_profile/add', $data);
    }

    public function add_act()
    {
        $document_name = $this->input->post('document_name');
        $is_active = $this->input->post('is_active');
        $is_posting = $this->input->post('is_posting');
    
        if ($is_posting == NULL) {
            $is_posting = 0;
        }
        
        if($is_posting == 1){
            $where = ['is_posting' => 1];
            $data = ['is_posting' => 0];
            $this->company_profile->update($where,$data);
        }

        $this->form_validation->set_rules('document_name', 'Dokumen', 'required', array(
            'required' => '%s wajib diisi',
        ));

        if ($this->form_validation->run() != FALSE) {

            $upload_file = $_FILES['document_file']['name'];

            if ($upload_file) {
                $file = $this->company_profile->upload();

                if ($file['result'] == "failed") {
                    $this->session->set_flashdata('document_file', $file['error']);
                    
                } else {
                    $data['document_file'] = 'assets/company_profile/' . $file['file']['file_name'];
                }
            }

            $data['document_name'] = $document_name;
            $data['id_user'] = $this->ion_auth->get_user_id();
            $data['is_posting'] = $is_posting;
            $data['is_active'] = $is_active;
            $this->company_profile->save($data);
            $this->session->set_flashdata('flash', 'add_success');
            redirect('Company_Profile/add');
        } else {     
            $data['groups'] = $this->_groups();      
            $data['pg_menu'] = 'Home';
            $data['pg_title'] = 'Company Profile';
            $this->template->load('Admin', 'company_profile/add', $data);
        }
    }

    public function edit($id)
    {
        $data['groups'] = $this->_groups();
        $data['company_profile_row'] = $this->company_profile->get_by_id($id);
        $data['pg_menu'] = 'Home';
        $data['pg_title'] = 'Company Profile';
        $this->template->load('Admin', 'company_profile/edit', $data);
    }

    public function edit_act()
    {
        $id = $this->input->post('id');
        $company_profile_row =  $this->company_profile->get_by_id($id);
        $document_name = $this->input->post('document_name');
        $is_active = $this->input->post('is_active');
        $is_posting = $this->input->post('is_posting');
        // if ($is_admit == NULL) {
        //     $is_admit = 0;
        // }
        if ($is_posting == NULL) {
            $is_posting = 0;
        }
        
        if($is_posting == 1){
            $where = ['is_posting' => 1];
            $data = ['is_posting' => 0];
            $this->company_profile->update($where,$data);
        }

        $this->form_validation->set_rules('document_name', 'Dokumen', 'required', array(
            'required' => '%s wajib diisi',
        ));
        
        if ($this->form_validation->run() != FALSE) {

            $upload_file = $_FILES['document_file']['name'];

            if ($upload_file) {
                $file = $this->company_profile->upload();

                if ($file['result'] == "failed") {
                    $this->session->set_flashdata('document_file', $file['error']);
                    
                } else {
                    $old_file = $company_profile_row->document_file;
                    
                    if (file_exists(FCPATH . $old_file)) {
                        unlink(FCPATH . $old_file);
                    }
                    
                    $data['document_file'] = 'assets/company_profile/' . $file['file']['file_name'];
                }
            }

            $data['document_name'] = $document_name;
            $data['id_user'] = $this->ion_auth->get_user_id();
            $data['is_posting'] = $is_posting;
            $data['is_active'] = $is_active;

            $where = array(
                'id' => $id
            );

            $this->company_profile->update($where, $data);
            $this->session->set_flashdata('flash', 'edit_success');
            redirect('Company_Profile/edit/' . $id);
        } else {
            if (empty($id)) {
                $this->session->set_flashdata('flash', 'edit_failed');
                redirect('Company_Profile');
            }
            $data['groups'] = $this->_groups();
            $data['company_profile_row'] = $this->company_profile->get_by_id($id);
            $data['pg_menu'] = 'Home';
            $data['pg_title'] = 'Company Profile';
            $this->template->load('Admin', 'company_profile/edit', $data);
        }
    }

    function delete($id)
    {
        $file = $this->company_profile->get_by_id($id);
        $hapus = $file->document_file;

        if (file_exists(FCPATH . $hapus)) {
                unlink(FCPATH . $hapus);
        }

        $this->company_profile->delete_by_id($id);
        $this->session->set_flashdata('flash', 'delete_success');
        redirect('Company_Profile');
    }

    function is_admit_update()
    {
        $id = $this->input->post('id');
        $data = $this->input->post('is_admit');

        $data = array(
            'is_admit' => $data,
        );

        if($this->company_profile->update(array('id' => $id), $data)){

            echo json_encode(array("status" => TRUE));
        }
    
    }
}
