<?php

// To use reCAPTCHA, you need to sign up for an API key pair for your site.
// link: http://www.google.com/recaptcha/admin
$config['recaptcha_site_key'] = '6LdywY0aAAAAAAHNU9IlscC5QbTIA_whpZ6gUFHG';
$config['recaptcha_secret_key'] = '6LdywY0aAAAAAH24Fjnjf3hOKv69mGeS0hr4G8N1';

// reCAPTCHA supported 40+ languages listed here:
// https://developers.google.com/recaptcha/docs/language
$config['recaptcha_lang'] = 'en';

/* End of file recaptcha.php */
/* Location: ./application/config/recaptcha.php */
