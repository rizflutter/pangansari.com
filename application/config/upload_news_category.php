<?php 

// konfigurasi
$config['upload_path']          = FCPATH . 'assets\images\news\\';
$config['allowed_types']        = 'gif|jpg|png';
$config['max_size']             = 100;
$config['max_width']            = 88;
$config['max_height']           = 88;
