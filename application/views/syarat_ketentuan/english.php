<section id="news-list" class="section-margine blog-list">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="news-list-box">
                    <h1 class="text-center">TERMSANDCONDITIONSOFPURCHASEORDER PRODUCTS AND/ORSERVICES</h1>
                    <div style="text-align: justify;">
                        <hr style="border: 1px solid #e7e7e7;">
                        <br>
                        <h4>General Provisions</h4>
                        <ol>
                            <li>To verify an order for a Product and/or Service, this Purchase Order is sent via facsimile or email to the Supplier (the original Purchase Order to be sent by the Company to the Supplier by request). Upon the Purchase Order being
                                received via facsimile or email from the Company, the Supplier is deemed to approve and be subject to the terms and conditions of this Purchase Order and other terms and conditions agreed upon in writing between the Company
                                and the Supplier. The Supplier must send to the Company one copy of the Purchase Order that has been signed by the officer authorized to represent the Supplier via facsimile or email within 3 (three) working days from when the
                                Purchase Order is sent by the Company. Within the following 3 (three) working days, the Supplier shall then courier to the Company the original of the executed Purchase Order. This Purchase Order is binding. In the event there is
                                an objection and/or rebuttal to any of the terms and conditions of this Purchase Order, the Supplier shall raise its objection and/or rebuttal in writing to the Company by: (i) no later than 30 (thirty) days as of the date of issuance of this
                                Purchase Order; or (ii) before delivering the Product and/or Service required under this Purchase Order, whichever comes first.</li>
                            <br>
                            <li>Tender has been completed in accordance with the provisions of the Company.</li>
                            <br>
                            <li>All documents and correspondence in connection with this Purchase Order must specify this Purchase Order number.</li>
                        </ol>
                        <br>
                        <h4>Price</h4>
                        <ol start="4">
                            <li>The price of the Product and/or Service stated in this Purchase Order shall be the price as agreed upon in writing by the Company and the Supplier.</li>
                            <br>
                            <li>In the case of Purchase Order of a Product, the price of the Product includes delivery until the Product is received at the shipping address stated on the face of the Purchase Order, unless provided otherwise in writing by the Company.</li>
                        </ol>
                        <br>
                        <h4>Terms of Payment</h4>
                        <ol start="6">
                            <li>In the event the Supplier of Products places advertisements of the Company: (a) The Supplier shall pay taxes, charges, fees and penalties imposed under the local regulations concerning advertisements of the Company included in
                                the Purchase Order. The receipts of payments therefor must be submitted to the Company to corroborate the Supplier’s invoices; (b) The Supplier must ensure that the advertisements of the Company included in the Purchase
                                Order shall be disseminated and displayed in accordance with the prevailing local regulations and consistent with the licenses/permits and taxes paid. The Supplier must provide the government employees with the correct information
                                on the types, quantities, size, locations, and period for which the advertisements of the Company are placed and other data in connection with the acquisition of the advertising permits, tax collection, personnel or the requisite
                                documents of the Company; and (c) Any offer, payment in cash or valuable gift-giving by the Supplier to third parties in connection with the provision of Products must be described in the invoices in detail along with the date, quantities
                                and reasons.</li>
                            <br>
                            <li>The due dates of payment of each staged payment for the prices of Products and/or Services shall be subject to the terms of payment time prevailing in the accounting department of the Company, i.e. the Company shall make
                                payments on Wednesdays of the week. In the event the due date for payment not falling on Wednesday, the payment shall be made on the first Wednesday following the due date for payment, provided however that (a) the Products
                                ordered have been received and/or the Services have been completed according to the Data and Design of the Products and/or Services and other specifications required in this Purchase Order, and/or the Services completed have
                                been received by the Company and (b) the original invoices, tax collection, and receipts of payments for charges or advertising tax and other documents required by the Company and the correction of invoices (in case any correction
                                is required by the Company) have been received by the Company from the Supplier, unless provided otherwise on the face of this Purchase Order.
                            </li>
                        </ol>
                        <br>
                        <h4>Taxation</h4>
                        <ol start="8">
                            <li>The Supplier agrees that if the payment belongs to the type of payment is subject to the Income Tax (PPh) withholding as provided in the provisions of Taxation in Indonesia, the Company shall withhold the Income Tax (PPh), the
                                amount of which shall be determined as provided in the provisions of Taxation in Indonesia, and therefore the Supplier is entitled to receive the original withholding Income Tax (PPh) slip. A foreign Supplier who is a foreign taxpayer
                                must submit a Certificate of Domicile for the purpose of tax exemption and/or allowance under the Agreement for the Avoidance of Double Taxation between the Government of Indonesia and the country in which the Supplier is
                                domiciled.
                            </li>
                            <br>
                            <li>The Supplier agrees that in the event the Supplier has collected VAT and issue a Tax Invoice to the Company but the Supplier has not yet been confirmed as a Taxable Entrepreneur and/or fails to return an issued Tax Invoice to the
                                Taxation Office (KPP) as governed by the Taxation Law prevailing in Indonesia, the Supplier shall be liable to incur all the invoices or tax assessment (Principles and administrative Sanctions/interest) that may be imposed by the
                                Taxation Office (KPP) on the Company for crediting a Tax Invoice issued by the Supplier.
                            </li>
                        </ol>
                        <br>
                        <h4>Products and Delivery of Products and/or Services and Completion of Services</h4>
                        <ol start="10">
                            <li>The Products and/or Services must be delivered to the Company at the shipping address as stated on the face of the Purchase Order. The Products and/or Services must be delivered in a good state of repair and/or completed in an
                                appropriate and timely manner in accordance with the terms of this Purchase Order and/or the Data and Design of the Product. The shipping notice covering the Product must specify the Purchase Order (PO) number, quantities,
                                description of the Product and the Company code number, if required.</li>
                            <br>
                            <li>Under no circumstances, including but not limited to the Force Majeure, shall the Supplier change the specifications of the Product and/or Service, the date of delivery of the Product to the Company and/or the date of the completion
                                of the Service, the price of the Product and/or Service, the point of delivery of the Product and/or the completion of the Service, the data or procedures for payment without the written consent of the Company.</li>
                            <br>
                            <li>In the event that the business activities of the Company are disrupted, prevented or delayed in production due to circumstances beyond the control of the Company, the Company may decide to push back the date or dates of
                                delivery of the Products and/or the completion of the Services. In the event a Product has not yet been delivered and/or a Service has not yet been completed by the Supplier to the Company at an agreed upon time for any reason
                                whatsoever, the Company shall have discretion to cancel the whole or any part of the order without any obligation to give any compensation.</li>
                            <br>
                            <li>The Company shall not be liable for any order unless it has been confirmed and stated in this Purchase Order (PO) or other documents (if any) that are authentic and printed-out and signed by the Authorized Officer of the Company.</li>
                        </ol>
                        <br>
                        <h4>Inspection and Rejection</h4>
                        <ol start="14">
                            <li>In the case of Purchase Order of a Product, the Company shall be entitled to inspect the Product during the manufacturing process and at the time of receipt by the Company. In the event that the Product delivered to the Company
                                not in a good state of repair and in due time, and in accordance with the terms of this Purchase Order and/or the Data and Design of the Product, or the Product is delivered in a damaged or defective state of repair, the Company
                                may return that Product and/or request the Supplier to repair and replace the Product entirely at the Supplier's expenses, or cancel the order of the Product without any obligation to pay the price of the Product whose order has been
                                canceled, or compensation.</li>
                            <br>
                            <li> In the case of Purchase Order of a Service, if the Service is not completed in an appropriate and timely manner in accordance with the terms of this Purchase Order and/or the Data and Design of the Service, the Company may, at
                                the Supplier's expenses, request the Supplier to promptly repair or reach the completion of the Service, or cancel the order of the Service without any obligation of the Company to pay the price of the Service whose order has been
                                canceled, or compensation.</li>
                        </ol>
                        <br>
                        <h4>Warranties of the Supplier</h4>
                        <ol start="16">
                            <li>The Supplier warrants that: <br><br>
                                (a) The Products and/or Services shall conform with the specifications, descriptions or Sample Proof (Products only) as required in this Purchase Order and/or the Data and Design of the Product and Service and be delivered in a
                                timely manner and on schedule as determined by the Company;
                                <br>
                                (b) The Products and/or Services shall conform with the specifications, descriptions or Sample Proof (Products only) as required in this Purchase Order and/or the Data and Design of the Product and Service and be delivered in a
                                timely manner and on schedule as determined by the Company;
                                <br>
                                (c) The Products and/or Services shall be of the best quality and/or the best standards for the delivery of the Products and/or the completion of the Services of the similar type;
                                <br>
                                (d) The Products and/or Services have complied with the prevailing regulations;
                                <br>
                                (e) The Products are clear of liens or encumbrances; and
                                <br>
                                (f) The Product is not in a damaged or defective state of repair.
                                <br>
                                The warranties shall be an addition to the warranties provided by the Supplier in respect of the Products and/or Services and other warranties as governed by the prevailing laws and regulations.
                            </li>
                        </ol>
                        <br>
                        <h4>Transfer of Ownership and Product Risks</h4>
                        <ol start="17">
                            <li>No Product ownership and risks shall pass to the Company until the Product is delivered to the Company, subject to the inspection and rejection as provided in point 14.</li>
                        </ol>
                        <br>
                        <h4>Confidential Information</h4>
                        <ol start="18">
                            <li>The Supplier must maintain the confidentiality of any and all the Confidential Information provided by the Company in connection with the order and manufacture of a Product and/or the order of a Service and as soon as the Product
                                is delivered to the Company and/or the Service is completed, the Supplier must return the Confidential Information to the Company as soon as practical. This term also applies to directors, commissioners, employees, agents,
                                representatives and subcontractors of the Supplier.</li>
                        </ol>
                        <br>
                        <h4>Failure of Timely Delivery of Products and/or Completion of Services</h4>
                        <ol start="19">
                            <li>Time is of the essence in the order. Failure of the Supplier to deliver the Products and/or complete the Services within the time specified in the Purchase Order, either in part or in whole, unless the Supplier can prove that the failure
                                is caused by the Force Majeure, shall be imposed the following sanctions: <br><br>
                                (a) Every day’s delay shall be assessed a penalty of 2‰ (two per mile) of the price of the Product delayed in delivery and/or of the price of the Service delayed in completion, with a maximum penalty of 5% (five percent) of the price
                                of the Product and/or the price of the Service.
                                <br>
                                (b) Notwithstanding the term of point 22 below, if the delay exceeds more than 30 (thirty) consecutive calendar days, this Purchase Order may be canceled unilaterally by the Company by a written notice to the Supplier without any
                                obligation to give any compensation, and such cancellation shall not affect the obligation of the Supplier to pay the penalty as referred to in point 19 (a) above.
                                <br><br>
                                In the event that the whole and/or any part of the payment has been made to the Supplier, the Supplier must refund all the payments that have been received without any deduction within 3 (three) working days of the date of the
                                notice is sent by the Company to the Supplier regarding the cancellation. </li>
                        </ol>
                        <br>
                        <h4>Compensation</h4>
                        <ol start="20">
                            <li>Notwithstanding any prevailing laws, in the event that the Product is delivered or the Product is in use by the Company and/or in the event the Service is completed other than in accordance with and not in a timely manner as
                                provided in the terms of this Purchase Order and/or the Data and Design of the Service and/or any specification specified in this Purchase Order, or in the event there are damages, damage, loss, or defects of goods and/or
                                injury, or death of the employee of the Company and/or the third party due to the use of the Product and/or the use of the Service other than in accordance with the Purchase Order or there is failure of the Supplier and/or the
                                employees, agents, and subcontractors of the Supplier, the Company may demand the Supplier to repair and/or give compensation for the damages, damage, loss, defects, injury, or death.</li>
                            <br>
                            <li>The Company shall not be liable for any damages, damage, loss, defects, injury, or death of the Supplier, employees, agents or subcontractors of the Supplier, either directly or indirectly, in connection with the performance of
                                this Purchase Order by the Supplier.
                            </li>
                            <br>
                            <li>The Supplier shall be liable to give compensation to the Company at any time for any and all expenses, damages or liabilities (inability) incurred, including legal service fees and other expenses incurred by the Company as a
                                result of the Supplier’s violation of the terms and conditions of the Purchase Order, or as a result of the failure or non-performance of the Supplier or its representatives or employees, or as a result of an action or anything
                                performed or nothing performed by the Supplier or its representatives or employees in connection with the provision of Products and/or Services.
                            </li>
                        </ol>
                        <br>
                        <h4>Transfer and Subcontracting</h4>
                        <ol start="23">
                            <li>No order of the Products and/or Services shall be transferred by the Supplier to third parties. The Supplier shall not be allowed to subcontract the production or provision of the whole or any part of the Products and/or Services
                                without a written consent of the Company. In the event the Company consents the appointment of the subcontractor, the Supplier shall, unless otherwise agreed upon in writing by the Company and the Supplier, remain responsible
                                to fulfill all the obligations and liabilities of the Supplier and/or the subcontractors in a timely manner (without any exceptions) under this Purchase Order.
                            </li>
                        </ol>
                        <br>
                        <h4>Intellectual Property Rights</h4>
                        <ol start="24">
                            <li>
                                <ol type="a">
                                    <li>The Supplier warrants that the no provision of Products and/or Services under this Purchase Order constitutes an infringement of the intellectual property rights of the third parties and/or the Company.</li>
                                    <br>
                                    <li>All information (including illustrations, drawings, tools, samples and specifications) provided by the Company to the Supplier shall remain the property of the Company and must be treated as Confidential Information by the
                                        Supplier. Such information may solely be used by the Supplier to prepare an offer price or complete the Purchase Order for the Company. The Supplier shall not reproduce or copy, sell, lend or otherwise transfer or use
                                        such information without the prior written consent of the Company. All illustrations, drawings, tools, samples and specifications provided to the Supplier must be returned to the Company once requested by the Company.</li>
                                    <br>
                                    <li>The Supplier shall not use any trademark, trade name, slogan or logo of the Company or the affiliated parties of the Company without the special written consent of the Company.</li>
                                </ol>
                            </li>
                        </ol>
                        <br>
                        <h4>The Code of Conduct of the Company
                        </h4>
                        <ol start="25">
                            <br>
                            <li>The Supplier represents that in the course of the provision of the Products and/or Services it has no and will not have conflict of interest that would affect the Supplier's performance to provide the Products and/or Services or
                                may give the impression of ineligibility in respect of the provision of the Products and/or the Services by the Supplier. The Supplier further represents and warrants that it or the owner, colleagues or officers, directors or employees
                                affiliated with the Supplier are not or will not be officers or employees of a government agency or a body controlled by the government or of an international public organization or a person acting in his/her official capacity for or
                                on behalf of the parties mentioned above or officials of a political party or candidate for a political position, in the course of provision of the Products and/or Services without the prior written consent of the Company.
                            </li>
                            <br>
                            <li>The Supplier shall not authorize, offer, promise or give any payments or anything else of value, through any means whatsoever, (i) to any Government Official or to any other person with the knowledge that all or any portion of
                                the thing of value will be offered, promised or given to a Government Official for the purpose of influencing official action to obtain or retain business or secure any improper advantage, or to reward such an act, or (ii) to any
                                person (whether or not a Government Official) to influence that person to act in breach of a duty of good faith, impartiality or trust, or to reward such an act. This includes a prohibition on offering or making “facilitation” payments.
                                Facilitation payments are small payments to Government Officials to expedite or secure the performance of routine government action (actions that are ordinarily and commonly performed). Examples include payments to speed
                                up issuing of legitimate visas; licenses or permits; and to connect telephones or other utility services. For the avoidance of doubt and without limiting the generality of the foregoing warranty, the Supplier further warrants that it
                                shall not make a gift or political contribution in cash or in kind to, nor shall it entertain, any Government Official or any other persons on behalf of the Company without the prior written approval of the Company, and that all such
                                approved gifts, entertainment and contributions will be accurately recorded in its books and records and will not be reimbursed by the Company without having received the necessary approvals from the Company. For the
                                purposes of this paragraph 26, a person shall be deemed to have “knowledge” with respect to conduct, circumstances or results if such person is aware of (1) the existence of or (2) a high probability of the existence of such
                                conduct, circumstances or results.</li>
                            <br>
                            <li>No part of the payment by the Company to the Supplier may be used, directly or indirectly, or in any manner whatsoever, (i) for any purpose that violates the laws of the country in which the Products and/or the Services are to
                                be provided, the countries in which the Company and the Supplier are established, or any other country whose laws may apply to any of the parties or its affiliates; (ii) to benefit from any government employees; or (iii) for
                                unlawful, unethical or improper purposes whether or not related to this Purchase Order, and the Supplier warrants that it will not use the funds in a manner that breaches these terms.</li>
                            <br>
                            <li>
                                The Supplier must ensure that its employees, agents, representatives and subcontractors with legal authority from or in connection with the Supplier in connection with this Purchase Order, read and comply with the standards of
                                conduct specified in point 25 to point 31. The Supplier also agrees to comply with and must cause its employees, agents, representatives and subcontractors to comply with the policy or code of business conduct that have been
                                notified or will be notified at later date by the Company to the Supplier.
                            </li>
                            <br>
                            <li>
                                The Supplier does and shall not engage a third party: <br><br>
                                (a) to provide Benefits that may affect the loyalty and objectivity of any employee of the Company in making a decision for and on behalf of the Company. No gift-giving in the form of travel and
                                <br>
                                (b) Plodging facilities is allowed.
                                <br>
                                (c) to enter into business relationship with the Company if the Company's employees have Substantial Interests within the Supplier’s organization and have the capability to determine to choose or terminate the Supplier, or
                                the terms of this Purchase Order between the Supplier and the Company.
                                <br>
                                (d) to bribe, give covert commission or other unauthorized or improper compensation to any party. <br>
                                (e) to conduct unauthorized duplication of software belonging to or licensed to the Company.
                                <br>
                                The Supplier must immediately notify the Company if the Supplier becomes aware of the violation of this point.
                                In this point, Benefits means any payment, loan, service, gratuity, money, gift, entertainment (socially unusual and improper) or other patronage. Substantial Interests means economic, personal or family interests that may affect
                                or reasonably be deemed to affect a decision or act.
                            </li>
                            <br>
                            <li>All financial statements, records, and invoices submitted by the Supplier to the Company must, in reasonable detail, accurately and honestly, indicate the activities and transactions relating to the Company's account. The
                                Supplier must keep and maintain complete and accurate books, records of account, other reports and data necessary for the proper administration of this Purchase Order for a period of 5 (five) years upon the expiration or the
                                expiry date of this Purchase Order.</li>
                            <br>
                            <li>PThe Company is entitled to appoint its internal and/or independent auditors to audit during the normal business hours the financial records and books of the Supplier in relation to the performance duties of the Supplier by
                                reasonable prior notice. The Company may exercise its right to audit twice a year in the course of the provision of the Products and/or Services and once 12 (twelve) months immediately upon the expiration or the expiry date of
                                the provision of the Products and/or Services.</li>
                            <br>
                            <li>The Supplier represents that neither it, nor any of its affiliates (nor any director, officer or to its knowledge, employee of it or any of its affiliates) nor any of its agents, is a person, or is owned or controlled by a person that is (i) the
                                subject of any Sanctions; (ii) engaged in any activities that could trigger a designation under the Sanctions administered by the relevant authorities or (iii) employs, uses, procures or subcontracts any workers or labor originating
                                from or attributable to North Korea.
                            </li>
                            <br>
                            <li><b>Sanctions</b> means any economic or financial sanctions or trade embargoes implemented, administered or enforced by the U.S. Department of the Treasury’s Office of Foreign Assets Control, the U.S. Departments of State or
                                Commerce or any other US government authority, the United Nations Security Council, the European Union, Switzerland or other such Sanctions authority in a jurisdiction of relevance to this Purchase Order</li>
                        </ol>
                        <br>

                        <h4>Miscellaneous Provisions</h4>
                        <ol start="33">
                            <li>The terms and conditions of this Purchase Order are made in the Indonesian language and the English language. The translation of this Purchase Order into a foreign language is not binding on the Company and the Supplier.</li>
                            <br>
                            <li>Any matters not provided for by this Payment Order may be further governed individually by an agreement made in writing between the Company and the Supplier.</li>
                            <br>
                            <li>
                                The Company may, at its own discretion, change or cancel its order of the Products and/or Services before the Products and/or Services are delivered to the Company, and the Supplier must immediately use any reasonable
                                effort to alleviate the expenses arising from such change or cancellation. The Company's liability shall be limited to the expenses borne directly by the Supplier up to the date of cancellation, and the Company shall not be liable
                                in any manner whatsoever to the indirect loss due to the consequential loss or loss of profit due to such change or cancellation.
                            </li>
                            <br>
                            <li>
                                The Company and the Supplier agree to waive the provisions of Article 1266 of the Civil Code that requires a court decision to terminate an agreement.
                            </li>
                            <br>
                            <li>
                                This Purchase Order is governed by the laws of the Republic of Indonesia
                            </li>
                            <br>
                            <li>
                                The terms and conditions of this Purchase Order of the Products and/or Services shall be an inseparable and complementary part of the terms and conditions of the contract made in writing by and between the Company and
                                the Supplier (if any). In the event of any inconsistency between the Purchase Order of the Products and/or Services and the contract made in writing, the written contract shall prevail
                            </li>
                        </ol>
                    </div>
                    <hr style="border: 1px solid #e7e7e7;">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="news-list-box">
                    <div class="news-list-box-pager text-right">
                        <a class="btn btn-info" href="<?= base_url('Syarat_Ketentuan'); ?>">Bahasa</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>