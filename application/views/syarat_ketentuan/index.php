<section id="news-list" class="section-margine blog-list">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="news-list-box">
                    <h1 class="text-center">KETENTUAN DAN SYARAT-SYARAT ORDER PEMBELIAN PRODUK DAN/ATAU JASA </h1>
                    <div style="text-align: justify;">
                        <hr style="border: 1px solid #e7e7e7;">
                        <br>
                        <h4>Ketentuan Umum </h4>
                        <ol>
                            <li>Sebagai bukti pemesanan Produk dan/atau Jasa. Order Pembelian ini dikirimkan melalui faksimili atau email kepada Pemasok (Order Pembelian asli akan dikirimkan oleh Perusahaan kepada Pemasok (jika diminta). Dengan diterimanya salinan Order Pembelian tersebut melalui faksimili atau email dari Perusahaan, Pemasok dianggap menyetujui dan tunduk pada ketentuan dan syarat-syarat Order Pembelian ini dan ketentuan dan syarat-syarat lain yang disepakati secara tertulis antara Perusahaan dan Pemasok. Pemasok harus mengirimkan kepada Perusahaan satu salinan dari Order Pembelian yang telah ditandatangani oleh pejabat yang berwenang untuk mewakili Pemasok dalam waktu 3 (tiga) hari kerja sejak tanggal dikirimnya Order Pembelian oleh Perusahaan. Dalam waktu 3 (tiga) hari kerja berikutnya, Pemasok kemudian harus mengirimkan kepada Perusahaan dokumen asli dari Order Pembelian yang telah ditandatangani. Order Pembelian ini bersifat mengikat. Apabila terdapat keberatan dan/atau sanggahan terhadap syarat dan ketentuan apa pun dari Order Pembelian ini, Pemasok harus menyampaikan keberatan dan/atau sanggahannya tersebut secara tertulis kepada Perusahan paling lambat pada: (i) 30 (tiga puluh) hari sejak tanggal dikeluarkannya Order Pembelian ini; atau (ii) saat sebelum menyerahkan Produk dan/atau Jasa sebagaimana diminta berdasarkan Order Pembelian ini, yang mana yang lebih dahulu.</li>
                            <br>
                            <li>Tender telah dilakukan sesuai dengan ketentuan Perusahaan. </li>
                            <br>
                            <li>Semua dokumen dan surat menyurat yang berkaitan dengan Order Pembelian ini harus menyebutkan Order Pembelian ini.</li>
                        </ol>
                        <br>
                        <h4>Harga</h4>
                        <ol start="4">
                            <li>Harga Produk dan/atau Jasa yang tercantum dalam Order Pembelian ini adalah harga kesepakatan tertulis oleh Perusahaan dan Pemasok. </li>
                            <br>
                            <li>Dalam hal Order Pembelian Produk, harga Produk sudah termasuk sampai Produk diterima pada alamat pengiriman sebagaimana tercantum dalam halaman muka Order Pembelian, kecuali apabila dinyatakan lain secara tertulis oleh Perusahaan. </li>
                        </ol>
                        <br>
                        <h4>Ketentuan Pembayaran</h4>
                        <ol start="6">
                            <li>Khusus untuk Pemasok Produk untuk penempatan reklame-reklame Perusahaan: (a) Pemasok akan membayar seluruh pajak-pajak, retribusi-retribusi, biaya-biaya dan denda-denda yang dikenakan berdasarkan hukum dan perundang-undangan lokal sehubungan dengan reklame-reklame Perusahaan termasuk dalam Order Pembelian. Bukti-bukti pembayaran tersebut wajib diberikan kepada Perusahaan untuk mendukung tagihan Pemasok; (b) Pemasok wajib memastikan bahwa reklame-reklame Perusahaan yang termasuk dalam Order Pembelian akan disebarkan atau diperlihatkan sesuai dengan peraturan lokal yang berlaku dan konsisten dengan izin-izin dan pajak yang dibayar. Pemasok wajib menyampaikan kepada pegawai pemerintah informasi yang benar mengenai jenis, jumlah, ukuran, lokasi dan jangka waktu penempatan reklame Perusahaan dan data lain sehubungan dengan perolehan izin reklame, tagihan pajak atau dokumen-dokumen yang mungkin disyaratkan bagi Perusahaan; dan (c) Setiap penawaran, pembayaran tunai atau pemberian hadiah yang berharga oleh Pemasok kepada setiap pihak ketiga sehubungan dengan penyediaan Produk harus dijelaskan secara rinci di dalam tagihan dengan menyebutkan tanggal, jumlah dan alasannya.</li>
                            <br>
                            <li>Tanggal-tanggal jatuh tempo pembayaran setiap tahapan pembayaran harga Produk dan/atau Jasa tunduk pada ketentuan waktu pembayaran yang berlaku di bagian akunting Perusahaan, yaitu bahwa Perusahaan melakukan pembayaran setiap hari Rabu setiap minggunya. Bilamana tanggal-tanggal jatuh tempo pembayaran tersebut jatuh tidak tepat pada hari Rabu, maka pembayaran akan dilakukan pada hari Rabu pertama setelah tanggal jatuh tempo pembayaran tersebut dan dengan ketentuan (a) Produk yang dipesan diterima dan/atau Jasa dilaksanakan secara lengkap sesuai dengan Data dan desain Produk dan/atau Jasa dan spesifikasi lain yang disyaratkan dalam Order Pembelian ini, dan/atau pelaksanaan Jasa telah diterima oleh Perusahaan dan (b) asli tagihan lengkap, tagihan pajak, bukti pembayaran retribusi atau pajak reklame dan dokumen-dokumen lain yang disyaratkan oleh Perusahaan dan koreksi atas tagihan (dalam hal koreksi diminta Perusahaan) telah diterima oleh Perusahaan dan Pemasok, kecuali ditentukan lain pada halaman muka Order Pembelian ini.</li>
                        </ol>
                        <br>
                        <h4>Perpajakan</h4>
                        <ol start="8">
                            <li>Pemasok sepakat bahwa apabila terhadap pembayaran merupakan jenis pembayaran yang wajib dilakukan pemotongan Pajak Penghasilan (PPh) sebagaimana ditentukan dalam ketentuan Perpajakan di Indonesia maka Perusahaan akan melakukan pemotongan Pajak Penghasilan (PPh) yang besarnya sebagaimana ditentukan dalam ketentuan Perpajakan di Indonesia, yang demikian itu Pemasok berhak menerima asli bukti pemotongan PPh dimaksud.Pemasok asing yang merupakan wajib pajak asing harus memberikan Surat Keterangan Domisili untuk tujuan pembebasan dan/atau pengurangan tarif pajak berdasarkan Perjanjian Penghindaran Pajak berganda antara Pemerintah Indonesia dan negara tempat kedudukan Pemasok.</li>
                            <br>
                            <li>Pemasok sepakat dalam hal Pemasok telah menagih PPN dan menerbitkan faktur pajak kepada Perusahaan namun melakukan kelalaian yaitu belum dikukuhkan sebagai Pengusaha Kena Pajak dan/atau tidak melaporkan Faktur Pajak yang diterbitkan tersebut ke Kantor Pelayanan Pajak (KPP) sebagaimana diatur di dalam Undang-Undang Perpajakan yang berlaku di Indonesia maka Pemasok akan bertanggungjawab dengan menanggung semua tagihan atau ketetapan Pajak (Pokok dan Sanksi administrasi / bunga) yang dapat dikenakan oleh KPP kepada Perusahaan karena telah mengkreditkan Faktur Pajak yang diterbitkan oleh Pemasok. </li>
                        </ol>
                        <br>
                        <h4>Produk dan Penyerahan Produk dan/atau Jasa dan Pelaksanaan Jasa</h4>
                        <ol start="10">
                            <li>Produk dan/atau Jasa harus diserahkan kepada Perusahaan pada alamat pengiriman sebagaimana tercantum dalam halaman muka Order Pembelian. Produk dan/atau Jasa tersebut harus diserahkan dalam kondisi baik dan/atau dilaksanakan dengan baik serta tepat waktu sesuai ketentuan Order Pembelian ini dan/atau Data dan Desain Produk. Nota pengiriman yang menyertai Produk harus menyebutkan nomor Order Pembelian, jumlah, uraian Produk dan nomor kode Perusahan apabila perlu.</li>
                            <br>
                            <li>Dalam keadaan apapun termasuk tetapi tidak terbatas kepada Keadaan Kahar, Pemasok tidak dapat mengubah spesifikasi Produk dan/atau Jasa, tanggal penyerahan Produk kepada Perusahaan dan/atau tanggal pelaksanaan Jasa, harga Produk dan/atau Jasa, tempat penyerahan Produk dan/atau Jasa, tanggal maupun tata cara pembayaran tanpa persetujuan tertulis dari Perusahaan.</li>
                            <br>
                            <li>Dalam hal kegiatan usaha Perusahaan terganggu, terhambat atau terlambat dalam produksi yang disebabkan oleh keadaan diluar kekuasaan Perusahaan maka Perusahaan dapat memutuskan untuk mengundurkan tanggal atau tanggal-tanggal penyerahan Produk dan/atau penyelesaian Jasa. Apabila terdapat Produk yang belum diserahkan dan/atau Jasa yang belum dilaksanakan oleh Pemasok kepada Perusahaan pada waktu yang telah disepakati karena sebab apapun maka Perusahaan dapat memutuskan untuk membatalkan sebagian atau seluruh pesanan tersebut tanpa ada kewajiban untuk memberikan ganti rugi apapun juga.</li>
                            <br>
                            <li>Perusahan tidak akan bertanggung jawab atas pesanan-pesanan apapun, kecuali yang telah dikonfirmasi dan ditegaskan pada Order Pembelian ini atau dokumen lain (jika ada) yang resmi dan tercetak serta telah ditandatangani oleh Pejabat Berwenang Perusahaan. </li>
                        </ol>
                        <br>
                        <h4>Pemeriksaan dan Penolakan</h4>
                        <ol start="14">
                            <li>Dalam hal Order Pembelian Produk, Perusahaan berhak melakukan pemeriksaan atas Produk selama dalam pembuatan dan pada saat diterima oleh Perusahaan. Dalam hal Produk tidak diserahkan kepada Perusahaan dalam keadaan dan kondisi baik serta tepat waktu dan sesuai dengan ketentuan Order Pembelian ini dan/atau Data dan Desain Produk, atau Produk diserahkan dalam keadaan rusak atau cacat maka Perusahaan dapat mengembalikan Produk tersebut dan/atau meminta Pemasok untuk melakukan perbaikan, penggantian Produk oleh Pemasok, keseluruhannya atas biaya Pemasok, atau membatalkan pesanan Produk tersebut tanpa kewajiban pembayaran harga Produk yang dibatalkan pesanannya atau ganti rugi apapun.</li>
                            <br>
                            <li>Dalam hal Order Pembelian Jasa, apabila Jasa tidak dilaksanakan dengan baik serta tepat waktu sesuai dengan ketentuan Order Pembelian ini dan/atau Data dan Desain Jasa, maka atas biaya Pemasok, Perusahaan dapat meminta Pemasok untuk segera memperbaiki atau memenuhi pelaksanaan Jasa atau membatalkan pesanan Jasa tersebut tanpa menimbulkan kewajiban bagi Perusahaan untuk membayar harga Jasa yang dibatalkan pesanannya atau ganti rugi apapun.</li>
                        </ol>
                        <br>
                        <h4>Jaminan Pemasok</h4>
                        <ol start="16">
                            <li>Pemasok menjamin bahwa: <br><br>
                                (a) Produk dan/atau Jasa akan sesuai dengan spesifikasi, uraian, atau Sample Proof (khusus untuk Produk), sebagaimana disyaratkan dalam Order Pembelian ini dan/atau Data dan Desain Produk dan/atau Jasa, dan tepat waktu sesuai jadwal yang telah ditentukan oleh Perusahaan;
                                <br>
                                (b) Produk dan/atau Jasa akan cocok untuk setiap tujuan yang secara tegas/eksplisit maupun secara tersirat/implisit diberitahukan kepada Pemasok oleh Perusahaan;
                                <br>
                                (c) Produk dan/atau Jasa merupakan kualitas terbaik dan/atau standard yang terbaik untuk penyerahan Produk dan/atau penyelesaian Jasa sejenis;
                                <br>
                                (d) Produk dan/atau Jasa telah memenuhi peraturan yang berlaku; (e) Produk tidak dijaminkan atau dibebankan;
                                <br>
                                (e) Produk dalam keadaan tidak rusak atau cacat.
                                <br>
                                Jaminan-jaminan tersebut merupakan tambahan dan jaminan-jaminan yang diberikan Pemasok sehubungan dengan Produk dan/atau Jasa dan jaminan-jaminan lain yang diatur dibawah hukum yang berlaku.</li>
                        </ol>
                        <br>
                        <h4>Peralihan Hak Milik dan Risiko Produk</h4>
                        <ol start="17">
                            <li>Hak milik dan risiko atas Produk belum beralih kepada Perusahaan sampai Produk diserahkan kepada Perusahaan, dengan tunduk kepada pemeriksaan dan penolakan yang diatur dalam butir. </li>
                        </ol>
                        <br>
                        <h4>Informasi Rahasia </h4>
                        <ol start="18">
                            <li>Pemasok wajib menjaga kerahasiaan setiap dan seluruh informasi Rahasia yang diberikan Perusahaan sehubungan dengan pemesanan dan pembuatan Produk dan/atau pemesanan Jasa dan segera setelah Produk diserahkan kepada Perusahaan dan/atau Jasa dilaksanakan, Pemasok wajib mengembalikan Informasi Rahasia kepada Perusahaan dalam waktu secepatnya. Ketentuan ini juga berlaku bagi para direktur, komisaris, pegawai, agen, perwakilan dan sub kontraktor dari Pemasok.
                                Kegagalan Penyerahan Produk dan/atau Pelaksanaan Jasa pada Waktunya</li>
                            <br>
                        </ol>
                        <h4>Kegagalan Penyerahan Produk dan/atau Pelaksanaan Jasa pada Waktunya </h4>
                        <ol start="19">
                            <li>Waktu merupakan hal yang amat penting dalam pemesanan ini. Kegagalan Pemasok untuk menyerahkan Produk dan/atau menyelesaikan Jasa dalam waktu yang telah ditentukan dalam Order Pembelian baik sebagian atau seluruhnya, kecuali jika Pemasok dapat membuktikan bahwa kegagalan itu disebabkan oleh Keadaan Kahar, akan dikenakan sanksi-sanksi sebagai berikut: <br><br>
                                (a) Untuk setiap hari keterlambatan dikenakan denda sebesar 2‰ (dua per mil) dari harga Produk yang terlambat penyerahannya dan/atau dari harga Jasa yang terlambat pelaksanaannya, dengan denda maksimal 5% (lima persen) dari harga Produk dan/atau Jasa.
                                <br>
                                (b) Tanpa mengurangi ketentuan dari butir 22 di bawah, apabila keterlambatan tersebut lebih dari 30 (tiga puluh) hari kalender berturu-turut maka Order Pembelian ini dapat dibatalkan secara sepihak oleh Perusahaan dengan pemberitahuan tertulis kepada Pemasok tanpa ada kewajiban untuk memberikan ganti rugi apapun juga dan dengan tidak mengurangi kewajiban Pemasok untuk membayar denda sebagaimana dimaksud dalam butir 19 (a) di atas.
                                <br><br>
                                Dalam hal telah dilakukan seluruh dan/atau sebagian pembayaran kepada Pemasok maka Pemasok wajib mengembalikan seluruh pembayaran yang telah diterimanya tersebut tanpa adanya potongan dalam waktu 3 (tiga) hari kerja sejak tanggal dikirimkannya pemberitahuan oleh Perusahaan kepada Pemasok mengenai terjadinya pembatalan tersebut. </li>
                        </ol>
                        <br>
                        <h4>Ganti Rugi </h4>
                        <ol start="20">
                            <li>Tanpa mengurangi ketentuan hukum yang berlaku, bilamana pada saat penerimaan produk atau pemakaian Produk oleh Perusahaan dan/atau dalam hal Jasa tidak dilaksanakan dengan sebagaimana mestinya sesuai serta tepat waktu sesuai dengan ketentuan Order Pembelian ini dan/atau Data dan Desain Jasa dan/atau spesifikasi apapun yang disebutkan dalam Order Pembelian ini, atau timbul kerugian, kerusakan, kehilangan atau cacat, atas barang-barang dan/atau cidera, atau kematian pegawai Perusahaan dan/atau pihak ketiga manapun akibat pemakaian Produk dan/atau Jasa yang tidak sesuai dengan Order Pembelian atau yang mengandung kelalaian dari Pemasok dan/atau pegawai, agen dan sub kontraktor dari Pemasok maka Perusahaan dapat menuntut kepada Pemasok untuk memperbaiki dan atau mengganti kerugian atas kerugian, kerusakan, kehilangan, cacat, cidera atau kematian tersebut. </li>
                            <br>
                            <li>Perusahaan tidak bertanggung jawab atas kerugian, kerusakan, kehilangan, cacat. cidera, atau kematian apapun yang diderita oleh Pemasok, pegawai, agen maupun sub kontraktor dari Pemasok, baik secara langsung maupun tidak langsung, sehubungan dengan pelaksanaan Order Pembelian ini oleh Pemasok.</li>
                            <br>
                            <li>Pemasok bersedia mengganti kerugian Perusahaan setiap saat dari dan terhadap setiap dan segala ongkos-ongkos, kerugian atau kewajiban (liability), yang diderita, termasuk biaya-biaya bantuan hukum dan biaya-biaya lain yang dikeluarkan Perusahaan sebagai akibat dilanggarnya ketentuan dan syarat-syarat Order Pembelian oleh Pemasok atau akibat kelalaian atau wanprestasi Pemasok atau kuasanya atau pegawainya atau akibat sesuatu tindakan atau hal yang dilakukan atau lalai untuk dilakukan oleh Pemasok atau kuasanya atau pegawainya sehubungan dengan penyediaan Produk dan/atau Jasa.</li>
                        </ol>
                        <br>
                        <h4>Pengalihan dan Sub-Kontrak</h4>
                        <ol start="23">
                            <li>Pemesanan Produk dan/atau Jasa tidak dapat dialihkan oleh Pemasok kepada pihak ketiga. Pemasok tidak diperkenankan melakukan sub-kontrak atas produksi atau penyediaan sebagian atau seluruh Produk dan/atau Jasa tanpa persetujuan tertulis Perusahaan. Dalam hal disetujuinya penunjukan sub-kontraktor oleh Perusahaan maka kecuali diperjanjikan lain secara tertulis oleh Perusahaan dan Pemasok, Pemasok tetap bertanggung jawab atas pemenuhan secara tepat waktu seluruh (tanpa kecuali) kewajiban-kewajiban dan tanggung jawab Pemasok dan atau sub-kontraktor berdasarkan Order Pembelian ini.</li>
                        </ol>
                        <br>
                        <h4>Hak Kekayaan Intelektual </h4>
                        <ol start="24">
                            <li>
                                <ol type="a">
                                    <li>Pemasok menjamin bahwa penyediaan Produk dan/atau Jasa berdasarkan Order Pembelian ini tidak merupakan suatu pelanggaran atas hak kekayaan intelektual pihak ketiga dan/atau Perusahaan.</li>
                                    <br>
                                    <li>Semua informasi (termasuk ilustrasi, gambar, alat-alat, sampel dan spesifikasi) yang disampaikan Perusahaan kepada Pemasok tetap merupakan hak milik Perusahaan dan harus diperlakukan sebagai informasi Rahasia oleh Pemasok. Informasi tersebut semata-mata hanya dapat digunakan oleh Pemasok untuk mempersiapkan harga penawaran atau melengkapi Order Pembelian kepada Perusahaan. Pemasok tidak diperkenankan memperbanyak atau menyalin, menjual, meminjamkan atau dengan cara lain mengalihkan atau menggunakan informasi tersebut tanpa persetujuan tertulis terlebih dahulu dari Perusahaan. Semua ilustrasi, gambar, alatalat, sampel dan spesifikasi yang diberikan kepada Pemasok wajib dikembalikan kepada Perusahaan ketika diminta oleh Perusahaan.</li>
                                    <br>
                                    <li>Pemasok tidak diperkenankan menggunakan merk dagang, nama dagang, slogan atau logo Perusahaan atau pihak terafiliasi dari Perusahaan tanpa persetujuan tertulis secara khusus dari Perusahaan.</li>
                                </ol>
                            </li>
                        </ol>
                        <br>
                        <h4>Code of Conduct Perusahaan</h4>
                        <ol start="25">
                            <br>
                            <li>Pemasok menyatakan bahwa ia tidak memiliki dan tidak akan, selama berlangsungnya penyediaan Produk dan/atau Jasa ini, memiliki benturan kepentingan yang akan mengurangi kemampuan Pemasok untuk menyediakan Produk dan/atau Jasa atau akan menimbulkan kesan ketidaklayakan sehubungan dengan penyediaan Produk dan/atau Jasa oleh Pemasok. Pemasok selanjutnya menyatakan dan menjamin bahwa dirinya ataupun pemilik, rekan atau petugas, direktur atau pegawai manapun yang terkait dengan Pemasok bukan atau tidak akan menjadi petugas atau pegawai suatu badan pemerintah atau badan yang dikendalikan oleh pemerintah atau dari suatu organisasi publik internasional atau seseorang yang bertindak dalam kapasitas resmi untuk atas nama pihak-pihak yang disebut sebelumnya atau pejabat dari suatu partai politik atau calon untuk suatu posisi politik, selama masa penyediaan Produk dan/atau Jasa ini tanpa adanya persetujuan tertulis terlebih dahulu dari Perusahaan.</li>
                            <br>
                            <li>Pemasok menjamin bahwa dirinya tidak akan mengijinkan, menawarkan, menjanjikan, atau melakukan pembayaran atau suatu hal lainnya yang bernilai, dengan cara apapun, baik secara langsung ataupun tidak langsung, kepada (i) setiap staf atau pegawai pemerintah atau pihak manapun yang memiliki pengetahuan bahwa semua atau sebagian dari hal yang bernilai tersebut akan ditawarkan, dijanjikan atau diberikan kepada pegawai pemerintah untuk memengaruhi tindakan resmi untuk mendapatkan atau mempertahankan bisnis atau menjamin keuntungan yang tidak semestinya, atau untuk memberi imbalan atas tindakan tersebut, atau (ii) pihak manapun (baik pegawai pemerintah atau bukan) untuk memengaruhi orang tersebut untuk bertindak melanggar kewajiban itikad baik, ketidakberpihakan atau kepercayaan, atau untuk menghargai tindakan semacam itu. Hal ini termasuk larangan menawarkan atau melakukan pembayaran ”fasilitasi”. Pembayaran fasilitasi adalah pembayaran kecil kepada pegawai pemerintah untuk memperlancar atau mengamankan kinerja tindakan rutin pemerintah (tindakan yang biasanya dan biasa dilakukan). Contohnya termasuk pembayaran untuk mempercepat penerbitan visa yang sah; lisensi atau ijin; dan untuk menghubungkan telepon atau layanan utilitas lainnya. Untuk menghindari keraguan dan tanpa membatasi pengertian umum dari jaminan tersebut dalam ayat ini, Pemasok selanjutnya menjamin bahwa ia tidak akan memberikan suatu hadiah atau sumbangan politik dalam bentuk uang atau sejenisnya kepada, maupun melayani atau memenuhi permintaan, pegawai pemerintah atau pihak manapun untuk dan atas nama Perusahan atau pihak afiliasinya tanpa persetujuan tertulis dari Perusahaan terlebih dahulu, dan semua pemberian, hiburan dan kontribusi yang disetujui tersebut dicatat secara akurat dalam buku dan catatannya dan tidak akan diganti oleh Perusahaan atau pihak afiliasinya tanpa mendapat persetujuan terlebih dahulu dari Perusahaan. Untuk tujuan ayat 2 ini, seseorang dianggap memiliki ”pengetahuan” berkenaan dengan perilaku, keadaan atau hasil jika orang tersebut mengetahui (1) adanya atau (2) probabilitas yang tinggi terhadap keberadaan perilaku, keadan atau hasil tersebut.</li>
                            <br>
                            <li>Tidak ada bagian dari pembayaran oleh Perusahaan kepada Pemasok yang dapat dipergunakan, baik secara langsung maupun tidak langsung, atau dengan suatu cara apapun, (i) untuk tujuan apapun yang merupakan pelanggaran atas undang-undang dari negara di mana penyediaan Produk dan/atau Jasa akan dilakukan, negara-negara dimana Perusahaan dan Pemasok didirikan, atau setiap negara lain yang hukumnya dapat berlaku bagi salah satu pihak atau afiliasinya masing-masing, (ii) untuk mendapatkan keuntungan apapun dari pegawai pemerintah manapun atau (iii) untuk tujuan tidak sah, tidak etis atau tidak layak baik yang berhubungan maupun tidak berhubungan dengan Order pembelian ini dan Pemasok menjamin bahwa ia tidak akan mempergunakan dana yang dimaksud dengan cara yang melanggar ketentuan-ketentuan ini.</li>
                            <br>
                            <li>
                                Pemasok wajib memastikan bahwa setiap pegawainya, agennya, perwakilannya dan subkontraktor yang memiliki kewenangan secara sah dari atau yang terkait dengan Pemasok sehubungan dengan Order Pembelian ini, membaca dan mematuhi standar perilaku yang ditentukan didalam butir-butir 25 sampai 31 ini. Pemasok juga setuju untuk mematuhi dan wajib menyebabkan setiap pegawai, agen, perwakilan dan subkontraktor untuk mematuhi kebijakan-kebijakan dan aturan-aturan perilaku usaha yang telah diberitahukan atau yang akan diberitahukan di masa yang akan datang secara tegas oleh Perusahaan kepada Pemasok.
                            </li>
                            <br>
                            <li>
                                Pemasok tidak dan tidak akan menggunakan pihak ketiga untuk: <br>
                                (a) Memberikan manfaat yang dapat mempengaruhi loyalitas dan objektifitas setiap pegawai Perusahaan dalam membuat keputusan untuk dan atas nama Perusahaan.
                                <br>
                                (b) Pemberian hadiah berupa fasilitas perjalanan dan penginapan Pribadi tidak diperbolehkan.
                                <br>
                                (c) Melakukan hubungan bisnis dengan Perusahaan jika pegawai Perusahaan memiliki Kepentingan Yang Substansial dalam organisasi Pemasok dan memiliki kemampuan untuk menentukan dipilihnya atau diberhentikannya Pemasok atau syarat-syarat Order Pembelian ini antara Pemasok dan Perusahaan.
                                <br>
                                (d) Menyogok, memberikan komisi rahasia atau pemberian kompensasi lain yang tidak sah atau tidak layak kepada pihak manapun. <br>
                                (e) Melakukan duplikasi yang tidak sah atas perangkat lunak (software) yang dimiliki oleh atau dilisensikan kepada Perusahaan.
                                Pemasok diwajibkan untuk segera memberitahukan Perusahaan jika Pemasok mengetahui adanya pelanggaran atas butir ini.
                                <br>
                                Dalam butir ini, istilah Manfaat memiliki pengertian setiap pembayaran, pinjaman, pemberian jasa, gratifikasi, uang, hadiah, jamuan (entertainment) di luar kebiasaan dan kewajaran bersosial atau bantuan lainnya. Kepentingan Substansial memiliki pengertian kepentingan ekonomi, Pribadi atau keluarga, yang dapat mempengaruhi atau secara wajar dapat dianggap mempengaruhi keputusan atau tindakan.
                            </li>
                            <br>
                            <li>Seluruh laporan keuangan, catatan-catatatan dan tagihan-tagihan yang disampaikan oleh Pemasok kepada Perusahaan harus, dalam rincian yang wajar, secara tepat dan jujur menunjukan kegiatan-kegiatan dan transaksitransaksi yang berhubungan dengan rekening Perusahaan. Pemasok wajib menyimpan dan memelihara secara lengkap dan tepat, pembukuan-pembukuan, catatan-catatan atas rekening, laporan-laporan dan data-data lain yang diperlukan bagi administrasi yang layak atas Order Pembelian ini untuk jangka waktu 5 (lima) tahun setelah berakhirnya atau lampaunya waktu Order Pembelian ini.</li>
                            <br>
                            <li>Perusahaan berhak menunjuk auditor internal dan/atau independennya untuk melakukan pemeriksaan selama jam kerja normal terhadap catatan keuangan dan pembukuan Pemasok berkaitan dengan pelakasanaan tugas Pemasok dengan melakukan pemberitahuan sebelumnya secara wajar. Perusahaan dapat melaksanakan haknya untuk melakukan pemeriksaan sebanyak dua kali dalam setahun selama masa berlangsungnya penyediaan Produk dan/atau Jasa dan satu kali selama 12 (dua belas) bulan segera setelah berakhirnya atau lampaunya waktu penyediaan Produk dan/atau Jasa ini. </li>
                            <br>
                            <li>Pemasok menyatakan bahwa baik dirinya maupun afiliasinya (atau setiap direktur, petugas atau berdasarkan sepengetahuannya, karyawannya atau afiliasinya) maupun agennya, bukanlah seseorang atau entitas yang dimiliki atau dikontrol oleh seseorang atau entitas yang merupakan (i) subjek dari setiap Sanksi; (ii) terlibat dalam kegiatan apapun yang dapat memicu penunjukan sebagaimana diatur dalam Sanksi yang dikelola oleh otoritas yang relevan; atau (iii) mempekerjakan, menggunakan, mengadakan atau mensubkontrakkan pekerja atau buruh apa pun yang berasal dari atau dapat distribusikan ke Negara Perusahaan.</li>
                            <br>
                            <li><b>Sanksi</b> berarti segala sanksi ekonomi atau keuangan atau embargo perdagangan yang dilaksanakan, dikelola atau ditegakkan oleh Kantor Departemen Keuangan yang berlaku internasional untuk Pengawasan Aset Asing atau otoritas lainnya yang dapat memberikan Sanksi dalam yurisdiksi yang relevan dengan Order Pembelian ini. </li>
                        </ol>
                        <br>

                        <h4>Ketentuan Lain-Lain </h4>
                        <ol start="33">
                            <li>Ketentuan dan syarat-syarat Order Pembelian ini dibuat dalam Bahasa Indonesia. Terjemahan atas Order Pembelian ini ke dalam Bahasa asing tidaklah mengikat Perusahaan dan Pemasok. </li>
                            <br>
                            <li>Hal-hal lain yang belum diatur dalam Order Pembelian ini dapat diatur lebih lanjut secara terpisah berdasarkan kesepakatan yang dibuat secara tertulis Perusahaan dan Pemasok. </li>
                            <br>
                            <li>
                                Perusahaan atas pilihannya dapat mengubah atau membatalkan pesanannya atas Produk dan/atau Jasa sebelum Produk dan/atau Jasa diserahkan kepada Perusahaan dan Pemasok wajib segera melakukan segala hal yang memungkinkan untuk mengurangi biaya yang timbul akibat perubahan atau pembatalan tersebut. Tanggung jawab Perusahaan terbatas pada biaya yang langsung ditanggung oleh Pemasok sampai pada tanggal pembatalan dan Perusahaan tidak bertanggung jawab dalam hal apapun atas kerugian tidak langsung (indirect loss), akibat turutan dari kerugian yang langsung diderita (consequential loss) atau kerugian kehilangan keuntungan (loss of profit) akibat perubahan atau pembatalan tersebut.
                            </li>
                            <br>
                            <li>
                                Perusahaan dan Pemasok sepakat untuk mengesampingkan berlakunya ketentuan Pasal 1266 Kitab Undang-Undang Hukum Perdata yang mensyaratkan adanya keputusan pengadilan untuk pengakhiran suatu perjanjian.
                            </li>
                            <br>
                            <li>
                                Order Pembelian ini diatur berdasarkan hukum Negara Republik Indonesia.
                            </li>
                            <br>
                            <li>
                                Ketentuan dan syarat-syarat Order Pembelian Produk dan/atau Jasa ini merupakan bagian yang tidak terpisahkan dan saling melengkapi dengan ketentuan dan syarat-syarat kontrak yang dibuat secara tertulis oleh dan antara Perusahaan dan Pemasok (jika ada). Apabila terjadi ketidaksesuaian antara Order Pembelian dan/atau Jasa dengan kontrak yang dibuat secara tertulis maka yang berlaku adalah kontrak yang dibuat secara tertulis.
                            </li>
                        </ol>
                    </div>
                    <hr style="border: 1px solid #e7e7e7;">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="news-list-box">
                    <div class="news-list-box-pager text-right">
                        <a class="btn btn-info" href="<?= base_url('Syarat_Ketentuan/english'); ?>">English</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>