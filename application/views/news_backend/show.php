<section id="news-list" class="section-margine blog-list">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <div class="news-list-box">
          <?php if ($news->news_category_id == 5) { ?>
            <?php echo html_entity_decode(htmlspecialchars_decode($news->video_embed)); ?>
          <?php } else { ?>
            <img src="<?php echo base_url() . $news->img; ?>" class="img-responsive" width="100%" alt="News">
          <?php } ?>
          <h1 class="text-left"><?php echo $news->title; ?></h1>
          <div style="text-align: justify;">
          <?php echo html_entity_decode(htmlspecialchars_decode($news->description)); ?>
          </div>
          <hr style="border: 1px solid #e7e7e7;">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8">
        <div class="news-list-box">
          <div class="news-list-box-pager text-right">
            <button class="btn btn-primary" onclick="window.history.go(-1); return false;">Back</button>
            <!-- <a href="#"><img src="<?php echo base_url(); ?>assets/images/news/arrow-left.png" width="30"></a>
            <a href="#"><img src="<?php echo base_url(); ?>assets/images/news/arrow-right.png" width="30"></a> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</section>