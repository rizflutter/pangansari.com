<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><?= $pg_title; ?></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><?= $pg_menu; ?></li>
                        <li class="breadcrumb-item"><?= $pg_title; ?></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- alert success -->
                    <?php if ($this->session->flashdata('flash')) { ?>
                        <?php if ($this->session->flashdata('flash') == 'edit_success') { ?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <p class="text-center">Data berhasil diubah!</p>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <!-- alert error upload -->
                    <?php if ($this->session->flashdata('img')) { ?>
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <p class="text-center"><?= "Gagal upload image" . $this->session->flashdata('img'); ?></p>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <?php } ?>
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Edit <?= $pg_title; ?></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <?php echo form_open("News_Backend/edit_act", array('enctype' => 'multipart/form-data', 'id' => 'news-backend-form')); ?>
                        <div class="card-body">
                        <div class="form-group">
                                <label for="">News Category</label>
                                <select name="news_category_id" class="form-control">
                                    <option value="" id="">Pilih Category</option>
                                    <?php foreach ($news_category as $nc) : ?>
                                        <option value="<?= $nc->id; ?>"<?php if ($nc->id == $news_row->news_category_id) { echo 'selected'; } ?>><?= $nc->description; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <?php echo form_error('news_category_id', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="id" class="form-control" value="<?= $news_row->id; ?>">
                                <label for="">Judul</label>
                                <input type="text" name="title" class="form-control" placeholder="Judul lama : <?= $news_row->title; ?>, Birkan kosong jika tidak ingin mengubah judul !" value="<?= set_value('title') ?>">
                                <?php echo form_error('title', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="">Description</label>
                                <textarea name="description" id="news-ckeditor"><?= html_entity_decode(htmlspecialchars_decode($news_row->description)); ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="">Video</label>
                                <textarea name="video_embed" class="form-control" placeholder="Masukkan url video, contoh : <iframe width='' height='' src='' frameborder='' allow='' allowfullscreen></iframe"><?= $news_row->video_embed; ?></textarea>
                            </div>
                            <!-- /.form group -->
                            <img src="<?= base_url($news_row->img); ?>" alt="" height="100">
                            <div class="form-group">
                                <label for="exampleInputFile">Img</label>
                                <div class="input-group">
                                    <input type="file" name="img" class="form-control" id="">
                                </div>
                                <p>*Max Size 2MB - Max Height 768px & Max Width 1366px</p>
                            </div>
                            <div class="form-group">
                                <label for="">Start Date</label>
                                <input type="date" name="start_date" class="form-control" value="<?= date_format(date_create($news_row->start_date), 'Y-m-d'); ?>">
                                <?php echo form_error('start_date', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="">Finish Date</label>
                                <input type="date" name="finish_date" class="form-control" value="<?= date_format(date_create($news_row->finish_date), 'Y-m-d'); ?>">
                                <?php echo form_error('finish_date', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="">Is Active ?</label>
                                <select name="is_active" id="" class="form-control">
                                    <option value="0" <?php if ($news_row->is_active == 1) {
                                                            echo 'selected';
                                                        }; ?>>Disable</option>
                                    <option value="1" <?php if ($news_row->is_active == 1) {
                                                            echo 'selected';
                                                        }; ?>>Active</option>
                                </select>
                            </div>
                            <!-- <?php
                            if ($this->ion_auth->in_group($groups)) { ?>
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" name="is_admit" type="checkbox" id="customCheckbox1" value="1" <?php if ($news_row->is_admit == 1) {
                                                                                                                                            echo 'checked';
                                                                                                                                        } ?>>
                                    <label for="customCheckbox1" class="custom-control-label">Approved ?</label>
                                </div>
                            </div>
                            <?php } ?> -->
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a href="<?= base_url('News_Backend'); ?>" class="btn btn-danger">Kembali</a>
                        </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
</div>