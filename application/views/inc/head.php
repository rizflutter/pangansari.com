<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="PT Pangansari Utama Food Resources">
<meta name="keywords" content="PT Pangansari Utama Food Resources | <?php echo ucwords($pg_title); ?>">
<meta name="author" content="pangansarigroup">

<meta property="og:title" content="<?php echo ucwords($pg_title); ?>">
<meta property="og:description" content="PT Pangansari Utama Food Resources.">
<?php if(!empty($news_detail['img'])){ ?>
<meta property="og:image" content="<?php echo base_url(); ?><?php echo $news_detail['img']; ?>">
<?php } ?>
<meta property="og:url" content="https://www.pangansari.com">

<!-- Site Title   -->
<title>PT. Pangansari Utama Food Resources | <?php echo ucwords($pg_title); ?></title>
<!-- Fav Icons   -->
<link rel="icon" href="<?php echo base_url(); ?>assets/images/favicon.ico" type="image/x-icon">
<!-- Bootstrap -->
<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
<!-- <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet"> -->
<link href="<?php echo base_url(); ?>assets/css/bootstrap-dropdownhover.min.css" rel="stylesheet">
<!-- Hover CSS -->
<link href="<?php echo base_url(); ?>assets/css/hover-min.css" rel="stylesheet">
<!-- Fonts Awesome -->
<link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
<!-- Google Fonts -->
<!-- <link href='<?php echo base_url(); ?>assets/fonts/fonts.css' rel='stylesheet' type='text/css'> -->
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">
<!-- animate Effect -->
<link href="<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet">
<!-- Main CSS -->
<link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
<!-- Responsive CSS -->
<link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet">
<!-- TOASTR -->
<link href="<?php echo base_url(); ?>assets/vendor/toastr/toastr.css" rel="stylesheet"/>
<!-- JQuery -->
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<!-- lightbox -->
<link href="<?= base_url(); ?>assets/lightbox/dist/css/lightbox.css" rel="stylesheet" />
<!-- jssocials -->
<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/jssocials/dist/jssocials.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/jssocials/dist/jssocials-theme-flat.css" />
<script src="<?php echo base_url(); ?>assets/jssocials/dist/jssocials.min.js"></script>
<!-- OwlCarousel -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/owlcarousel/dist/assets/owl.carousel.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/owlcarousel/dist/assets/owl.theme.default.min.css">
<script src="<?php echo base_url(); ?>assets/owlcarousel/dist/owl.carousel.min.js"></script>

<style type="text/css">

	.parallax1 {
	  background-image: url("<?php echo base_url().$parallax1['img']; ?>");
	  /*background-image: url('<?php echo base_url(); ?>assets/images/parallax/parallax-5.jpg');*/
	  min-height: 400px; 
	  /* background-attachment: fixed; */
	  /* background-position: -70% center; */
	  background-repeat: no-repeat;
	  background-size: cover;
	}

	.parallax2 {
	  background-image: url("<?php echo base_url().$parallax2['img']; ?>");
	  /*background-image: url('<?php echo base_url(); ?>assets/images/parallax/parallax-5.jpg');*/
	  min-height: 200px; 
	  background-attachment: fixed;
	  background-position: -70% center;
	  background-repeat: no-repeat;
	  background-size: contain;
	}

	.parallax3 {
	  background-image: url("<?php echo base_url().$parallax3['img']; ?>");
	  /*background-image: url('<?php echo base_url(); ?>assets/images/parallax/parallax-5.jpg');*/
	  min-height: 200px; 
	  background-attachment: fixed;
	  background-position: -70% center;
	  background-repeat: no-repeat;
	  background-size: contain;
	}

	.parallax4 {
	  background-image: url("<?php echo base_url().$parallax4['img']; ?>");
	  /*background-image: url('<?php echo base_url(); ?>assets/images/parallax/parallax-5.jpg');*/
	  min-height: 200px; 
	  background-attachment: fixed;
	  background-position: -70% center;
	  background-repeat: no-repeat;
	  background-size: contain;
	}

	@media (max-width: 575.98px) {  
		.parallax1 {
			background-image: url("<?php echo base_url().$parallax2['img']; ?>");
			width: 100%;
		}
	}

	@media only screen and (min-width : 1024px) {  
		
	}

	@media only screen and (min-width : 1366px) {  
	    /*---- START PARALLAX ----*/
	    .parallax1 {
	      background-position: -100% center;
	    }
	    /*---- END PARALLAX ----*/

		/*---- START PARALLAX ----*/
	    .parallax2 {
	      background-position: -100% center;
	    }
	    /*---- END PARALLAX ----*/

		/*---- START PARALLAX ----*/
	    .parallax3 {
	      background-position: -100% center;
	    }
	    /*---- END PARALLAX ----*/

		/*---- START PARALLAX ----*/
	    .parallax4 {
	      background-position: -100% center;
	    }
	    /*---- END PARALLAX ----*/
	}

	@media only screen and (min-width : 1440px) {  
	    /*---- START PARALLAX ----*/
	    .parallax1 {
	      	background-position: 20% center;
			background-repeat: no-repeat;
			background-size: 60% auto;
	    }
	    /*---- END PARALLAX ----*/

		/*---- START PARALLAX ----*/
	    .parallax2 {
	      	background-position: 20% center;
			background-repeat: no-repeat;
			background-size: 60% auto;
	    }
	    /*---- END PARALLAX ----*/

		/*---- START PARALLAX ----*/
	    .parallax3 {
	      	background-position: 20% center;
			background-repeat: no-repeat;
			background-size: 60% auto;
	    }
	    /*---- END PARALLAX ----*/

		/*---- START PARALLAX ----*/
	    .parallax4 {
	      	background-position: 20% center;
			background-repeat: no-repeat;
			background-size: 60% auto;
	    }
	    /*---- END PARALLAX ----*/
	}

	@media only screen and (min-width : 1600px) {  
	    /*---- START PARALLAX ----*/
	    .parallax1 {
	      	background-position: 20% center;
			background-repeat: no-repeat;
			background-size: 60% auto;
	    }
	    /*---- END PARALLAX ----*/

		/*---- START PARALLAX ----*/
	    .parallax2 {
	      	background-position: 20% center;
			background-repeat: no-repeat;
			background-size: 60% auto;
	    }
	    /*---- END PARALLAX ----*/

		/*---- START PARALLAX ----*/
	    .parallax3 {
	      	background-position: 20% center;
			background-repeat: no-repeat;
			background-size: 60% auto;
	    }
	    /*---- END PARALLAX ----*/

		/*---- START PARALLAX ----*/
	    .parallax4 {
	      	background-position: 20% center;
			background-repeat: no-repeat;
			background-size: 60% auto;
	    }
	    /*---- END PARALLAX ----*/
	}

	@media only screen and (min-width : 1920px) {  
	    /*---- START PARALLAX ----*/
	    .parallax1 {
			background-position: 20% center;
			background-repeat: no-repeat;
			background-size: 60% auto;
	    }
		/*---- START PARALLAX ----*/
	    .parallax2 {
			background-position: 20% center;
			background-repeat: no-repeat;
			background-size: 60% auto;
	    }
		/*---- START PARALLAX ----*/
	    .parallax3 {
			background-position: 20% center;
			background-repeat: no-repeat;
			background-size: 60% auto;
	    }
		/*---- START PARALLAX ----*/
	    .parallax4 {
			background-position: 20% center;
			background-repeat: no-repeat;
			background-size: 60% auto;
	    }
	}

	@media only screen and (min-width : 2048px) {  
	}
</style>