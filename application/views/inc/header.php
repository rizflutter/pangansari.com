<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<header id="header" class="head">
  <div class="top-header">
     <div class="container">
       <div class="row ">
       <?php if(isset($company_profile_row)){ ?>
         <div class="social-links col-md-6 pull-right">
           <ul class="social-icons pull-right">
             <li><a href="<?= base_url($company_profile_row->document_file) ?>"><i class="fa fa-file-pdf-o"></i> Company Profile</a></li>
           </ul>
         </div>
        <?php } ?>
       </div>
     </div>
    </div>
    <nav class="navbar navbar-default navbar-menu">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> 
            <span class="sr-only">Toggle navigation</span> 
            <span class="icon-bar"></span> 
            <span class="icon-bar"></span> 
            <span class="icon-bar"></span> 
          </button>
          <a class="navbar-brand" href="<?php echo base_url(); ?>">
            <!-- <div class="logo-text"><span><samp>M</samp>Medi</span>camp</div> -->
            <img src="<?php echo base_url(); ?>assets/images/logo-pangansari.png" alt="pufr" width="200px">
          </a> 
        </div>
        <div class="collapse navbar-collapse" style="padding-left: 15px; padding-right: 15px;" id="bs-example-navbar-collapse-1" data-hover="dropdown" data-animations="fadeIn">
          <ul class="nav navbar-nav navbar-right">
            <li <?php if(empty($this->uri->segment(1)) OR $this->uri->segment(1) == "home"){ ?> class="active" <?php } ?>><a href="<?php echo base_url(); ?>">Home</a></li>
            <li <?php if($this->uri->segment(1) == "pangansari_group"){ ?> class="active" <?php } ?>><a href="<?php echo base_url(); ?>pangansari_group">Pangansari Group</a></li>
            <li <?php if($this->uri->segment(1) == "our_business"){ ?> class="active" <?php } ?>><a href="<?php echo base_url(); ?>our_business">Our Business</a></li>
            <li <?php if($this->uri->segment(1) == "news"){ ?> class="active" <?php } ?>><a href="<?php echo base_url(); ?>news#news-list">News</a></li>
            <li <?php if($this->uri->segment(1) == "http://glomart.id"){ ?> class="active" <?php } ?>><a href="http://glomart.id" target="_blank">E Commerce</a></li>
            <li <?php if($this->uri->segment(1) == "career"){ ?> class="active" <?php } ?>><a href="<?php echo base_url(); ?>career">Career</a></li>
            <li <?php if($this->uri->segment(1) == "contact"){ ?> class="active" <?php } ?>><a href="<?php echo base_url(); ?>contact">Contact</a></li>
          </ul>
        </div>
        <!--/.nav-collapse --> 
      </div>
    </nav>
</header>