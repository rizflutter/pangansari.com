<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.plugin.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.isotope.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/jquery.magnific-popup.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/bootstrap-dropdownhover.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/waypoints.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/jquery.counterup.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/toastr/toastr.js"></script>
<script src="<?php echo base_url(); ?>assets/js/main.js"></script>
<script src="<?= base_url(); ?>assets/lightbox/dist/js/lightbox.min.js"></script>