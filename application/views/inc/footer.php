<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section id="footer-top" class="footer-top">
  <div class="container">
    <div class="row">
      <div class="col-md-5 col-lg-5">
        <div class="footer-top-box footer-align-1">
          <h5>Head Office</h5>
          <h5>Jl. Raya Poncol No. 24 Ciracas<br>Jakarta 13740 - Indonesia</h5><br>
          <h5><img src="<?php echo base_url(); ?>assets/images/Home-36.png" width="20px"> +62 21 871 7870</h5>
          <h5><img src="<?php echo base_url(); ?>assets/images/Home-37.png" width="20px"> +62 21 871 7869</h5>
          <h5><a href="<?= base_url('Syarat_Ketentuan'); ?>" style="color: white">Syarat & Ketentuan</a></h5>
        </div>
      </div>
      <div class="col-md-2 col-lg-2">
        <div class="footer-top-box footer-align-2">
          <h5>Follow Us</h5><br>
          <img src="<?php echo base_url(); ?>assets/images/Home-38.png" width="30px">
          <img src="<?php echo base_url(); ?>assets/images/Home-39.png" width="30px">
          <img src="<?php echo base_url(); ?>assets/images/Home-40.png" width="30px">
          <br>
          <br>
          <img src="<?php echo base_url(); ?>assets/images/Home-41.png" width="30px">
          <img src="<?php echo base_url(); ?>assets/images/Home-42.png" width="30px">
          <img src="<?php echo base_url(); ?>assets/images/Home-43.png" width="30px">
        </div>
      </div>
      <div class="col-md-5 col-lg-5">
        <div class="footer-top-box footer-align-3">
          <a href="<?php echo base_url(); ?>contact" class="btn btn-primary2">Send Email</a>
          <br>
          <br>
          <a href="<?php echo base_url(); ?>e_commmerce" class="btn btn-primary3">&nbsp; Glomart &nbsp;</a>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="footer-bottom" class="footer-bottom">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-lg-12">
        <div class="copyright">Copyright &copy; 2020 Pangansari - All Right Reserved.</div>
      </div>
    </div>
  </div>
</section>