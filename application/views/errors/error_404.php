<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section id="section19" class="section19">
  <div class="container">
   <div class="row">
     <div class="col-md-12 col-lg-12"><h1>404</h1><h3>Oops, the page you are looking for does not exist.</h3></div>
     <div class="col-md-12 col-lg-12">
     <div class=" text-center">
       <a href="<?php echo base_url(); ?>" class="btn btn-primary">Back to Home</a>
     </div>
     </div>
   </div>  
  </div>
</section>