<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<div class="header-bottom">
    <img src="<?php echo base_url(); ?>assets/images/career/Career-29.png" width="100%">
</div>

<section id="career" class="section-margine blog-list">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="career-box">
                    <h3 class="text-left"><?php echo $career->title; ?></h3>
                    <div>
                        <?php echo html_entity_decode(htmlspecialchars_decode($career->description)); ?>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <div class="text-right"><a href="<?php echo $career->link; ?>" class="btn btn-primary">Apply</a></div>
                        </div>
                    </div>
                    <hr style="border: 1px solid #e7e7e7;">
                </div>
            </div>
        </div>
    </div>
</section>