<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><?= $pg_title; ?></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><?= $pg_menu; ?></li>
                        <li class="breadcrumb-item"><?= $pg_title; ?></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- alert success -->
                    <?php if ($this->session->flashdata('flash')) { ?>
                        <?php if ($this->session->flashdata('flash') == 'edit_success') { ?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <p class="text-center">Data berhasil diubah!</p>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <!-- alert error upload -->
                    <?php if ($this->session->flashdata('img')) { ?>
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <p class="text-center"><?= "Gagal upload image" . $this->session->flashdata('img'); ?></p>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <?php } ?>
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Edit <?= $pg_title; ?></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <?php echo form_open("Career_Backend/edit_act", array('enctype' => 'multipart/form-data', 'id' => 'career-backend-form')); ?>
                        <div class="card-body">
                            <div class="form-group">
                                <input type="hidden" name="id" value="<?= $career_row->id; ?>">
                                <label for="">Judul</label>
                                <input type="text" name="title" class="form-control" id="" placeholder="Masukkan judul" value="<?= $career_row->title; ?>">
                                <?php echo form_error('title', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="">Description</label>
                                <textarea name="description" id="news-ckeditor"><?= html_entity_decode(htmlspecialchars_decode($career_row->description)); ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="">Link</label>
                                <input type="text" name="link" class="form-control" id="" placeholder="Masukkan link" value="<?= $career_row->link; ?>">
                                <?php echo form_error('link', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label>Start Date</label>
                                <input type="date" class="form-control" value="<?= date_format(date_create($career_row->start_date), 'Y-m-d'); ?>" name="start_date" />
                                <?php echo form_error('finish_date', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label>Finish Date</label>
                                <div class="input-group">
                                    <input type="date" class="form-control" value="<?= date_format(date_create($career_row->finish_date), 'Y-m-d'); ?>" name="finish_date" />
                                </div>
                                <?php echo form_error('finish_date', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="">Is Active ?</label>
                                <select name="is_active" id="" class="form-control">
                                    <option value="0" <?php if ($career_row->is_active == 1) {
                                                            echo 'selected';
                                                        }; ?>>Disable</option>
                                    <option value="1" <?php if ($career_row->is_active == 1) {
                                                            echo 'selected';
                                                        }; ?>>Active</option>
                                </select>
                            </div>
                            <!-- <?php
                            if ($this->ion_auth->in_group($groups)) { ?>
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" name="is_admit" type="checkbox" id="customCheckbox1" value="1" <?php if ($career_row->is_admit == 1) {
                                                                                                                                            echo 'checked';
                                                                                                                                        } ?>>
                                    <label for="customCheckbox1" class="custom-control-label">Approved ?</label>
                                </div>
                            </div>
                            <?php } ?> -->
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a href="<?= base_url('Career_Backend'); ?>" class="btn btn-danger">Kembali</a>
                        </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
</div>