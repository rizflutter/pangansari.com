<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><?= $pg_title; ?></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><?= $pg_menu; ?></li>
                        <li class="breadcrumb-item"><?= $pg_title; ?></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <?php if ($this->session->flashdata('flash')) { ?>
                <?php if ($this->session->flashdata('flash') == 'edit_success') { ?>
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <p class="text-center">Data berhasil diubah!</p>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>
            <?php } ?>
            <!-- alert error upload -->
            <?php if ($this->session->flashdata('img')) { ?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <p class="text-center"><?= "Gagal upload image" . $this->session->flashdata('img'); ?></p>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php } ?>
            <div class="row">
                <div class="col-lg-5">
                    <!-- Profile Image -->
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                            <div class="text-center">
                                <img class="profile-user-img img-fluid img-circle" src="<?= base_url() . $user->img ?>" alt="User profile picture">
                            </div>

                            <h3 class="profile-username text-center"><?= ucwords($user->company) ?></h3>

                            <p class="text-muted text-center"></p>

                            <ul class="list-group list-group-unbordered mb-3">
                                <li class="list-group-item">
                                    <b>Email</b> <a class="float-right"><?= ucwords($user->email) ?></a>
                                </li>
                                <li class="list-group-item">
                                    <b>First Name</b> <a class="float-right"><?= ucwords($user->first_name) ?></a>
                                </li>
                                <li class="list-group-item">
                                    <b>Last Name</b> <a class="float-right"><?= ucwords($user->last_name) ?></a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>

                <div class="col-lg-7">
                    <!-- About Me Box -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title"></h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form action="<?= base_url('Profile/update_img_profile/');  ?>" .<?= $user->id; ?> method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <strong><i class="far fa-images"></i> IMG  </strong>
                                    <input name="id" type="hidden" value="<?= $user->id ?>" class="form-control" onsubmit="">
                                    <input type="file" name="img" class="form-control" onchange="form.submit()">
                                    <p>Max size 2MB - height 500px - width 500px</p>
                                </div>
                            </form>

                            <hr>

                            <form enctype="multipart/form-data" class="bg-white shadow-sm p-3" action="<?= base_url('Profile/edit_act'); ?>" method="POST">
                                <div class="form-group">
                                    <input name="id" type="hidden" value="<?= $user->id ?>" class="form-control" onsubmit="">
                                    <strong><i class="fas fa-user-alt"></i> First Name</strong>
                                    <input type="text" class="form-control" name="first_name" placeholder="Masukkan nama depan anda ...">
                                    <?php echo form_error('first_name', '<div class="error">', '</div>'); ?>
                                </div>

                                <hr>

                                <div class="form-group">
                                    <strong><i class="fas fa-user-alt"></i> Last Name</strong>
                                    <input type="text" class="form-control" name="last_name" placeholder="Masukkan nama belakang anda ...">
                                    <?php echo form_error('last_name', '<div class="error">', '</div>'); ?>
                                </div>

                                <hr>

                                <div class="form-group">
                                    <strong><i class="fas fa-lock"></i> New Password</strong>
                                    <input type="password" class="form-control" name="password" placeholder="Masukkan password baru anda ...">
                                    <?php echo form_error('password', '<div class="error">', '</div>'); ?>
                                </div>

                                <input type="submit" class="btn btn-primary btn-block" value="Save changes">
                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                </div>
            </div>
        </div>
    </div>
</div>