<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<!-- <style>
.footer1 {
   position: fixed;
   left: 100px;
   bottom: 0;
   width: 60%;
   height: 10%;
   background-color: red;
   color: white;
   text-align: center;
}
</style> -->

<div class="header-bottom">
  <img src="<?php echo base_url(); ?>assets/images/news/News-23.png" width="100%">
</div>

<section id="news" class="news">
  <div class="container">
    <div class="row">
      <?php foreach ($news_category as $key => $news_category) { ?>
        <div class="col-md-2">
          <div class="news-box wow bounceIn">
            <div class="news-box-icon-background"><a href="<?php echo base_url() . $news_category->link; ?>#news-list" class="hvr-shrink">
                <?php if ((isset($_GET['cat']) and $_GET['cat'] == $news_category->name) or (!isset($_GET['cat']) and $this->uri->segment(1) == $news_category->name)) {
                  $img_news   = $news_category->img_active;
                  $txt_news   = '#203873';
                } else {
                  $img_news   = $news_category->img_nonactive;
                  $txt_news   = '#a9a9a9';
                } ?>
                <img src="<?php echo base_url(); ?><?php echo $img_news; ?>"></a>
            </div>
            <a href="<?php echo base_url() . $news_category->link; ?>#news-list" class="hvr-shrink">
              <h4 style="color: <?php echo $txt_news; ?>" class="text-center"><?php echo $news_category->description; ?></h4>
            </a>
          </div>
        </div>
      <?php } ?>
    </div>
  </div>
</section>
<hr>

<!-- carousel-moving-box -->
<?php if (count($latest_news) > 0) { ?>
  <section id="home-news2" class="home-news2">
    <div class="container">
      <div class="owl-carousel owl-theme">
        <?php foreach ($latest_news as $key => $row_latest_news) { ?>
          <ul class="thumbnails">
            <div class="thumbnail">
              <a href="<?php echo base_url() . "news/detail/" . $row_latest_news->link_post . "?cat=" . $row_latest_news->news_category_name; ?>#news-list">
                <img class="owl-lazy" data-src="<?php echo base_url() . $row_latest_news->img; ?>">
              </a>
            </div>
            <div class="caption-box">
              <a href="<?php echo base_url() . "news/detail/" . $row_latest_news->link_post . "?cat=" . $row_latest_news->news_category_name; ?>#news-list">
                <h5 class="text-left"><?php echo substr($row_latest_news->title, 0, 100); ?>...</h5>
              </a>
            </div>
          </ul>
        <?php } ?>
      </div>
    </div>
    </div>
  </section>
  <hr>
  <script>
    $('.owl-carousel').owlCarousel({
      items: 4,
      lazyLoad: true,
      loop: false,
      margin: 10
    });
  </script>
<?php } ?>

<section id="news-list" class="section-margine blog-list">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <?php if (count($news) > 0) { ?>
          <?php foreach ($news as $key => $row_news) { ?>
            <div class="news-list-box">
              <h1 class="text-left"><?php echo $row_news->title; ?></h1>
              <br>
              <?php if ($row_news->news_category_id == 5) { ?>
                <?php echo html_entity_decode(htmlspecialchars_decode($row_news->video_embed)); ?>
              <?php } else { ?>
                <img src="<?php echo base_url() . $row_news->img; ?>" class="img-responsive" width="100%" alt="News">
              <?php } ?>
              <div class="news-description">
                <?php echo html_entity_decode(htmlspecialchars_decode($row_news->description)); ?>
              </div>
              <hr style="border: 1px solid #e7e7e7;">
              <div id="share"></div>
            </div>
            <script>
              var isMobile = false; //initiate as false
              // device detection
              if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) ||
                /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
                isMobile = true;
              }
              $("#share").jsSocials({
                showLabel: false,
                showCount: false,
                shares: [{
                    share: "twitter",
                    url: "<?php echo base_url() . "news/detail/" . $row_news->link_post . "?cat=" . $row_news->news_category_name; ?>#news-list"
                  },
                  {
                    share: "facebook",
                    url: "<?php echo base_url() . "news/detail/" . $row_news->link_post . "?cat=" . $row_news->news_category_name; ?>#news-list"
                  },
                  {
                    share: "linkedin",
                    url: "<?php echo base_url() . "news/detail/" . $row_news->link_post . "?cat=" . $row_news->news_category_name; ?>#news-list"
                  },
                  {
                    share: "pinterest",
                    url: "<?php echo base_url() . "news/detail/" . $row_news->link_post . "?cat=" . $row_news->news_category_name; ?>#news-list"
                  },
                  {
                    share: "whatsapp",
                    shareUrl: isMobile ? "whatsapp://send?text={url}" : "https://web.whatsapp.com/send?text={url}",
                    shareIn: "blank",
                    url: "<?php echo base_url() . "news/detail/" . $row_news->link_post . "?cat=" . $row_news->news_category_name; ?>#news-list"
                  },
                ]
              });
            </script>
          <?php } ?>
        <?php } else { ?>
          <div class="news-list-box">
            <h3>Conten Not Available Now</h3>
            <h5>Try again later...</h5>
            <img src="<?php echo base_url(); ?>assets/images/news/coming-soon.png" class="img-responsive" alt="News">
            <!-- <div class="news-description">
              <?php echo $row_news->description; ?>
            </div> -->
          </div>
        <?php } ?>
      </div>

      <?php if (count($news_archive) > 0) { ?>
        <div class="col-md-3 col-md-offset-1">
          <div class="news-list-box">
            <h4>Archive</h4>
            <ul>
              <?php foreach ($news_archive as $key => $row_news_archive) { ?>
                <li class="underline"><i class="fa fa-caret-right" aria-hidden="true"></i> <a href="<?php echo base_url() . "news/archive/" . $row_news_archive->year . "/0?cat=" . $row_news_archive->news_category_name; ?>#news-list"><?php echo $row_news_archive->year; ?></a></li>
              <?php } ?>
            </ul>
          </div>
        </div>
      <?php } ?>
      <div class="col-md-3 col-md-offset-1">
        <div class="news-list-box">
          <a href="https://www.glomart.id" target="blank">
            <img src="<?php echo base_url(); ?>assets/images/ads_sidebar/ads1.jpg" alt="">
          </a>
          <p></p>
          <a href="https://www.glomart.id" target="blank">
            <img src="<?php echo base_url(); ?>assets/images/ads_sidebar/ads2.jpg" alt="">
          </a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8">
        <div class="news-list-box">
          <div class="news-list-box-pager text-right">
            <div id="pagination">
              <?php echo $this->pagination->create_links(); ?>
            </div>
            <!-- <a href="#"><img src="<?php echo base_url(); ?>assets/images/news/arrow-left.png" width="30"></a>
            <a href="#"><img src="<?php echo base_url(); ?>assets/images/news/arrow-right.png" width="30"></a> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- <div class="footer1">
  <p>Footer</p>
</div> -->


<?php if ($parallax1) { ?>
  <script>
    if ($("p")[1]) {
      var a = document.createElement("a");
      a.setAttribute("href", "<?php echo $parallax1['link']; ?>");
      a.setAttribute("target", "_blank");
      var btn = document.createElement("div");
      btn.className = 'parallax1 fade-in';
      $("p")[1].appendChild(a);
      a.appendChild(btn);
    }
  </script>
<?php } ?>

<?php if ($parallax2) { ?>
  <script>
    if ($("p")[7]) {
      var a = document.createElement("a");
      a.setAttribute("href", "<?php echo $parallax2['link']; ?>");
      a.setAttribute("target", "_blank");
      var btn = document.createElement("div");
      btn.className = 'parallax2 fade-in';
      $("p")[7].appendChild(a);
      a.appendChild(btn);
    }
  </script>
<?php } ?>

<?php if ($parallax3) { ?>
  <script>
    if ($("p")[14]) {
      var a = document.createElement("a");
      a.setAttribute("href", "<?php echo $parallax3['link']; ?>");
      a.setAttribute("target", "_blank");
      var btn = document.createElement("div");
      btn.className = 'parallax3 fade-in';
      $("p")[14].appendChild(a);
      a.appendChild(btn);
    }
  </script>
<?php } ?>

<?php if ($parallax4) { ?>
  <script>
    if ($("p")[24]) {
      var a = document.createElement("a");
      a.setAttribute("href", "<?php echo $parallax4['link']; ?>");
      a.setAttribute("target", "_blank");
      var btn = document.createElement("div");
      btn.className = 'parallax4 fade-in';
      $("p")[24].appendChild(a);
      a.appendChild(btn);
    }
  </script>
<?php } ?>
</script>