<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('inc_backend/head'); ?>
</head>
<style>
    .error {
        color: red;
    }
</style>
<body class="hold-transition sidebar-mini">
    <?php $this->load->view('inc_backend/header'); ?>

    <?php echo $contents; ?>
    <?php $this->load->view('inc_backend/footer'); ?>
</body>

</html>