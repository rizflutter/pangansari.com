<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="header-bottom">
  <img src="<?php echo base_url(); ?>assets/images/pangansarigroup/PangansariGroup-47.png" width="100%">
</div>

<div class="pangansarigroup-structure">
  <img src="<?php echo base_url(); ?>assets/images/pangansarigroup/Struktur-Bisnis-02.jpg" alt="Snow" style="width:100%;">
  <div class="container">
    <div class="row">
      <div class="pangansarigroup-structure-title">Pangansari<br>Business Structure</div>
    </div>
  </div>
</div>

<!-- <div class="pangansarigroup-structure">
  <img src="<?php echo base_url(); ?>assets/images/pangansarigroup/PangansariGroup-48.png" alt="Snow" style="width:100%;">
  <div class="pangansarigroup-structure-title">Pangansari<br>Business Structure</div>
</div> -->

<section id="home-aboutus" class="home-aboutus">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-6">
        <div class="home-top-box">
          <h2>About Us</h2>
          <p align="justify">PUFR GROUP is an integrated food manufacturing, distribution, and catering company operating in 33 Indonesian provinces since 1975. Its operations are carried out by the catering company (PSU), meat manufacturer (DDFI), food manufacturer (PUFI), supply chain (PUFD), pastry & coffee (PUPAT), vegetable processing (PUMS), security services (DPL), and trader (NBSU).</p>
          <p align="justify">The integrated approach enables PUFR to devise cost-efficient, seamless solutions for all customers’ support needs under one roof, freeing clients to focus on their core business. Pangansari Utama Group regularly supplies to multinationals and well-known domestic companies, including handling the needs of remote oil and gas operations.</p>
          <br>
          <p>
            <a href="#" class="slider btn btn-primary">Read More</a>
          </p>
        </div>
    </div>
  </div>
</section>

<section id="pangansarigroup-visionmission" class="pangansarigroup-visionmission">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-lg-6" align="center">
        <div class="home-top-box">
          <div class="vissionmission">
              <img src="<?php echo base_url(); ?>assets/images/pangansarigroup/PangansariGroup-51.png"/ alt=""  width="120" >
              <h3>Vision</h3>
              <p>To become a leading integrated food company with international performance standards.</p>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-6" align="center">
        <div class="home-top-box">
          <div class="vissionmission">
              <img src="<?php echo base_url(); ?>assets/images/pangansarigroup/PangansariGroup-52.png"/ alt=""  width="120" >
              <h3>Mission</h3>
              <p>To provide food products and services to catering and wholesale customers that will add value for our customers, suppliers and investors. Accomplished through experienced staff, wide network of business associations, warehousing and distribution capabilities and manufacturing facilities.</p>
          </div>
        </div>
      </div>
  </div>
</section>