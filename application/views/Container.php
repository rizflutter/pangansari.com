<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('inc/head'); ?>
    <script>
        $(window).load(function() {
            setTimeout(function(){
                $("#loading").hide();
                $(".loader").hide();
            },0);
        });
    </script>
</head>
<?php if($this->session->flashdata('info')){ ?>
    <script>
        $(document).ready(function() {
            // show when page load
            toastr.info('<?php echo $this->session->flashdata('info'); ?>');
        });
    </script>
    <?php } ?>

    <?php if($this->session->flashdata('success')){ ?>
    <script>
        $(document).ready(function() {
            // show when page load
            toastr.success('<?php echo $this->session->flashdata('success'); ?>');
        });
    </script>
    <?php } ?>

    <?php if($this->session->flashdata('error')){  ?>
        <script>
        $(document).ready(function() {
            // show when page load
            toastr.error('<?php echo $this->session->flashdata('error'); ?>');
        });
    </script>
    <?php } ?>

    <?php if($this->session->flashdata('warning')){  ?>
        <script>
        $(document).ready(function() {
            // show when page load
            toastr.warning('<?php echo $this->session->flashdata('warning'); ?>');
        });
    </script>
    <?php } ?>
<body>
    <div id="loading">
        <span class="loader"></span>
    </div>

	<?php $this->load->view('inc/header'); ?>
		<?php echo $contents; ?>
	<?php $this->load->view('inc/footer'); ?>
	<?php $this->load->view('inc/foot'); ?>
</body>

</html>