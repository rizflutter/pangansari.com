<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title><?php echo ucwords($subject); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <style type="text/css">
        @page { 
            mso-page-orientation : potrait;
            size                 : 21cm 29.7cm ;
            margin               : 2.54cm 2.54cm 2.54cm 2.54cm;
        }

        @page Section1 {
            mso-header-margin : .5in;
            mso-footer-margin : .5in;
            mso-header        : h1;
            mso-footer        : f1;
        }

        div.Section1 {
            page : Section1;
        }

        p {
            font-family : 'Verdana', 'sans-serif';
            color       : #000;
            font-size   : 12px;
            line-height: 10px;
            margin: 3px;
        }

        table.header {
            font-family : 'Verdana', 'sans-serif';
            color       : #FFF;
            font-size   : 12px;
        }
        table.header th {
            padding: 0px 10px 0px 10px;
        }

        table.outline {
            font-family : 'Verdana', 'sans-serif';
            color       : #000;
            line-height : 10px;
            font-size   : 12px;
            border-collapse: collapse;
        }

        table.outline td{
            padding: 10px 20px 10px 20px;
        }

        table.table1 {
            font-family : 'Verdana', 'sans-serif';
            color       : #000;
            line-height : 10px;
            font-size   : 12px;
        }

        table.table1 td{
            padding: 2px 5px 2px 5px;
        }

    </style>
</head>

<body>
<p>Dear Marketing PT. Pangansari Utama,</p>
<p>Berikut email dari <?php echo ucwords($name); ?> | <?php echo $email; ?> :</p>
<p style="white-space: pre-wrap;"><?php echo $message; ?></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Terima Kasih</p>
<p>&nbsp;</p>
<p>*Pesan ini dikirim otomatis oleh http://www.pangansari.com</p>
</body>
</html>