<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="header-bottom">
  <img src="<?php echo base_url(); ?>assets/images/career/Career-29.png" width="100%">
</div>

<section id="career" class="section-margine blog-list">
  <div class="container">
    <?php
      $i = 0;
      $len = count($career);
      
      foreach ($career as $key => $row_career) { ?>
      <div class="row">
        <div class="col-md-12 col-lg-12">
          <div class="career-box">
            <h3 class="text-left"><?php echo $row_career->title; ?></h3>
            <div>
              <?php echo html_entity_decode(htmlspecialchars_decode($row_career->description)); ?>
            </div>            
            <div class="row">
              <div class="col-md-12 col-lg-12">
                <div class="text-right"><a href="<?php echo $row_career->link; ?>" class="btn btn-primary">Apply</a></div>
              </div>
            </div>
            <?php if($i < $len-1) { ?><hr><?php  } ?>
          </div>
        </div>
      </div>
    <?php $i++; } ?>
    <div class="row">
      <div class="col-md-12 col-lg-12">
        <div class="career-box">
            <div class="career-box-pager text-right">
              <div id="pagination">
                <?php 
                  echo $this->pagination->create_links();
                ?>
              </div>
              <!-- <a href="#"><img src="<?php echo base_url(); ?>assets/images/news/arrow-left.png" width="30"></a>
              <a href="#"><img src="<?php echo base_url(); ?>assets/images/news/arrow-right.png" width="30"></a> -->
            </div>
          </div>
        </div>
      </div>
  </div>
</section>