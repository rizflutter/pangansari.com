<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><?= $pg_title; ?></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><?= $pg_menu; ?></li>
                        <li class="breadcrumb-item"><?= $pg_title; ?></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-8">
                    <!-- alert success -->
                    <?php if ($this->session->flashdata('flash')) { ?>
                        <?php if ($this->session->flashdata('flash') == 'edit_success') { ?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <p class="text-center">Data berhasil diubah!</p>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php } ?>
                        <!-- alert error upload -->
                        <?php if ($this->session->flashdata('img')) { ?>
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <p class="text-center"><?= "Gagal upload image" . $this->session->flashdata('img'); ?></p>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Edit <?= $pg_title; ?></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <?php echo form_open("Slider_Home/edit_act", array('enctype' => 'multipart/form-data', 'id' => 'slider-home-form')); ?>
                        <div class="card-body">
                            <div class="form-group">
                                <label>Urutan Gambar</label>
                                <br>
                                <?php foreach ($get_business_category_by_id as $bci) :
                                    echo "<img src=" . base_url($bci->img) . " alt='' height='50'>" . $bci->sort . "| ";
                                endforeach; ?>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="id" value="<?= $slider_row->id; ?>">
                                <label for="">Business Category Description</label>
                                <input type="text" class="form-control" placeholder="" value="<?= $slider_row->business_category_description; ?>" disabled>
                                <?php echo form_error('index', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="">Urutan</label>
                                <input type="number" class="form-control" id="" placeholder="" value="<?= $slider_row->sort; ?>" disabled>
                            </div>
                            <div class="form-group">
                                <label for="">Judul 1</label>
                                <input type="text" name="title1" class="form-control" id="" placeholder="Masukkan judul 1" value="<?= $slider_row->title1; ?>">
                                <?php echo form_error('title1', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="">Judul 2</label>
                                <input type="text" name="title2" class="form-control" id="" placeholder="Masukkan judul 2" value="<?= $slider_row->title2; ?>">
                                <?php echo form_error('title2', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="">Judul 3</label>
                                <input type="text" name="title3" class="form-control" id="" placeholder="Masukkan judul 2" value="<?= $slider_row->title3; ?>">
                                <?php echo form_error('title3', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="">Judul 4</label>
                                <input type="text" name="title4" class="form-control" id="" placeholder="Masukkan judul 2" value="<?= $slider_row->title4; ?>">
                                <?php echo form_error('title4', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="">Description</label>
                                <textarea name="description" id="" class="form-control" placeholder="Masukkan deskripsi"><?= $slider_row->description; ?></textarea>
                                <?php echo form_error('description', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label>Start Date</label>
                                <input type="date" class="form-control" value="<?= date_format(date_create($slider_row->start_date), 'Y-m-d'); ?>" name="start_date" />
                                <?php echo form_error('finish_date', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label>Finish Date</label>
                                <div class="input-group">
                                    <input type="date" class="form-control" value="<?= date_format(date_create($slider_row->finish_date), 'Y-m-d'); ?>" name="finish_date" />
                                </div>
                                <?php echo form_error('finish_date', '<div class="error">', '</div>'); ?>
                            </div>
                            <img src="<?= base_url($slider_row->img); ?>" alt="" height="100">
                            <div class="form-group">
                                <label for="exampleInputFile">Gambar</label>
                                <div class="input-group">
                                    <input type="file" name="img" class="form-control" id="exampleInputFile">
                                </div>
                                <p>*Max Size 2MB - Max Height 1334px & Max Width 556px</p>
                            </div>
                            <div class="form-group">
                                <label for="">Is Active ?</label>
                                <select name="is_active" id="" class="form-control">
                                    <option value="0" <?php if ($slider_row->is_active == 1) {
                                                            echo 'selected';
                                                        }; ?>>Disable</option>
                                    <option value="1" <?php if ($slider_row->is_active == 1) {
                                                            echo 'selected';
                                                        }; ?>>Active</option>
                                </select>
                            </div>
                            <!-- <?php
                            if ($this->ion_auth->in_group($groups)) { ?>
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" name="is_admit" type="checkbox" id="customCheckbox1" value="1" <?php if ($slider_row->is_admit == 1) { echo 'checked'; } ?>>
                                        <label for="customCheckbox1" class="custom-control-label">Approved ?</label>
                                    </div>
                                </div>
                            <?php } ?> -->
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a href="<?= base_url('Slider_Home'); ?>" class="btn btn-danger">Kembali</a>
                        </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
</div>