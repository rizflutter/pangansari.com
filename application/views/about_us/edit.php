<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><?= $pg_title; ?></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><?= $pg_menu; ?></li>
                        <li class="breadcrumb-item"><?= $pg_title; ?></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-8">
                    <!-- alert success -->
                    <?php if ($this->session->flashdata('flash')) { ?>
                        <?php if ($this->session->flashdata('flash') == 'edit_success') { ?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <p class="text-center">Data berhasil diubah!</p>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Edit <?= $pg_title; ?></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <?= form_open("About_Us/edit_act", array('enctype' => 'multipart/form-data', 'id' => 'about-us-form')); ?>
                        <div class="card-body">
                            <input type="hidden" name="id" value="<?= $about_us->id; ?>">
                            <div class="form-group">
                                <label for="">Judul 1</label>
                                <input type="text" name="title1" class="form-control" id="" placeholder="Masukkan judul 1" value="<?= $about_us->title1; ?>">
                                <?= form_error('title1', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="">Judul 2</label>
                                <input type="text" name="title2" class="form-control" id="" placeholder="Masukkan judul 2" value="<?= $about_us->title2; ?>">
                                <?= form_error('title2', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="">Judul 3</label>
                                <input type="text" name="title3" class="form-control" id="" placeholder="Masukkan judul 3" value="<?= $about_us->title3; ?>">
                                <?= form_error('title3', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="">Judul 4</label>
                                <input type="text" name="title4" class="form-control" id="" placeholder="Masukkan judul 4" value="<?= $about_us->title4; ?>">
                                <?= form_error('title4', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="">Deskripsi</label>
                                <textarea name="description" class="form-control" placeholder="Masukkan deskripsi"><?= $about_us->description; ?></textarea>
                                <?php echo form_error('description', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="">Is Active ?</label>
                                <select name="is_active" id="" class="form-control">
                                    <option value="0" <?php if ($about_us->is_active == 1) {
                                                            echo 'selected';
                                                        }; ?>>Disable</option>
                                    <option value="1" <?php if ($about_us->is_active == 1) {
                                                            echo 'selected';
                                                        }; ?>>Active</option>
                                </select>
                            </div>
                            <!-- <?php if ($this->ion_auth->in_group($groups)) { ?>
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" name="is_admit" type="checkbox" id="customCheckbox1" value="1" <?php if ($about_us->is_admit == 1) { echo 'checked'; } ?>>
                                    <label for="customCheckbox1" class="custom-control-label">Approved ?</label>
                                </div>
                            </div>
                            <?php } ?> -->
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" name="is_posting" type="checkbox" id="customCheckbox2" value="1" <?php if ($about_us->is_posting == 1) { echo 'checked'; } ?> onclick="return confirm('Yakin ingin mengganti About Us ?');">
                                    <label for="customCheckbox2" class="custom-control-label">Posting ?</label>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a href="<?= base_url('About_Us'); ?>" class="btn btn-danger">Kembali</a>
                        </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
</div>