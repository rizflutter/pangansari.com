<section id="home-aboutus" class="home-aboutus">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-6">
        <div class="home-top-box">
          <?php if(!empty($about_us)) { ?>
          <h2><?= $about_us->title1; ?></h2>
          <?php if(!empty($about_us->title2)){ ?>
            <h2><?= $about_us->title2; ?></h2>
          <?php } ?>
          <?php if(!empty($about_us->title3)){ ?>
            <h2><?= $about_us->title3; ?></h2>
          <?php } ?>
          <?php if(!empty($about_us->title4)){ ?>
            <h2><?= $about_us->title4; ?></h2>
          <?php } ?>
          <p align="justify"><?= $about_us->description; ?></p>
          <!-- <p align="justify">PUFR GROUP is an integrated food manufacturing, distribution, and catering company operating in 33 Indonesian provinces since 1975. Its operations are carried out by the catering company (PSU), meat manufacturer (DDFI), food manufacturer (PUFI), supply chain (PUFD), pastry & coffee (PUPAT), vegetable processing (PUMS), security services (DPL), and trader (NBSU).</p>
          <p align="justify">The integrated approach enables PUFR to devise cost-efficient, seamless solutions for all customers’ support needs under one roof, freeing clients to focus on their core business. Pangansari Utama Group regularly supplies to multinationals and well-known domestic companies, including handling the needs of remote oil and gas operations.</p> -->
          <?php } else { ?>
            <h2>About Us</h2>
            <p align="justify">About Us belum ditambahkan!</p>
         <?php } ?>
        </div>
    </div>
  </div>
</section>

<section id="pangansarigroup-visionmission" class="pangansarigroup-visionmission">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-lg-6" align="center">
        <div class="home-top-box">
          <div class="vissionmission">
              <img src="<?php echo base_url(); ?>assets/images/pangansarigroup/PangansariGroup-51.png"/ alt=""  width="120" >
              <h3>Vision</h3>
              <p>To become a leading integrated food company with international performance standards.</p>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-6" align="center">
        <div class="home-top-box">
          <div class="vissionmission">
              <img src="<?php echo base_url(); ?>assets/images/pangansarigroup/PangansariGroup-52.png"/ alt=""  width="120" >
              <h3>Mission</h3>
              <p>To provide food products and services to catering and wholesale customers that will add value for our customers, suppliers and investors. Accomplished through experienced staff, wide network of business associations, warehousing and distribution capabilities and manufacturing facilities.</p>
          </div>
        </div>
      </div>
  </div>
</section>