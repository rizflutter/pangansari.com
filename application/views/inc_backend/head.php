<meta charset="utf-8">
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<!-- Site Title   -->
<title>PT. Pangansari Utama Food Resources</title>
<!-- Fav Icons   -->
<link rel="icon" href="<?= base_url(); ?>assets/images/favicon.ico" type="image/x-icon">

<!-- Google Font: Source Sans Pro -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<!-- Font Awesome -->
<link rel="stylesheet" href="<?= base_url(); ?>assets/adminlte/plugins/fontawesome-free/css/all.min.css">
<!-- daterange picker -->
<!-- <link rel="stylesheet" href="<?= base_url(); ?>assets/adminlte/plugins/daterangepicker/daterangepicker.css"> -->
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?= base_url(); ?>assets/adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
<!-- Bootstrap Color Picker -->
<!-- <link rel="stylesheet" href="<?= base_url(); ?>assets/adminlte/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css"> -->
<!-- Tempusdominus Bootstrap 4 -->
<!-- <link rel="stylesheet" href="<?= base_url(); ?>assets/adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css"> -->
<!-- Select2 -->
<!-- <!-- <link rel="stylesheet" href="<?= base_url(); ?>assets/adminlte/plugins/select2/css/select2.min.css"> -->
<!-- <link rel="stylesheet" href="<?= base_url(); ?>assets/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css"> -->
<!-- Bootstrap4 Duallistbox -->
<!-- <link rel="stylesheet" href="<?= base_url(); ?>assets/adminlte/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css"> -->
<!-- BS Stepper -->
<!-- <link rel="stylesheet" href="<?= base_url(); ?>assets/adminlte/plugins/bs-stepper/css/bs-stepper.min.css"> -->
<!-- dropzonejs -->
<!-- <link rel="stylesheet" href="<?= base_url(); ?>assets/adminlte/plugins/dropzone/min/dropzone.min.css"> -->
<!-- Theme style -->
<link rel="stylesheet" href="<?= base_url(); ?>assets/adminlte/dist/css/adminlte.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url(); ?>assets/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<!-- <link rel="stylesheet" href="<?= base_url(); ?>assets/adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css"> -->