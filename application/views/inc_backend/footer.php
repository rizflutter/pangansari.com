<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
        <h5>Title</h5>
        <p>Sidebar content</p>
    </div>
</aside>
<!-- /.control-sidebar -->

<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
        dev 1.1
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; <?= date('Y'); ?> <a href="#">PSU</a>.</strong> All rights reserved.
</footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="<?= base_url(); ?>assets/adminlte/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?= base_url(); ?>assets/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- jquery-validation -->
<script src="<?= base_url(); ?>assets/adminlte/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?= base_url(); ?>assets/adminlte/plugins/jquery-validation/additional-methods.min.js"></script>
<!-- DataTables -->
<script src="<?= base_url(); ?>assets/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url(); ?>assets/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<!-- <script src="<?= base_url(); ?>assets/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?= base_url(); ?>assets/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script> -->
<!-- Select2 -->
<!-- <script src="<?= base_url(); ?>assets/adminlte/plugins/select2/js/select2.full.min.js"></script> -->
<!-- Bootstrap4 Duallistbox -->
<!-- <script src="<?= base_url(); ?>assets/adminlte/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script> -->
<!-- InputMask -->
<!-- <script src="<?= base_url(); ?>assets/adminlte/plugins/moment/moment.min.js"></script> -->
<!-- <script src="<?= base_url(); ?>assets/adminlte/plugins/inputmask/jquery.inputmask.min.js"></script> -->
<!-- date-range-picker -->
<!-- <script src="<?= base_url(); ?>assets/adminlte/plugins/daterangepicker/daterangepicker.js"></script> -->
<!-- bootstrap color picker -->
<!-- <script src="<?= base_url(); ?>assets/adminlte/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script> -->
<!-- Tempusdominus Bootstrap 4 -->
<!-- <script src="<?= base_url(); ?>assets/adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script> -->
<!-- Bootstrap Switch -->
<!-- <script src="<?= base_url(); ?>assets/adminlte/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script> -->
<!-- AdminLTE App -->
<script src="<?= base_url(); ?>assets/adminlte/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url(); ?>assets/adminlte/dist/js/demo.js"></script>

<!-- custom js -->
<script>
    // Buat variabel base_url agar bisa di akses di semua file js
    var base_url = '<?= base_url(); ?>';
    var uri = '<?= $this->uri->segment(1); ?>';
    // console.log(base_url + uri + '/ajax_list');
</script>

<!-- datatables -->
<script src="<?= base_url(); ?>assets/js/custom-datatables.js"></script>

<!-- is_admit_update -->
<script src="<?= base_url(); ?>assets/js/custom-is-admit.js"></script>
<!-- company_profile -->
<script src="<?= base_url(); ?>assets/js/custom-company-profile-validation.js"></script>
<!-- slider_home -->
<!-- <script src="<?= base_url(); ?>assets/js/custom-ajax-slider-home.js"></script> -->
<script src="<?= base_url(); ?>assets/js/custom-slider-home-validation.js"></script>
<!-- business_category -->
<script src="<?= base_url(); ?>assets/js/custom-ajax-crud-business-category.js"></script>
<!-- slider_product -->
<!-- <script src="<?= base_url(); ?>assets/js/custom-ajax-slider-product.js"></script> -->
<script src="<?= base_url(); ?>assets/js/custom-slider-product-validation.js"></script>
<!-- about_us -->
<script src="<?= base_url(); ?>assets/js/custom-about-us-validation.js"></script>
<!-- news_category -->
<script src="<?= base_url(); ?>assets/js/custom-news-category-validation.js"></script>
<!-- news_backend -->
<script src="<?= base_url(); ?>assets/js/custom-news-backend-validation.js"></script>
<!-- ckeditor dan kcfinder -->
<script src="<?= base_url('assets/ckeditor/ckeditor.js'); ?>"></script>
<?php if (($this->uri->segment(1) == "News_Backend" and $this->uri->segment(2) == "add") or ($this->uri->segment(1) == "News_Backend" and $this->uri->segment(2) == "add_act") or ($this->uri->segment(1) == "News_Backend" and $this->uri->segment(2) == "edit") or ($this->uri->segment(1) == "News_Backend" and $this->uri->segment(2) == "edit_act") or ($this->uri->segment(1) == "Career_Backend" and $this->uri->segment(2) == "add") or ($this->uri->segment(1) == "Career_Backend" and $this->uri->segment(2) == "edit")) { ?>
    <script type="text/javascript">
        $(function() {
            CKEDITOR.replace('news-ckeditor', {
                height: '400px'
            });
        });
    </script>
<?php } ?>
<!-- parallax -->
<script src="<?= base_url(); ?>assets/js/custom-parallax-validation.js"></script>
<!-- career -->
<script src="<?= base_url(); ?>assets/js/custom-career-backend-validation.js"></script>