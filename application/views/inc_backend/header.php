<?php $user_logged_in = $this->ion_auth->user()->row(); ?>
<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
            <!-- <li class="nav-item d-none d-sm-inline-block">
                <a href="index3.html" class="nav-link">Home</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">Contact</a>
            </li> -->
        </ul>

        <!-- SEARCH FORM -->
        <!-- <form class="form-inline ml-3">
            <div class="input-group input-group-sm">
                <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-navbar" type="submit">
                        <i class="fas fa-search"></i>
                    </button>
                </div>
            </div>
        </form> -->

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <!-- Messages Dropdown Menu -->
            <li class="nav-item dropdown">
                <!-- <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="far fa-comments"></i>
                    <span class="badge badge-danger navbar-badge">3</span>
                </a> -->
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <a href="#" class="dropdown-item">
                        <!-- Message Start -->
                        <div class="media">
                            <img src="<?= base_url(); ?>assets/adminlte/dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                            <div class="media-body">
                                <h3 class="dropdown-item-title">
                                    Brad Diesel
                                    <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                                </h3>
                                <p class="text-sm">Call me whenever you can...</p>
                                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                            </div>
                        </div>
                        <!-- Message End -->
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <!-- Message Start -->
                        <div class="media">
                            <img src="<?= base_url(); ?>assets/adminlte/dist/img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                            <div class="media-body">
                                <h3 class="dropdown-item-title">
                                    John Pierce
                                    <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                                </h3>
                                <p class="text-sm">I got your message bro</p>
                                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                            </div>
                        </div>
                        <!-- Message End -->
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <!-- Message Start -->
                        <div class="media">
                            <img src="<?= base_url(); ?>assets/adminlte/dist/img/user3-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                            <div class="media-body">
                                <h3 class="dropdown-item-title">
                                    Nora Silvester
                                    <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                                </h3>
                                <p class="text-sm">The subject goes here</p>
                                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                            </div>
                        </div>
                        <!-- Message End -->
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
                </div>
            </li>
            <!-- Notifications Dropdown Menu -->
            <li class="nav-item dropdown">
                <!-- <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="far fa-bell"></i>
                    <span class="badge badge-warning navbar-badge">15</span>
                </a> -->
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <span class="dropdown-header">15 Notifications</span>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <i class="fas fa-envelope mr-2"></i> 4 new messages
                        <span class="float-right text-muted text-sm">3 mins</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <i class="fas fa-users mr-2"></i> 8 friend requests
                        <span class="float-right text-muted text-sm">12 hours</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <i class="fas fa-file mr-2"></i> 3 new reports
                        <span class="float-right text-muted text-sm">2 days</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
                </div>
            </li>
            <li class="nav-item">
                <!-- <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button"><i class="fas fa-th-large"></i></a> -->
            </li>
        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="<?= base_url(); ?>" class="brand-link">
            <!-- <img src="<?= base_url(); ?>assets/adminlte/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8"> -->
            <img src="<?= base_url(); ?>assets/images/logo-pangansari.png" alt="PanganSari Logo" width="200" class="ml-2">
            <span class="brand-text font-weight-light"></span>
            <!-- <span class="brand-text font-weight-light">PSU</span> -->
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="<?= base_url().$user_logged_in->img; ?>" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                    <a href="<?= base_url('profile'); ?>" class="d-block"><?= ucwords($user_logged_in->first_name) ?></a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                    <li class="nav-item has-treeview <?php if ($this->uri->segment(1) == 'Company_Profile' or $this->uri->segment(1) == 'Slider_Home' or $this->uri->segment(1) == 'Business_Category' or $this->uri->segment(1) == 'Slider_Product' or $this->uri->segment(1) == 'About_Us') {
                                                            echo 'menu-open';
                                                        } ?>">
                        <a href="#" class="nav-link <?php if ($this->uri->segment(1) == 'Company_Profile' or $this->uri->segment(1) == 'Slider_Home' or $this->uri->segment(1) == 'Business_Category' or $this->uri->segment(1) == 'Slider_Product' or $this->uri->segment(1) == 'About_Us') {
                                                        echo 'active';
                                                    } ?>">
                            <i class="nav-icon fas fa-home"></i>
                            <p>
                                Home
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview ">
                        <li class="nav-item">
                                <a href="<?= base_url('Company_Profile'); ?>" class="nav-link
                                 <?php if ($this->uri->segment(1) == 'Company_Profile') {
                                        echo 'active';
                                    } ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Company Profile</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('Slider_Home'); ?>" class="nav-link
                                 <?php if ($this->uri->segment(1) == 'Slider_Home') {
                                        echo 'active';
                                    } ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Slider Home</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('Business_Category'); ?>" class="nav-link
                                <?php if ($this->uri->segment(1) == 'Business_Category') {
                                    echo 'active';
                                } ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Business Category</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('Slider_Product'); ?>" class="nav-link
                                <?php if ($this->uri->segment(1) == 'Slider_Product') {
                                    echo 'active';
                                } ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Slider Product</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('About_Us'); ?>" class="nav-link
                                <?php if ($this->uri->segment(1) == 'About_Us') {
                                    echo 'active';
                                } ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>About Us</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview <?php if ($this->uri->segment(1) == 'News_Category' or $this->uri->segment(1) == 'News_Backend' or $this->uri->segment(1) == 'Parallax') {
                                                            echo 'menu-open';
                                                        } ?>">
                        <a href="#" class="nav-link <?php if ($this->uri->segment(1) == 'News_Category' or $this->uri->segment(1) == 'News_Backend' or $this->uri->segment(1) == 'Parallax') {
                                                        echo 'active';
                                                    } ?>">
                            <i class="nav-icon fas fa-newspaper"></i>
                            <p>
                                News
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= base_url('News_Category'); ?>" class="nav-link
                                <?php if ($this->uri->segment(1) == 'News_Category') {
                                    echo 'active';
                                } ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>News Category</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('News_Backend'); ?>" class="nav-link
                                <?php if ($this->uri->segment(1) == 'News_Backend') {
                                    echo 'active';
                                } ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>News</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('Parallax'); ?>" class="nav-link
                                <?php if ($this->uri->segment(1) == 'Parallax') {
                                    echo 'active';
                                } ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Parallax</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview <?php if ($this->uri->segment(1) == 'Career_Backend') {
                                                            echo 'menu-open';
                                                        } ?>">
                        <a href="#" class="nav-link <?php if ($this->uri->segment(1) == 'Career_Backend') {
                                                        echo 'active';
                                                    } ?>">
                            <i class="nav-icon fas fa-user-tie"></i>
                            <p>
                                Career
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?= base_url('Career_Backend'); ?>" class="nav-link <?php if ($this->uri->segment(1) == 'Career_Backend') {
                                                                                                    echo 'active';
                                                                                                } ?>">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Add Career</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url(); ?>auth/logout" class="nav-link" onclick="return confirm('Anda yakin ingin logout ?')">
                            <i class="nav-icon fas fa-sign-out-alt"></i>
                            <p>
                                Logout
                            </p>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>