<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="header-bottom">
  <img src="<?php echo base_url(); ?>assets/images/our_business/Our-Business-01.png" width="100%">
</div>

<section id="ourbusiness-catering" class="ourbusiness-catering">
  <div class="container">    
    <div class="row cont">
      <div class="col-md-12">
        <div class="home-top-box3">
          <h2 style="color: #264080;">Our Business</h2>
          <hr width="10%" align="left" style="border: 2px solid #7fb742;">
          <h2 style="color: #7fb742;">Industrial Catering & Related Services</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-lg-6">
        <div class="home-top-box3">
          <div class="section-5-box-text-cont">
            <img src="<?php echo base_url(); ?>assets/images/our_business/Our-Business-02.png" class="img-responsive" alt="">
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-6">
        <div class="section-5-box-text-cont">
          <div class="home-top-box" align="justify">
            <div class="description1">
              <h4 align="left">PT PANGANSARI UTAMA</h4>
              <p>PSU serves every corner of the archipelago and beyond, providing our services to construction sites, mining sites, offshore drilling platforms, factories, offices, hospitals, schools and the government sector. Not resting on its laurels, PSU continues to push to improve its capabilities, driven by its main focus of making client’s primary business easier for the long run. PSU has ISO 9001 : 2015, ISO 22000 : 2005, ISO 14001: 2015 and OHSAS 18001 : 2007 certified, and are rated by NOSA Occupational Health and Safety.</p>
              <p>PSU provides integrated support for all non-core needs to remote-area mining and energy exploration sites. PSU offers total maintenance services for clients, enabling them to concentrate on their core strengths especially in remote sites.</p>
              <p>Our services include remote site & industrial catering, housekeeping & accommodation services, camp management, gardening & landscaping, facilities maintenance, and laundry.</p>
              <button class="btn-plain" id="shower1">(read more)</button><button class="btn-plain" id="hider1">(read less)</button>  
            </div>
            <br>
            <div class="row">
              <div class="col-xs-6">
                <a href="http://pangansari.co.id" target="_blank" class="btn btn-psu">Visit PSU</a>
              </div>
              <div class="col-xs-6 text-right">
                <img src="<?php echo base_url(); ?>assets/images/our_business/Our-Business-18.png" width="80%">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="ourbusiness-productions" class="ourbusiness-productions">
  <div class="container">    
    <div class="row">
      <div class="col-md-12">
        <div class="home-top-box3">
          <h2 style="color: #7fb742;">Productions</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-lg-6">
        <div class="home-top-box3">
          <div class="section-5-box-text-cont">
            <img src="<?php echo base_url(); ?>assets/images/our_business/Our-Business-03.png" class="img-responsive" alt="">
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-6">
        <div class="section-5-box-text-cont">
          <div class="home-top-box" align="justify">
            <div class="description2">
              <h4 align="left">PT DUNIA DAGING FOOD INDUSTRIES</h4>
              <p>DDFI is one of the leading companies in industries that engage in food processing products which are halal, excellent in quality, innovative, hygiene and nutritious that meets the standard of food quality and safety and supported by reliable human resources, creativity, and sufficient food processing technology. DDFI believes to grow and develop in the dynamics of togetherness to create the harmony competition in transparencies through dedication, integrity, and teamwork among the stakeholders.</p>
              <p>As a growing and developing company, DDFI manufactures various products to meet the customer needs for home consumption, caterer and industrial needs; such as Gorudo, Fronte &amp; Nidia sausages, burgers, meatballs and cold cut products, also processed chicken and beef. All products are supported by various production systems and equipment. Preparation room, sausages line, nugget &amp; patties line, meatball (bakso) line, packing line, freezing system, and cold storage.</p>
              <button class="btn-plain" id="shower2">(read more)</button><button class="btn-plain" id="hider2">(read less)</button>  
            </div>
            <br>
            <div class="row">
              <div class="col-xs-6">
                <a href="http://ddfi.co.id" target="_blank" class="btn btn-ddfi">Visit DDFI</a>
              </div>
              <div class="col-xs-6 text-right">
                <img src="<?php echo base_url(); ?>assets/images/our_business/Our-Business-19.png" width="80%">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6 col-lg-6">
        <div class="home-top-box3">
          <div class="section-5-box-text-cont">
            <img src="<?php echo base_url(); ?>assets/images/our_business/Our-Business-04.png" class="img-responsive" alt="">
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-6">
        <div class="section-5-box-text-cont">
          <div class="home-top-box" align="justify">
            <div class="description3">
              <h4 align="left">PT PANGANSARI UTAMA FOOD INDUSTRY</h4>
              <p>Capitalizing on the food preparation expertise, PUFI provides an extensive menu of affordable, Halal, nutritious, and delicious preservative-free food choices to retail consumers and businesses, with a proven system and international standards of rigorous quality control. Sourcing, processing and producing high quality produce is a key priority for PUFI, both to satisfy its own exacting quality standards and to meet clients and consumers expectation.</p>
              <p>PUFI also provides a vast array of main dishes, bakery items, and spices as for both the wholesale and retail market. All products are supported with fully equipped modern and sophisticated machinery with international reputation standards; such as blast freezer and other production systems for producing hygienic and high quality products.</p>
              <button class="btn-plain" id="shower3">(read more)</button><button class="btn-plain" id="hider3">(read less)</button>  
            </div>
            <br>
            <div class="row">
              <div class="col-xs-6">
                <a href="http://pufi.co.id" target="_blank" class="btn btn-pufi">Visit PUFI</a>
              </div>
              <div class="col-xs-6 text-right">
                <img src="<?php echo base_url(); ?>assets/images/our_business/Our-Business-20.png" width="50%">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6 col-lg-6">
        <div class="home-top-box3">
          <div class="section-5-box-text-cont">
            <img src="<?php echo base_url(); ?>assets/images/our_business/Our-Business-05.png" class="img-responsive" alt="">
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-6">
        <div class="section-5-box-text-cont">
          <div class="home-top-box" align="justify">
            <div class="description4">
              <h4 align="left">PT PLASMA USAHA MITRA SELARAS</h4>
              <p>As our group owes its success in no small part to the support and encouragement of the communities and stakeholders around it, PUMS exists to give grass roots impact in the communities where we operate, through community development and Corporate Social Responsibility initiatives by working closely with local farmers to improve their produce.</p>
              <p>Engaging with all stakeholders including customers, suppliers and partners, PUMS strives to identify grass-roots ways where we can make a real difference. We invite numerous experts with different backgrounds who actively participate to exchange knowledge about contemporary farming techniques, helping them to improve the quality of their products and increase the possibility of becoming one of PUMS suppliers.</p>
              <p>Through this holistic approach, PUFR shares the benefits with its partners and the community in a tangible manner, making them partners in our endeavor and giving them a shared stake in the success, for long-term mutual benefit.</p>

              <button class="btn-plain" id="shower4">(read more)</button><button class="btn-plain" id="hider4">(read less)</button>  
            </div>
            <br>
            <div class="row">
              <div class="col-xs-6">
                <a href="#" class="btn btn-pums">Visit PUMS</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="ourbusiness-retails" class="ourbusiness-retails">
  <div class="container">    
    <div class="row">
      <div class="col-md-12">
        <div class="home-top-box3">
          <h2 style="color: #7fb742;">Retails</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-lg-6">
        <div class="home-top-box3">
          <div class="section-5-box-text-cont">
            <img src="<?php echo base_url(); ?>assets/images/our_business/Our-Business-06.png" class="img-responsive" alt="">
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-6">
        <div class="section-5-box-text-cont">
          <div class="home-top-box" align="justify">
            <div class="description5">
              <h4 align="left">PT NIAGA BUANA SOLUSI UTAMA</h4>
              <p>Growing from a food service company, PUFR Group expands their business in the distributions category, with a reliable support system in sales and marketing services. Expertise in chilled and frozen fresh products, NBSU, as one of PUFR Group&rsquo;s subsidiaries, is capable of providing their consumers in B2B area as well as retail demand.</p>
              <p>NBSU is also pleased to publish their own brands for food and services trading companies, named Glotrade, focusing in B2B area, and Glomart as its retail platform. Glotrade&rsquo;s products vary in categories; from frozen processed meat (sausages, meatballs, nuggets, hams), frozen meals (ready-to-eat), to imported frozen potatoes. In the future, Glotrade believes to improve their capabilities in adding more product&rsquo;s categories and expanding their region.</p>
              <button class="btn-plain" id="shower5">(read more)</button><button class="btn-plain" id="hider5">(read less)</button>
            </div>
            <br>
            <div class="row">
              <div class="col-xs-6">
                <a href="http://glomart.id" target="_blank" class="btn btn-nbsu">Visit NBSU</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6 col-lg-6">
        <div class="home-top-box3">
          <div class="section-5-box-text-cont">
            <img src="<?php echo base_url(); ?>assets/images/our_business/Our-Business-07.png" class="img-responsive" alt="">
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-6">
        <div class="section-5-box-text-cont">
          <div class="home-top-box" align="justify">
            <div class="description6">
              <h4 align="left">PT PANGANSARI UTAMA PATISSERIE</h4>
              <p>The continuing growth of the middle class in Indonesia, and an increase in disposable income, is stimulating an interest in more diverse tastes and a greater appreciation of lifestyle. By combining its distribution and food preparation expertise with the specialized skills of its highly experienced retail partners, PUPAT through caf&eacute; brand Analogy becomes the main solution for that demand. We provide this demand directly to the customers.</p>
              <p>Our pastry and bakery are both safe and delicious to consume. All productions are supported by a proven system and international standards with rigorous quality control. The quest to deliver top quality products starts with the selection and sourcing of the best ingredients. To ensure the safety, quality, hygiene, and consistency, our infrastructure has been set up for full traceability that helps to coordinate with partners and suppliers.</p>
              <button class="btn-plain" id="shower6">(read more)</button><button class="btn-plain" id="hider6">(read less)</button>
            </div>
            <br>
            <div class="row">
              <div class="col-xs-6">
                <a href="#" class="btn btn-pupat">Visit PUPAT</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="ourbusiness-trading" class="ourbusiness-trading">
  <div class="container">    
    <div class="row">
      <div class="col-md-12">
        <div class="home-top-box3">
          <h2 style="color: #7fb742;">Trading</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-lg-6">
        <div class="home-top-box3">
          <div class="section-5-box-text-cont">
            <img src="<?php echo base_url(); ?>assets/images/our_business/Our-Business-08.png" class="img-responsive" alt="">
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-6">
        <div class="section-5-box-text-cont">
          <div class="home-top-box">
            <div class="description7">
              <h4 align="left">PT PANGANSARI UTAMA FOOD DISTRIBUTIONS</h4>
              <p>PUFD supplies raw and manufactured food products in 33 provinces throughout Indonesia. Logistic bases are located in Ciracas (Jakarta), Kalianak (Surabaya), Purwakarta, Balikpapan, Medan, Palembang, Pekanbaru, Makassar, Sorong, and Timika. The logistics services include food trading, distribution, forwarding, warehousing, and transportation making PUFD as a regional logistics leader. PUFD continuously invests in capital upgrades to distribution and manufacturing capabilities. PUFD has experienced 20 year on year growth rates. Maintenance and operations of warehouse and distribution facilities are assured with ISO 22000 certification.</p>
              <p>PUFD has invested in a modern facility for frozen and chilled food with temperature control for processing and warehousing. Besides that, PUFD also has transportations which includes every size container to ensure the proper handling of the goods to the client&rsquo;s site. PUFD offers a fully integrated warehouse service which covers ordering products, bulk storage of backup supplies, order pick-up and distribution of mini-markets daily requirements of chilled, frozen or canned, snack box and prepackaged foods.</p>
              <button class="btn-plain" id="shower7">(read more)</button><button class="btn-plain" id="hider7">(read less)</button>
            </div>
            <br>
            <div class="row">
              <div class="col-xs-6">
                <a href="#" class="btn btn-pufd">Visit PUFD</a>
              </div>
              <div class="col-xs-6 text-right">
                <img src="<?php echo base_url(); ?>assets/images/our_business/Our-Business-19.png" width="80%">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="ourbusiness-outsourcing" class="ourbusiness-outsourcing">
  <div class="container">    
    <div class="row">
      <div class="col-md-12">
        <div class="home-top-box3">
          <h2 style="color: #7fb742;">Security services & Facility management</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-lg-6">
        <div class="home-top-box3">
          <div class="section-5-box-text-cont">
            <img src="<?php echo base_url(); ?>assets/images/our_business/Our-Business-09.png" class="img-responsive" alt="">
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-6">
        <div class="section-5-box-text-cont">
          <div class="home-top-box">
            <div class="description8">
              <h4 align="left">PT DAYA PRIMA LESTARI</h4>
              <p>As an integrated logistical support, PUFR Group offers total maintenance services for clients, enabling them to concentrate on their core strengths. DPL provides professional security services with highly dedicated personnel who understand their duties and functions as Special Force Unit to their clients. DPL also provides Security Training Services by providing physically, mentally and periodically specialist training with professional and experienced trainers in their field.</p>
              <p>DPL experiences in giving consultation, planning, developing, installing and maintenance also provides and facilitates clients in supplying security tools and provides training to give maximum services to clients.</p>
              <button class="btn-plain" id="shower8">(read more)</button><button class="btn-plain" id="hider8">(read less)</button>
            </div>
            <br>
            <div class="row">
              <div class="col-xs-6">
                <a href="https://dayaprimalestari.com" target="_blank" class="btn btn-dpl">Visit DPL</a>
              </div>
              <div class="col-xs-6 text-right">
                <img src="<?php echo base_url(); ?>assets/images/our_business/Our-Business-22.png" width="50%">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
paragraphCount1 = $(".description1 > p").size();

$("#hider1").hide();
$("#shower1").hide();

if (paragraphCount1 > 1) {
    $("#shower1").show();
}
  
$( "#hider1").click(function() {
    $(".description1 p").not(":first").hide();
    $("#hider1").hide();
    $("#shower1").show();
});
  
$( "#shower1").click(function() {
  $(".description1 p").show();
    $("#shower1").hide();
    $("#hider1").show();
});

$(".description1 p").not(":first").hide();

//-----------------------------------------------

paragraphCount2 = $(".description2 > p").size();

$("#hider2").hide();
$("#shower2").hide();

if (paragraphCount2 > 1) {
    $("#shower2").show();
}
  
$( "#hider2").click(function() {
    $(".description2 p").not(":first").hide();
    $("#hider2").hide();
    $("#shower2").show();
});
  
$( "#shower2").click(function() {
  $(".description2 p").show();
    $("#shower2").hide();
    $("#hider2").show();
});

$(".description2 p").not(":first").hide();

//-----------------------------------------------

paragraphCount3 = $(".description3 > p").size();

$("#hider3").hide();
$("#shower3").hide();

if (paragraphCount1 > 1) {
    $("#shower3").show();
}
  
$( "#hider3").click(function() {
    $(".description3 p").not(":first").hide();
    $("#hider3").hide();
    $("#shower3").show();
});
  
$( "#shower3").click(function() {
  $(".description3 p").show();
    $("#shower3").hide();
    $("#hider3").show();
});

$(".description3 p").not(":first").hide();

//-----------------------------------------------

paragraphCount4 = $(".description4 > p").size();

$("#hider4").hide();
$("#shower4").hide();

if (paragraphCount1 > 1) {
    $("#shower4").show();
}
  
$( "#hider4").click(function() {
    $(".description4 p").not(":first").hide();
    $("#hider4").hide();
    $("#shower4").show();
});
  
$( "#shower4").click(function() {
  $(".description4 p").show();
    $("#shower4").hide();
    $("#hider4").show();
});

$(".description4 p").not(":first").hide();

//-----------------------------------------------

paragraphCount5 = $(".description5 > p").size();

$("#hider5").hide();
$("#shower5").hide();

if (paragraphCount1 > 1) {
    $("#shower5").show();
}
  
$( "#hider5").click(function() {
    $(".description5 p").not(":first").hide();
    $("#hider5").hide();
    $("#shower5").show();
});
  
$( "#shower5").click(function() {
  $(".description5 p").show();
    $("#shower5").hide();
    $("#hider5").show();
});

$(".description5 p").not(":first").hide();

//-----------------------------------------------

paragraphCount6 = $(".description6 > p").size();

$("#hider6").hide();
$("#shower6").hide();

if (paragraphCount1 > 1) {
    $("#shower6").show();
}
  
$( "#hider6").click(function() {
    $(".description6 p").not(":first").hide();
    $("#hider6").hide();
    $("#shower6").show();
});
  
$( "#shower6").click(function() {
  $(".description6 p").show();
    $("#shower6").hide();
    $("#hider6").show();
});

$(".description6 p").not(":first").hide();

//-----------------------------------------------

paragraphCount7 = $(".description7 > p").size();

$("#hider7").hide();
$("#shower7").hide();

if (paragraphCount1 > 1) {
    $("#shower7").show();
}
  
$( "#hider7").click(function() {
    $(".description7 p").not(":first").hide();
    $("#hider7").hide();
    $("#shower7").show();
});
  
$( "#shower7").click(function() {
  $(".description7 p").show();
    $("#shower7").hide();
    $("#hider7").show();
});

$(".description7 p").not(":first").hide();

//-----------------------------------------------

paragraphCount8 = $(".description8 > p").size();

$("#hider8").hide();
$("#shower8").hide();

if (paragraphCount1 > 1) {
    $("#shower8").show();
}
  
$( "#hider8").click(function() {
    $(".description8 p").not(":first").hide();
    $("#hider8").hide();
    $("#shower8").show();
});
  
$( "#shower8").click(function() {
  $(".description8 p").show();
    $("#shower8").hide();
    $("#hider8").show();
});

$(".description8 p").not(":first").hide();
</script>