<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><?= $pg_title; ?></h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><?= $pg_menu; ?></li>
            <li class="breadcrumb-item"><?= $pg_title; ?></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <?php if ($this->session->flashdata('flash')) { ?>
            <?php if ($this->session->flashdata('flash') == 'edit_failed') { ?>
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <p class="text-center">Data gagal diubah, harap periksa kembali!</p>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <?php } else if ($this->session->flashdata('flash') == 'delete_success') { ?>
              <div class="alert alert-success alert-dismissible fade show" role="alert">
                <p class="text-center">Data berhasil dihapus</p>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
          <?php }
          } ?>
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">DataTable <?= $pg_title; ?></h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <a href="<?= base_url('Parallax/add') ?>" class="btn btn-success mb-3">Tambah</a>
              <div class="table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Urutan</th>
                      <th>Title</th>
                      <th>Description</th>
                      <th>Start</th>
                      <th>Finish</th>
                      <th>Img</th>
                      <th>Link</th>
                      <th>PostBy</th>
                      <th>Status</th>
                      <th>Admit</th>
                      <th>Created</th>
                      <?php if ($this->ion_auth->in_group($groups)) : ?>
                      <th>Approved</th>
                      <?php endif; ?>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <!-- /.col-md-12 -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->