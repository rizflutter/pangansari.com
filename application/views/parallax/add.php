<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><?= $pg_title; ?></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><?= $pg_menu; ?></li>
                        <li class="breadcrumb-item"><?= $pg_title; ?></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- alert success -->
                    <?php if ($this->session->flashdata('flash')) { ?>
                        <?php if ($this->session->flashdata('flash') == 'add_success') { ?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <p class="text-center">Data berhasil ditambah!</p>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php if ($this->session->flashdata('img')) { ?>
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <p class="text-center"><?= "Gagal upload image" . $this->session->flashdata('img'); ?></p>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <?php } ?>
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Tambah <?= $pg_title; ?></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <?= form_open("Parallax/add_act", array('enctype' => 'multipart/form-data', 'id' => 'parallax-form')); ?>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="">Title</label>
                                <input type="text" name="title" class="form-control" id="" placeholder="Masukkan judul iklan" value="<?= set_value('title'); ?>">
                                <?php echo form_error('title', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="">Urutan (index)</label>
                                <input type="number" name="index" class="form-control" id="" placeholder="Masukkan urutan parallax" value="<?= set_value('index'); ?>" min="1" max="4" required>
                                <?php echo form_error('index', '<div class="error">', '</div>'); ?>
                                <p>*Hanya bisa menggunakan nomer urut 1 / 2 / 3/ 4</p>
                            </div>
                            <div class="form-group">
                                <label for="">Description</label>
                                <textarea name="description" class="form-control" placeholder="Masukkan deskripsi iklan"><?= set_value('description'); ?></textarea>
                                <?php echo form_error('description', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label>Start Date</label>
                                <input type="date" class="form-control" value="<?= set_value('start_date'); ?>" name="start_date" value="<?= set_value('start_date'); ?>" />
                                <?php echo form_error('finish_date', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label>Finish Date</label>
                                <div class="input-group">
                                    <input type="date" class="form-control" value="<?= set_value('finish_date'); ?>" name="finish_date" value="<?= set_value('finish_date'); ?>" />
                                </div>
                                <?php echo form_error('finish_date', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Img</label>
                                <div class="input-group">
                                    <input type="file" name="img" class="form-control" id="" required>
                                </div>
                                <p>*Max Size 2MB - Max Height 1344px & Max Width 2520px</p>
                            </div>
                            <div class="form-group">
                                <label for="">Link</label>
                                <textarea name="link" class="form-control" placeholder="Masukkan link iklan"><?= set_value('link'); ?></textarea>
                                <?php echo form_error('link', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="">Is Active ?</label>
                                <select name="is_active" id="" class="form-control">
                                    <option value="0">Disable</option>
                                    <option value="1">Active</option>
                                </select>
                            </div>
                            <!-- <?php
                                    if ($this->ion_auth->in_group($groups)) { ?>
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" name="is_admit" type="checkbox" id="customCheckbox1" value="1">
                                    <label for="customCheckbox1" class="custom-control-label">Approved ?</label>
                                </div>
                            </div>
                            <?php } ?> -->
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a href="<?= base_url('Parallax'); ?>" class="btn btn-danger">Kembali</a>
                        </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
</div>