<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><?= $pg_title; ?></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><?= $pg_menu; ?></li>
                        <li class="breadcrumb-item"><?= $pg_title; ?></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-8">
                    <!-- alert success -->
                    <?php if ($this->session->flashdata('flash')) { ?>
                        <?php if ($this->session->flashdata('flash') == 'add_success') { ?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <p class="text-center">Data berhasil ditambah!</p>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php if ($this->session->flashdata('img')) { ?>
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <p class="text-center"><?= "Gagal upload image" . $this->session->flashdata('img'); ?></p>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <?php } ?>
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Tambah <?= $pg_title; ?></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <?= form_open("Slider_Product/add_act", array('enctype' => 'multipart/form-data', 'id' => 'slider-product-form')); ?>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="">Urutan Gambar</label><br>
                                <?php foreach ($product_exists as $pe) : ?>
                                    <img src="<?= base_url($pe->img); ?>" alt="" height="50"> <?= $pe->index; ?> |
                                <?php endforeach; ?>
                            </div>
                            <div class="form-group">
                                <label for="">Urutan</label>
                                <input type="number" name="index" class="form-control" placeholder="Masukkan urutan gambar. *jangan gunakan urutan gambar yang sudah ada!" value="<?= set_value('index'); ?>">
                                <?= form_error('index', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="">Judul 1</label>
                                <input type="text" name="title1" class="form-control" id="" placeholder="Masukkan judul 1" value="<?= set_value('title1'); ?>">
                                <?php echo form_error('title1', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="">Judul 2</label>
                                <input type="text" name="title2" class="form-control" id="" placeholder="Masukkan judul 2" value="<?= set_value('title2'); ?>">
                                <?php echo form_error('title2', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="">Judul 3</label>
                                <input type="text" name="title3" class="form-control" id="" placeholder="Masukkan judul 2" value="<?= set_value('title3'); ?>">
                                <?php echo form_error('title3', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="">Judul 4</label>
                                <input type="text" name="title4" class="form-control" id="" placeholder="Masukkan judul 2" value="<?= set_value('title4'); ?>">
                                <?php echo form_error('title4', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="">Deskripsi</label>
                                <textarea name="description" id="" class="form-control" placeholder="Masukkan deskripsi"><?= set_value('description'); ?></textarea>
                                <?php echo form_error('description', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label>Start Date</label>
                                <input type="date" class="form-control" value="<?= set_value('start_date'); ?>" name="start_date" />
                                <?php echo form_error('finish_date', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label>Finish Date</label>
                                <div class="input-group">
                                    <input type="date" class="form-control" value="<?= set_value('finish_date'); ?>" name="finish_date" />
                                </div>
                                <?php echo form_error('finish_date', '<div class="error">', '</div>'); ?>
                            </div>
                            <!-- /.form group -->
                            <div class="form-group">
                                <label for="exampleInputFile">Gambar</label>
                                <div class="input-group">
                                    <input type="file" name="img" class="form-control" id="exampleInputFile" required>
                                </div>
                                <p>*Max Size 2MB - Max Height 1334px & Max Width 556px</p>
                                <div class="error"><?= @$message; ?></div>
                            </div>
                            <div class="form-group">
                                <label for="">Is Active ?</label>
                                <select name="is_active" id="" class="form-control">
                                    <option value="0">Disable</option>
                                    <option value="1">Active</option>
                                </select>
                            </div>
                            <!-- <?php if ($this->ion_auth->in_group($groups)) { ?>
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" name="is_admit" type="checkbox" id="customCheckbox1" value="1">
                                        <label for="customCheckbox1" class="custom-control-label">Approved ?</label>
                                    </div>
                                </div>
                            <?php } ?> -->
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a href="<?= base_url('Slider_Product'); ?>" class="btn btn-danger">Kembali</a>
                        </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
</div>