<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// hitung data slide
$slide_box = 0;
$slide_box_1 = $data_slide_to_box_1->sort;
$slide_box_2 = $slide_box_1 + $data_slide_to_box_2->sort;
$slide_box_3 = $slide_box_2 + $data_slide_to_box_3->sort;
$slide_box_4 = $slide_box_3 + $data_slide_to_box_4->sort;
$slide_box_5 = $slide_box_4 + $data_slide_to_box_5->sort;

// var_dump($slide_box_1);
// // var_dump($business_category_1->name);
// die;

?>

<section id="slider">
  <div id="main-slider" class="carousel slide" data-ride="carousel">
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <?php foreach ($slider_home as $row_slider_home){ ?>
        <div class="item" data-category="<?php echo $row_slider_home->business_category_id; ?>">
          <img src="<?php echo base_url().$row_slider_home->img; ?>" alt="slider" class="slider-image">
          <div class="container" style="position: relative;">
            <div class="slider-caption">
              <h1 class="slider-title"><?php echo $row_slider_home->title1; ?></h1>
              <?php if(!empty($row_slider_home->title2)){ ?>
                <h1 class="slider-title"><?php echo $row_slider_home->title2; ?></h1>
              <?php } ?>
              <?php if(!empty($row_slider_home->title3)){ ?>
                <h1 class="slider-title"><?php echo $row_slider_home->title3; ?></h1>
              <?php } ?>
              <?php if(!empty($row_slider_home->title4)){ ?>
                <h1 class="slider-title"><?php echo $row_slider_home->title4; ?></h1>
              <?php } ?>
              <p class="slider-description"><?php echo $row_slider_home->description; ?></p>
            </div>
          </div>        
        </div>
      <?php } ?>

      <div class="container">
        <div class="row selector" id="selector">
          <div class="col-xs-2 col-xs-offset-1">
            <div class="selector-box act">
              <a href="" style="text-decoration: none;" data-target="#main-slider" data-slide-to="<?= $slide_box; ?>" data-category="1">
                <img src="<?php echo base_url().$business_category_1->img; ?>" width="60%">
                <p class="selector-box-title"><?= $business_category_1->description; ?></p>
              </a>
              <a href="<?php echo base_url().$business_category_1->link; ?>" class="selector-box-link actv">Read More <i class="fa fa-arrow-right"></i></a>
            </div>
          </div>
          <div class="col-xs-2">
            <div class="selector-box">
              <a href="" style="text-decoration: none;" data-target="#main-slider" data-slide-to="<?= $slide_box_1; ?>" data-category="2">
                <img src="<?php echo base_url().$business_category_2->img; ?>" width="60%">
                <p class="selector-box-title"><?= $business_category_2->description; ?></p>
              </a>
              <a href="<?php echo base_url().$business_category_2->link; ?>" class="selector-box-link">Read More <i class="fa fa-arrow-right"></i></a>
            </div>
          </div>
          <div class="col-xs-2">
            <div class="selector-box">
              <a href="" style="text-decoration: none;" data-target="#main-slider" data-slide-to="<?= $slide_box_2; ?>" data-category="3">
                <img src="<?php echo base_url().$business_category_3->img; ?>" width="60%">
                <p class="selector-box-title"><?= $business_category_3->description; ?></p>
              </a>
              <a href="<?php echo base_url().$business_category_3->link; ?>" class="selector-box-link">Read More <i class="fa fa-arrow-right"></i></a>
            </div>
          </div>
          <div class="col-xs-2">
            <div class="selector-box">
              <a href="" style="text-decoration: none;" data-target="#main-slider" data-slide-to="<?= $slide_box_3; ?>" data-category="4">
                <img src="<?php echo base_url().$business_category_4->img; ?>" width="60%">
                <p class="selector-box-title"><?= $business_category_4->description; ?></p>
              </a>
              <a href="<?php echo base_url().$business_category_4->link; ?>" class="selector-box-link">Read More <i class="fa fa-arrow-right"></i></a>
            </div>
          </div>
          <div class="col-xs-2">
            <div class="selector-box">
              <a href="" style="text-decoration: none;" data-target="#main-slider" data-slide-to="<?= $slide_box_4; ?>" data-category="5">
                <img src="<?php echo base_url().$business_category_5->img; ?>" width="60%">
                <p class="selector-box-title"><?= $business_category_5->description; ?></p>
              </a>
              <a href="<?php echo base_url().$business_category_5->link; ?>" class="selector-box-link">Read More <i class="fa fa-arrow-right"></i></a>
            </div>
          </div>
          <!-- <div class="col-xs-2">
            <div class="selector-box">
              <a href="" style="text-decoration: none;" data-target="#main-slider" data-slide-to="9" data-category="10">
                <img src="<?php echo base_url(); ?>assets/images/slide/Home-25.png" width="60%">
                <p class="selector-box-title">Test</p>
              </a>
              <a href="<?php echo base_url(); ?>our_business" class="selector-box-link">Read More <i class="fa fa-arrow-right"></i></a>
            </div>
          </div> -->

          <!-- Controllers -->
          <a class="left carousel-control" href="#main-slider" data-slide="prev">
              <span><i class="fa fa-angle-left"></i></span>
          </a>
          <a class="right carousel-control" href="#main-slider" data-slide="next">
              <span><i class="fa fa-angle-right"></i></span>
          </a>
        </div>
      </div>

    </div>
  </div>
</section>

<script type="text/javascript">
  $('#main-slider').on('slid.bs.carousel', function (e) {
    var ele = $('#main-slider .active');

    //console.log(ele[0].getAttribute('data-category'))
    var header = document.getElementById("selector");
    var btns = header.getElementsByClassName("selector-box");

    for (var i = 0; i < btns.length; i++) {
      //console.log(btns[i].firstChild.dataset['slideTo'])
      //console.log(ele.index('#main-slider .item'))
      //console.log(btns[i].firstElementChild.getAttribute('data-category'))

      if(ele[0].getAttribute('data-category') === btns[i].firstElementChild.getAttribute('data-category')) {
        //btns[i].className += " act";
        // console.log(btns[i].lastElementChild.className)
        
        if(btns[i].lastElementChild.className == "selector-box-link"){
          btns[i].lastElementChild.className = btns[i].lastElementChild.className.replace("selector-box-link", "selector-box-link actv");
        } else {
          btns[i].lastElementChild.className = btns[i].lastElementChild.className.replace("selector-box-link actv", "selector-box-link actv");
        }

        if(btns[i].className == "selector-box"){
          btns[i].className = btns[i].className.replace("selector-box", "selector-box act");
        } else {
          btns[i].className = btns[i].className.replace("selector-box act", "selector-box act");
        }
      } else {

        if(btns[i].lastElementChild.className == "selector-box-link actv"){
          btns[i].lastElementChild.className = btns[i].lastElementChild.className.replace("selector-box-link actv", "selector-box-link");
        } else {
          btns[i].lastElementChild.className = btns[i].lastElementChild.className.replace("actv", "");
        }

        if(btns[i].className == "selector-box act"){
          btns[i].className = btns[i].className.replace("selector-box act", "selector-box");
        } else {
          btns[i].className = btns[i].className.replace("act", "");
        }
        //document.getElementsByClassName("selector-box")[0].className.replace("selector-box act act", "selector-box");
        ///btns[i].className = btns[i].className.replace(" act", "")
      }

      btns[i].addEventListener("click", function() {
      var current = document.getElementsByClassName("act");
      current[0].className = current[0].className.replace(" act", "");
      this.className += " act";
      });
    }
  })
</script>

<section id="slider-product">
  <div id="product-slider" class="carousel slide" data-ride="carousel">   

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <?php foreach ($slider_product as $key => $row_slider_product){ ?>
        <div class="item" data-category="<?php echo $row_slider_product->business_category_id; ?>">
          <img src="<?php echo base_url().$row_slider_product->img; ?>" alt="slider" class="slider-image">
          <div class="container" style="position: relative;">
            <div class="slider-caption">
              <h1 class="slider-title"><?php echo $row_slider_product->title1; ?></h1>
              <?php if(!empty($row_slider_product->title2)){ ?>
                <h1 class="slider-title"><?php echo $row_slider_product->title2; ?></h1>
              <?php } ?>
              <?php if(!empty($row_slider_product->title3)){ ?>
                <h1 class="slider-title"><?php echo $row_slider_product->title3; ?></h1>
              <?php } ?>
              <?php if(!empty($row_slider_product->title4)){ ?>
                <h1 class="slider-title"><?php echo $row_slider_product->title4; ?></h1>
              <?php } ?>
              <p class="slider-description"><?php echo $row_slider_product->description; ?></p>
            </div>
          </div>        
        </div>
      <?php } ?>

      <div class="container">
        <div class="row pull-right sticky-cart">
          <div class="sticky-cart-content">
            <a href="http://glomart.id" class="hvr-pulse">
              <img class="sticky-cart-img" src="<?php echo base_url(); ?>assets/images/slide/logo keranjang-08.png" width="100">
            </a>
          </div>            
        </div>        
      </div>
    </div>

    <div class="container" style="position: relative;">
      <div class="row">
          <!-- Indicators -->
          <ol class="carousel-indicators">
          <?php foreach ($slider_product as $key => $row_slider_product) {?>
            <li data-target="#product-slider" data-slide-to="<?= $key; ?>" <?php if($key == 0) { ?> class="active" <?php } ?>></li>
          <?php } ?>
          </ol><!--/ Indicators end-->
      </div>
    </div>

  </div>
</section>

<section id="home-aboutus" class="home-aboutus">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-6">
        <div class="home-top-box">
          <?php if(!empty($about_us)) { ?>
          <h2><?= $about_us->title1; ?></h2>
          <?php if(!empty($about_us->title2)){ ?>
            <h2><?= $about_us->title2; ?></h2>
          <?php } ?>
          <?php if(!empty($about_us->title3)){ ?>
            <h2><?= $about_us->title3; ?></h2>
          <?php } ?>
          <?php if(!empty($about_us->title4)){ ?>
            <h2><?= $about_us->title4; ?></h2>
          <?php } ?>
          <p align="justify"><?= $about_us->description; ?></p>
          <!-- <p align="justify">PUFR GROUP is an integrated food manufacturing, distribution, and catering company operating in 33 Indonesian provinces since 1975. Its operations are carried out by the catering company (PSU), meat manufacturer (DDFI), food manufacturer (PUFI), supply chain (PUFD), pastry & coffee (PUPAT), vegetable processing (PUMS), security services (DPL), and trader (NBSU).</p>
          <p align="justify">The integrated approach enables PUFR to devise cost-efficient, seamless solutions for all customers’ support needs under one roof, freeing clients to focus on their core business. Pangansari Utama Group regularly supplies to multinationals and well-known domestic companies, including handling the needs of remote oil and gas operations.</p> -->
          <?php } else { ?>
            <h2>About Us</h2>
            <p align="justify">About Us belum ditambahkan!</p>
         <?php } ?>
          <br>
          <p>
            <a href="<?= base_url('pangansari_group'); ?>" class="slider btn btn-primary">Read More</a>
          </p>
        </div>
    </div>
  </div>
</section>

<section id="home-news" class="home-news">
  <div class="container">
    <div class="row">
      <div class="col-sm-4 col-md-4">
        <div class="home-top-box">
          <h2>Pangansari News</h2>
        </div>
      </div>
      <div class="col-sm-8 col-md-8">      
        <div class="home-top-box text-right">
          <ul>
            <?php foreach ($news_category as $key => $news_category){ ?>
              <li><a href="<?php echo base_url().$news_category->link; ?>#news-list" class="hvr-shrink"><img src="<?php echo base_url().$news_category->img_active; ?>" width="50%"></a></li>
            <?php } ?>
          </ul>
        </div>
      </div> 
    </div>
    <div class="row">
      <?php foreach ($latest_news as $key => $row_latest_news) { ?>
        <div class="col-md-3 col-sm-6">
          <div class="home-top-box2">
            <div class="section-1-box">
              <div class="img-preview" >
                <a href="<?php echo base_url()."news/detail/".$row_latest_news->link_post."?cat=".$row_latest_news->news_category_name; ?>#news-list">
                  <img src="<?php echo base_url().$row_latest_news->img; ?>">
                </a>
              </div>
              <a href="<?php echo base_url()."news/detail/".$row_latest_news->link_post."?cat=".$row_latest_news->news_category_name; ?>#news-list"><h5 class="text-center"><?php echo ucwords($row_latest_news->title); ?></h5></a>
            </div>
          </div>
        </div>
      <?php } ?>
    </div>
  </div>
</section>

<section id="home-certificate" class="home-certificate">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="home-top-box">
        <a href="<?php echo base_url(); ?>auth/login"><h2 align="center">Certificates</h2></a>
          <div class="home-top-box" align="center">
            <img align="center" src="<?php echo base_url(); ?>assets/images/Home-35.png" width="80%">
          </div>
        </div>
    </div>
  </div>
</section>