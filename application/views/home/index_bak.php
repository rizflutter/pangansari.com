<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section id="slider">
  <!-- Carousel -->
  <div id="main-slide" class="carousel slide" data-ride="carousel">

    <!-- Carousel inner -->
    <div class="carousel-inner">

    <?php foreach ($slider_home as $row_slider_home){ ?>
      <div class="item" data-category="<?php echo $row_slider_home->business_category_id; ?>" style="background-image:url(<?php echo base_url().$row_slider_home->img; ?>)">
          <div class="container">
            <div class="slider-title">
              <div class="row">
                  <div class="col-md-6">
                    <div class="slider-top-box">
                      <h3 class="slide-sub-title"><?php echo $row_slider_home->title1; ?></h3>
                      <?php if(!empty($row_slider_home->title2)){ ?>
                        <h3 class="slide-sub-title"><?php echo $row_slider_home->title2; ?></h3>
                      <?php } ?>
                      <?php if(!empty($row_slider_home->title3)){ ?>
                        <h3 class="slide-sub-title"><?php echo $row_slider_home->title3; ?></h3>
                      <?php } ?>
                      <?php if(!empty($row_slider_home->title4)){ ?>
                        <h3 class="slide-sub-title"><?php echo $row_slider_home->title4; ?></h3>
                      <?php } ?>
                      <p class="slider-description lead"><?php echo $row_slider_home->description; ?></p>
                      <!-- <p class="effect3" style="margin:0px;">
                        <a href="#" class="slider btn btn-primary">Read More</a>
                      </p> --> 
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div><!--/ Carousel item end -->
    <?php } ?>

      <div class="container">
        <div class="row">
          <div class="slider-bottom-box" id="selector">
            <div style="position: absolute; top: 80%; width: 1140px; left: 53%; margin-right: -50%; transform: translate(-50%, -50%);">
              <a href="" style="text-decoration: none;" data-target="#main-slide" data-slide-to="0" data-category="1">
                <div class="col-xs-2">
                  <div class="slider-box act">
                    <img src="<?php echo base_url(); ?>assets/images/slide/Home-21.png" style="width: 100px;">
                    <p>Industrial Catering & Related Services</p>
                    <a href="<?php echo base_url(); ?>our_business" class="slider-box-link actv">Read More <i class="fa fa-arrow-right"></i></a>
                  </div>
                </div>
              </a>
              <a href="" style="text-decoration: none;" data-target="#main-slide" data-slide-to="2" data-category="2">
                <div class="col-xs-2 col-half-offset">
                  <div class="slider-box">
                    <img src="<?php echo base_url(); ?>assets/images/slide/Home-22.png" style="width: 100px;">
                    <p>Food Productions</p>
                    <a href="<?php echo base_url(); ?>our_business" class="slider-box-link">Read More <i class="fa fa-arrow-right"></i></a>
                  </div>
                </div>
              </a>
              <a href="" style="text-decoration: none;" data-target="#main-slide" data-slide-to="5" data-category="3">
                <div class="col-xs-2 col-half-offset">
                  <div class="slider-box">
                    <img src="<?php echo base_url(); ?>assets/images/slide/Home-23.png" style="width: 100px;">
                    <p>Retails</p>
                    <a href="<?php echo base_url(); ?>our_business" class="slider-box-link">Read More <i class="fa fa-arrow-right"></i></a>
                  </div>
                </div>
              </a>
              <a href="" style="text-decoration: none;" data-target="#main-slide" data-slide-to="7" data-category="4">
                <div class="col-xs-2 col-half-offset">
                  <div class="slider-box">
                    <img src="<?php echo base_url(); ?>assets/images/slide/Home-24.png" style="width: 100px;">
                    <p>Trading</p>
                    <a href="<?php echo base_url(); ?>our_business" class="slider-box-link">Read More <i class="fa fa-arrow-right"></i></a>
                  </div>
                </div>
              </a>
              <a href="" style="text-decoration: none;" data-target="#main-slide" data-slide-to="8" data-category="5">
                <div class="col-xs-2 col-half-offset">
                  <div class="slider-box">
                    <img src="<?php echo base_url(); ?>assets/images/slide/Home-25.png" style="width: 100px;">
                    <p>Security Services & Facility Management</p>
                    <a href="<?php echo base_url(); ?>our_business" class="slider-box-link">Read More <i class="fa fa-arrow-right"></i></a>
                  </div>
                </div>
              </a>
              
              <!-- Controllers -->
              <a class="left carousel-control" style="top: -35%; left: -7%;" href="#main-slide" data-slide="prev">
                  <span><i class="fa fa-angle-left"></i></span>
              </a>
              <a class="right carousel-control" style="top: -35%" href="#main-slide" data-slide="next">
                  <span><i class="fa fa-angle-right"></i></span>
              </a>

            </div>
          </div>
        </div>
      </div>
       
    </div><!-- Carousel inner end-->

  </div><!--/ Carousel end -->
</section>

<script>

$('#main-slide').on('slid.bs.carousel', function (e) {
var ele = $('#main-slide .active');

//console.log(ele[0].getAttribute('data-category'))

var header = document.getElementById("selector");
var btns = header.getElementsByClassName("slider-box");

for (var i = 0; i < btns.length; i++) {
  //console.log(btns[i].firstChild.dataset['slideTo'])
  //console.log(ele.index('#main-slide .item'))
  //console.log(btns[i].firstElementChild.getAttribute('data-category'))

  if(ele[0].getAttribute('data-category') === btns[i].firstElementChild.getAttribute('data-category')) {
    //btns[i].className += " act";

    //console.log(btns[i].lastElementChild.className)
    if(btns[i].lastElementChild.className == "slider-box-link"){
      btns[i].lastElementChild.className = btns[i].lastElementChild.className.replace("slider-box-link", "slider-box-link actv");
    } else {
      btns[i].lastElementChild.className = btns[i].lastElementChild.className.replace("slider-box-link actv", "slider-box-link actv");
    }

    if(btns[i].className == "slider-box"){
      btns[i].className = btns[i].className.replace("slider-box", "slider-box act");
    } else {
      btns[i].className = btns[i].className.replace("slider-box act", "slider-box act");
    }
  } else {

    if(btns[i].lastElementChild.className == "slider-box-link actv"){
      btns[i].lastElementChild.className = btns[i].lastElementChild.className.replace("slider-box-link actv", "slider-box-link");
    } else {
      btns[i].lastElementChild.className = btns[i].lastElementChild.className.replace("actv", "");
    }

    if(btns[i].className == "slider-box act"){
      btns[i].className = btns[i].className.replace("slider-box act", "slider-box");
    } else {
      btns[i].className = btns[i].className.replace("act", "");
    }
    //document.getElementsByClassName("slider-box")[0].className.replace("slider-box act act", "slider-box");
    ///btns[i].className = btns[i].className.replace(" act", "")
  }

  btns[i].addEventListener("click", function() {
  var current = document.getElementsByClassName("act");
  current[0].className = current[0].className.replace(" act", "");
  this.className += " act";
  });
}
})

</script>

<section id="slider">
  <!-- Carousel -->
  <div id="main-slide2" class="carousel slide" data-ride="carousel">
    <div class="container">
      <div class="row">
          <!-- Indicators -->
          <ol class="carousel-indicators visible-lg visible-md">
          <?php foreach ($slider_product as $key => $row_slider_product) { ?>
            <li data-target="#main-slide2" data-slide-to="<?php echo $row_slider_product->index; ?>" <?php if($key == 0) { ?> class="active" <?php } ?>></li>
          <?php } ?>
          </ol><!--/ Indicators end-->
      </div>
    </div>

    <!-- Carousel inner -->
    <div class="carousel-inner">
    <?php foreach ($slider_product as $key => $row_slider_product) { ?>
      <div class="item <?php if($key == 0) { ?> active <?php } ?>" style="background-image:url(<?php echo base_url().$row_slider_product->img; ?>)">
        <div class="container">
          <div class="slider-title">
            <div class="row">
                <div class="col-md-6">
                  <div class="slider-middle-box">
                    <h3 class="slide-sub-title2"><?php echo $row_slider_product->title1; ?></h3>
                    <?php if(!empty($row_slider_product->title2)){ ?>
                      <h3 class="slide-sub-title2"><?php echo $row_slider_product->title2; ?></h3>
                    <?php } ?>
                    <?php if(!empty($row_slider_product->title3)){ ?>
                      <h3 class="slide-sub-title2"><?php echo $row_slider_product->title3; ?></h3>
                    <?php } ?>
                    <?php if(!empty($row_slider_product->title4)){ ?>
                      <h3 class="slide-sub-title2"><?php echo $row_slider_product->title4; ?></h3>
                    <?php } ?>
                      <p class="slider-description lead"><?php echo $row_slider_product->description; ?></p>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div><!--/ Carousel item end -->
    <?php } ?>

      <div class="container">
        <div class="row pull-right sticky-cart">
          <div class="sticky-cart-content">
            <a href="http://glomart.id" class="hvr-pulse">
              <img class="sticky-cart-img" src="<?php echo base_url(); ?>assets/images/slide/logo keranjang-08.png" width="100">
            </a>
          </div>            
        </div>        
      </div>   
      
    </div><!-- Carousel inner end-->

  </div><!--/ Carousel end -->
</section>

<section id="home-aboutus" class="home-aboutus">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-6">
        <div class="home-top-box">
          <h2>About Us</h2>
          <p align="justify">PUFR GROUP is an integrated food manufacturing, distribution, and catering company operating in 33 Indonesian provinces since 1975. Its operations are carried out by the catering company (PSU), meat manufacturer (DDFI), food manufacturer (PUFI), supply chain (PUFD), pastry & coffee (PUPAT), vegetable processing (PUMS), security services (DPL), and trader (NBSU).</p>
          <p align="justify">The integrated approach enables PUFR to devise cost-efficient, seamless solutions for all customers’ support needs under one roof, freeing clients to focus on their core business. Pangansari Utama Group regularly supplies to multinationals and well-known domestic companies, including handling the needs of remote oil and gas operations.</p>
          <br>
          <p>
            <a href="#" class="slider btn btn-primary">Read More</a>
          </p>
        </div>
    </div>
  </div>
</section>

<section id="home-news" class="home-news">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-md-6">
        <div class="home-top-box">
          <h2>Pangansari News</h2>
        </div>
      </div>
      <div class="col-sm-6 col-md-6">      
        <div class="home-top-box text-right">
          <ul>
            <li class="text-right"><a href="<?php echo base_url(); ?>news?cat=streaming" class="hvr-shrink"><img src="<?php echo base_url(); ?>assets/images/news/Home-34.png" width="60%"></a></li>
            <li class="text-right"><a href="<?php echo base_url(); ?>news?cat=recipe" class="hvr-shrink"><img src="<?php echo base_url(); ?>assets/images/news/Home-31.png" width="60%"></a></li>
            <li class="text-right"><a href="<?php echo base_url(); ?>news?cat=healthy" class="hvr-shrink"><img src="<?php echo base_url(); ?>assets/images/news/Home-32.png" width="60%"></a></li>
            <li class="text-right"><a href="<?php echo base_url(); ?>news?cat=featured" class="hvr-shrink"><img src="<?php echo base_url(); ?>assets/images/news/icon_features-31.png" width="60%"></a></li>
            <li class="text-right"><a href="<?php echo base_url(); ?>news" class="hvr-shrink"><img src="<?php echo base_url(); ?>assets/images/news/Home-33.png" width="60%"></a></li>
          </ul>
        </div>
      </div> 
    </div>
    <div class="row">
      <?php foreach ($latest_news as $key => $row_latest_news) { ?>
        <div class="col-md-3 col-sm-6">
          <div class="home-top-box2">
            <div class="section-1-box">
              <div class="img-preview" >
                <a href="<?php echo base_url()."news/detail/".$row_latest_news->link_post."?cat=".$row_latest_news->news_category_name; ?>">
                  <img src="<?php echo base_url().$row_latest_news->img; ?>">
                </a>
              </div>
              <a href="<?php echo base_url()."news/detail/".$row_latest_news->link_post."?cat=".$row_latest_news->news_category_name; ?>"><h5 class="text-center"><?php echo ucwords($row_latest_news->title); ?></h5></a>
            </div>
          </div>
        </div>
      <?php } ?>
    </div>
  </div>
</section>

<section id="home-certificate" class="home-certificate">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="home-top-box">
          <h2 align="center">Certificates</h2>
          <div class="home-top-box" align="center">
            <img align="center" src="<?php echo base_url(); ?>assets/images/Home-35.png" width="80%">
          </div>
        </div>
    </div>
  </div>
</section>