<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php echo $script_captcha; ?>

<div class="header-bottom">
  <img src="<?php echo base_url(); ?>assets/images/contact/Contact-33.png" width="100%">
</div>

<section id="contact" class="contact">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-7">
        <div class="contact-top-box">
          <h4>Contact Us</h4>
          <form action="<?php echo base_url(); ?>contact/send_message" id="contact" role="form" method="POST">
            <!-- unsuccessfully -->
            <p class="error alert alert-danger"><i class="fa fa-times"></i> E-mail must be valid and message must be longer than 1 character. </p>
            <div class="row">
              <div class="col-md-6">
                <div class="control-group form-group">
                  <div class="controls">
                    <label class="form-label">Your name*</label>
                    <input class="form-control" type="text" name="cf-name">
                    <p class="help-block"></p>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="control-group form-group">
                  <div class="controls">
                    <label class="form-label">Your email address*</label>
                    <input class="form-control" type="text" name="cf-email">
                    <p class="help-block"></p>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="control-group form-group">
                  <div class="controls">
                    <label class="form-label">Subject</label>
                    <input class="form-control" type="text" name="cf-subject">
                    <p class="help-block"></p>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="control-group form-group">
                  <div class="controls">
                    <label class="form-label">Message*</label>
                    <textarea class="form-control custom-control" id="cf-message" rows="12" name="cf-message"></textarea>
                    <p class="help-block"></p>
                  </div>
                </div>
              </div>
            </div>       
            <?php echo $captcha; ?><br>
            <!-- For success/fail messages -->
            <button type="submit" id="cf-submit" class="btn btn-primary">Send Message</button>
          </form>
        </div>
      </div>
      <div class="col-xs-12 col-md-1"></div>
      <div class="col-xs-12 col-md-4">
        <div class="contact-top-box">
          <h4>Head Office</h4>
          <p>Jl. Raya Poncol No. 24 Ciracas <br> Jakarta 13740 - Indonesia</p>
          <p><img src="<?php echo base_url(); ?>assets/images/contact/Contact-35.png" width="25"> +62 21 871 7870</p>
          <p><img src="<?php echo base_url(); ?>assets/images/contact/Contact-36.png" width="25"> +62 21 871 7869</p>
          <br>
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.519271548892!2d106.86621361384127!3d-6.326687263665317!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69ed8f6c07832b%3A0x456f620f2cad5062!2sPT%20Pangansari%20Utama%20Food%20Resources%20(PUFR)!5e0!3m2!1sid!2sid!4v1612776874922!5m2!1sid!2sid" width="800" height="370" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
      </div>
    </div>
  </div>
</section>