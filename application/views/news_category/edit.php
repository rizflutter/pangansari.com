<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><?= $pg_title; ?></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><?= $pg_menu; ?></li>
                        <li class="breadcrumb-item"><?= $pg_title; ?></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-8">
                    <!-- alert success -->
                    <?php if ($this->session->flashdata('flash')) { ?>
                        <?php if ($this->session->flashdata('flash') == 'edit_success') { ?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <p class="text-center">Data berhasil diubah!</p>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <!-- alert error upload -->
                    <?php if ($this->session->flashdata('file_active_result')) { ?>
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <p class="text-center"><?= "Gagal upload image active" . $this->session->flashdata('file_active_result'); ?></p>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <?php } ?>
                    <?php if ($this->session->flashdata('file_nonactive_result')) { ?>
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <p class="text-center"><?= "Gagal upload image non active" . $this->session->flashdata('file_nonactive_result') ?></p>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <?php } ?>
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Edit <?= $pg_title; ?></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <?php echo form_open("News_Category/edit_act", array('enctype' => 'multipart/form-data', 'id' => 'news-category-form')); ?>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="">Urutan Icon</label><br>
                                <?php foreach ($sort as $s) : ?>
                                    <img src="<?= base_url($s->img_active); ?>" alt="" height="50"> <?= $s->sort; ?> |
                                <?php endforeach; ?>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="id" class="form-control" value="<?= $news_category_row->id; ?>">
                                <label for="">Sort</label>
                                <input type="number" name="sort" class="form-control" placeholder="Urutan lama adalah : <?= $news_category_row->sort; ?>, Biarkan kosong jika tidak ingin mengubah urutan" value="<?= set_value('sort'); ?>">
                                <?php echo form_error('sort', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="">Name</label>
                                <input type="text" name="name" class="form-control" id="" placeholder="Name lama adalah : <?= $news_category_row->name; ?>, Biarkan kosong jika tidak ingin mengubah name" value="<?= set_value('name'); ?>">
                                <?php echo form_error('name', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="">Description</label>
                                <input type="text" name="description" class="form-control" id="" placeholder="Masukkan description" value="<?= $news_category_row->description; ?>">
                                <?php echo form_error('description', '<div class="error">', '</div>'); ?>
                            </div>
                            <!-- /.form group -->
                            <img src="<?= base_url($news_category_row->img_active); ?>" alt="" height="100">
                            <div class="form-group">
                                <label for="exampleInputFile">Img Active</label>
                                <div class="input-group">
                                    <input type="file" name="img_active" class="form-control" id="">
                                </div>
                                <p>*Max Size 100KB - Max Height 88px & Max Width 88px</p>
                            </div>
                            <img src="<?= base_url($news_category_row->img_nonactive); ?>" alt="" height="100">
                            <div class="form-group">
                                <label for="exampleInputFile">Img Nonactive</label>
                                <div class="input-group">
                                    <input type="file" name="img_nonactive" class="form-control" id="">
                                </div>
                                <p>*Max Size 100KB - Max Height 88px & Max Width 88px</p>
                            </div>
                            <div class="form-group">
                                <label for="">Is Active ?</label>
                                <select name="is_active" id="" class="form-control">
                                    <option value="0" <?php if ($news_category_row->is_active == 1) {
                                                            echo 'selected';
                                                        }; ?>>Disable</option>
                                    <option value="1" <?php if ($news_category_row->is_active == 1) {
                                                            echo 'selected';
                                                        }; ?>>Active</option>
                                </select>
                            </div>
                            <!-- <?php
                            if ($this->ion_auth->in_group($groups)) { ?>
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" name="is_admit" type="checkbox" id="customCheckbox1" value="1" <?php if ($news_category_row->is_admit == 1) {
                                                                                                                                                    echo 'checked';
                                                                                                                                                } ?>>
                                        <label for="customCheckbox1" class="custom-control-label">Approved ?</label>
                                    </div>
                                </div>
                            <?php } ?> -->
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a href="<?= base_url('News_Category'); ?>" class="btn btn-danger">Kembali</a>
                        </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
</div>