<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><?= $pg_title; ?></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><?= $pg_menu; ?></li>
                        <li class="breadcrumb-item"><?= $pg_title; ?></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-8">
                    <!-- alert success -->
                    <?php if ($this->session->flashdata('flash')) { ?>
                        <?php if ($this->session->flashdata('flash') == 'edit_success') { ?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <p class="text-center">Data berhasil diubah!</p>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php if ($this->session->flashdata('document_file')) { ?>
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <p class="text-center"><?= "Gagal upload image" . $this->session->flashdata('document_file'); ?></p>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <?php } ?>
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Edit <?= $pg_title; ?></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <?= form_open("Company_Profile/edit_act", array('enctype' => 'multipart/form-data', 'id' => 'company-profile-form')); ?>
                        <div class="card-body">
                            <input type="hidden" name="id" value="<?= $company_profile_row->id; ?>">
                            <div class="form-group">
                                <label for="">Nama Dokumen</label>
                                <input type="text" name="document_name" class="form-control" id="" placeholder="Masukkan nama dokumen" value="<?= $company_profile_row->document_name; ?>">
                                <?php echo form_error('title1', '<div class="error">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Nama File Lama</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" value="<?= substr($company_profile_row->document_file, 23); ?>"" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">File Dokumen</label>
                                <div class="input-group">
                                    <input type="file" name="document_file" class="form-control" id="exampleInputFile">
                                </div>
                                <p>*Max Size 100MB - Harus dalam bentuk PDF</p>
                                <div class="error"><?= @$message; ?></div>
                            </div>
                            <div class="form-group">
                                <label for="">Is Active ?</label>
                                <select name="is_active" id="" class="form-control">
                                    <option value="0" <?php if ($company_profile_row->is_active == 1) {
                                                            echo 'selected';
                                                        }; ?>>Disable</option>
                                    <option value="1" <?php if ($company_profile_row->is_active == 1) {
                                                            echo 'selected';
                                                        }; ?>>Active</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" name="is_posting" type="checkbox" id="customCheckbox2" value="1" <?php if ($company_profile_row->is_posting == 1) { echo 'checked'; } ?> onclick="return confirm('Yakin ingin mengganti company profile ?');">
                                    <label for="customCheckbox2" class="custom-control-label">Posting ?</label>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a href="<?= base_url('Company_Profile'); ?>" class="btn btn-danger">Kembali</a>
                        </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
</div>