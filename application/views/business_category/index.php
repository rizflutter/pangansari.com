<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><?= $pg_title; ?></h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><?= $pg_menu; ?></li>
            <li class="breadcrumb-item"><?= $pg_title; ?></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">DataTable <?= $pg_title; ?></h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <!-- <button class="btn btn-success" onclick="add_bc()">Tambah</button> -->
              <!-- <button class="btn btn-default" onclick="reload_table()">Reload</button> -->
              <div class="table-responsive">
              <table id="example1" class="table table-dark table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Urutan</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Link</th>
                    <th>IMG</th>
                    <th>Created</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
              </div>
            </div>
          </div>
        </div>
        <!-- /.col-md-12 -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Bootstrap modal -->
<div class="modal fade" class="modal-default" id="modal-business-category" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Business Category</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body form">
        <form action="#" id="business-category-form">
          <input type="hidden" value="" name="id"/>
          <div class="form-body">
            <div class="form-group">
              <label class="control-label">Name</label>
              <input name="name" placeholder="Masukkan nama business category..." class="form-control" type="text">
              <span class="help-block"></span>
            </div>
            <div class="form-group">
              <label class="control-label">Description</label>
              <textarea name="description" id="" class="form-control" placeholder="Masukkan deskripsi"></textarea>
              <span class="help-block"></span>
            </div>
            <div class="form-group">
              <label class="control-label">Link</label>
              <input name="link" placeholder="Masukkan segment terakhir link business category..." class="form-control" type="text">
              <span class="help-block"></span>
            </div>
            <div class="form-group">
              <label class="control-label">IMG</label>
              <input type="file" name="img" id="" class="form-control">
              <span class="help-block"></span>
              <p>*Max Size 200KB - Max Height 175px & Max Width 555px</p>
              <div id="img-error" class="error"></div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary" onclick="save()">Submit</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->