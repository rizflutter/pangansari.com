<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_News extends CI_Model {

	function news_category($sort){
		return $query = $this->db->query("SELECT * FROM news_category WHERE is_active = 1 AND is_admit = 1 $sort");
	}

	function latest_news($cat){
		return $query = $this->db->query("
			SELECT news.*, news_category.name AS news_category_name, news_category.description AS news_category_description,
			news_category.link AS news_category_link FROM news
			JOIN news_category ON news_category.id = news.news_category_id
			WHERE news.is_active = 1 AND news.is_admit = 1 AND start_date <= NOW() AND finish_date >= NOW() AND news_category.name = '".$cat."' ORDER BY news.id DESC LIMIT 10");
	}

	function news_archive($cat){
		return $query = $this->db->query("SELECT year(news.created_at) AS year, news_category.name AS news_category_name
			FROM news
			JOIN news_category ON news_category.id = news.news_category_id
			WHERE news.is_active = 1 AND news.is_admit = 1 AND start_date <= NOW() AND finish_date >= NOW() AND news_category.name = '".$cat."'
			GROUP BY YEAR(news.created_at)");
	}

	function news($cat, $from, $per_page, $s = FALSE){
		if($s == TRUE){
			return $query = $this->db->query("SELECT news.*, news_category.name AS news_category_name, news_category.description AS news_category_description, news_category.link AS news_category_link FROM news JOIN news_category ON news_category.id = news.news_category_id WHERE news.is_active = 1 AND news.is_admit = 1 AND start_date <= NOW() AND finish_date >= NOW() AND news_category.name = '".$cat."' AND (news.title LIKE '%$s%' OR news.description LIKE '%$s%') ORDER BY news.created_at DESC LIMIT $from, $per_page");
		} else {
			return $query = $this->db->query("SELECT news.*, news_category.name AS news_category_name, news_category.description AS news_category_description, news_category.link AS news_category_link FROM news JOIN news_category ON news_category.id = news.news_category_id WHERE news.is_active = 1 AND news.is_admit = 1 AND start_date <= NOW() AND finish_date >= NOW() AND news_category.name = '".$cat."' ORDER BY news.created_at DESC LIMIT $from, $per_page");
		}
	}

	function news_all($cat, $s = FALSE){
		if($s == TRUE){
			return $query = $this->db->query("SELECT news.*, news_category.name AS news_category_name, news_category.description AS news_category_description, news_category.link AS news_category_link FROM news JOIN news_category ON news_category.id = news.news_category_id WHERE news.is_active = 1 AND news.is_admit = 1 AND start_date <= NOW() AND finish_date >= NOW() AND news_category.name = '".$cat."' AND (news.title LIKE '%$s%' OR news.description LIKE '%$s%') ORDER BY news.created_at DESC");
		} else {
			return $query = $this->db->query("SELECT news.*, news_category.name AS news_category_name, news_category.description AS news_category_description, news_category.link AS news_category_link FROM news JOIN news_category ON news_category.id = news.news_category_id WHERE news.is_active = 1 AND news.is_admit = 1 AND start_date <= NOW() AND finish_date >= NOW() AND news_category.name = '".$cat."' ORDER BY news.created_at DESC");
		}
	}

	function news_archives($cat, $year, $from, $per_page, $s = FALSE){
		if($s == TRUE){
			return $query = $this->db->query("SELECT news.*, news_category.name AS news_category_name, news_category.description AS news_category_description, news_category.link AS news_category_link FROM news JOIN news_category ON news_category.id = news.news_category_id WHERE news.is_active = 1 AND news.is_admit = 1 AND start_date <= NOW() AND finish_date >= NOW() AND year(news.created_at) = $year AND news_category.name = '".$cat."' AND (news.title LIKE '%$s%' OR news.description LIKE '%$s%') ORDER BY news.created_at DESC LIMIT $from, $per_page");
		} else {
			return $query = $this->db->query("SELECT news.*, news_category.name AS news_category_name, news_category.description AS news_category_description, news_category.link AS news_category_link FROM news JOIN news_category ON news_category.id = news.news_category_id WHERE news.is_active = 1 AND news.is_admit = 1 AND start_date <= NOW() AND finish_date >= NOW() AND year(news.created_at) = $year AND news_category.name = '".$cat."' ORDER BY news.created_at DESC LIMIT $from, $per_page");
		}
	}

	function news_archives_all($cat, $year, $s = FALSE){
		if($s == TRUE){
			return $query = $this->db->query("SELECT news.*, news_category.name AS news_category_name, news_category.description AS news_category_description, news_category.link AS news_category_link FROM news JOIN news_category ON news_category.id = news.news_category_id WHERE news.is_active = 1 AND news.is_admit = 1 AND start_date <= NOW() AND finish_date >= NOW() AND year(news.created_at) = $year AND news_category.name = '".$cat."' AND (news.title LIKE '%$s%' OR news.description LIKE '%$s%') ORDER BY news.created_at DESC");
		} else {
			return $query = $this->db->query("SELECT news.*, news_category.name AS news_category_name, news_category.description AS news_category_description, news_category.link AS news_category_link FROM news JOIN news_category ON news_category.id = news.news_category_id WHERE news.is_active = 1 AND news.is_admit = 1 AND start_date <= NOW() AND finish_date >= NOW() AND year(news.created_at) = $year AND news_category.name = '".$cat."' ORDER BY news.created_at DESC");
		}
	}

	function news_detail($link_post){
		return $query = $this->db->query("
			SELECT news.*, news_category.name AS news_category_name FROM news
			JOIN news_category ON news_category.id = news.news_category_id
			WHERE news.is_active = 1 AND news.is_admit = 1 AND start_date <= NOW() AND finish_date >= NOW() AND news.link_post = '".$link_post."'");
	}
}
