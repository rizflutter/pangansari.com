<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_slider_home extends CI_Model
{
    var $table = 'slider_home_user';
    var $column_order = array('business_category_description','sort','title1', 'description', 'start_date', 'finish_date', 'full_name', null, 'is_active', 'is_admit',null); //set column field database for datatable orderable
    var $column_search = array('business_category_description','sort','title1', 'description'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('id' => 'asc'); // default order 

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query()
    {

        $this->db->from($this->table);

        $i = 0;

        foreach ($this->column_search as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function get_all($table)
    {
        return $this->db->get($table)->result();
    }

    public function get_by_id($id)
    {
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $query = $this->db->get();

        return $query->row();
    }

    public function save($data)
    {
        $this->db->insert('slider_home', $data);
        return $this->db->insert_id();
    }

    public function update($where, $data)
    {
        $this->db->update('slider_home', $data, $where);
        return $this->db->affected_rows();
    }

    public function delete_by_id($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('slider_home');
    }

    public function get_business_category_by_id($id)
    {
        return $this->db->query("SELECT sh.sort, sh.img 
		FROM slider_home sh JOIN business_category bc
		ON sh.business_category_id = bc.id
		WHERE bc.id = $id AND sh.is_active = 1 AND sh.is_admit = 1")->result();
    }

    public function upload()
    {
        // konfigurasi
        $config['upload_path']          = FCPATH . 'assets/images/slide/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 2000;
        $config['max_width']            = 1335;
        $config['max_height']           = 557;

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('img')) {
            $return = array(
                'result' => 'success',
                'file' => $this->upload->data(),
                'error' => ''
            );

            return $return;
        } else {
            // Jika gagal :
            $return = array(
                'result' => 'failed',
                'file' => '',
                'error' => $this->upload->display_errors()
            );
            return $return;
        }
    }
}
