<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_home extends CI_Model
{

	function slider_home()
	{
		return $query = $this->db->query("
			SELECT slider_home.*, business_category.name as business_category_name, business_category.description as business_category_description, business_category.link as business_category_link FROM slider_home
			JOIN business_category ON business_category.id = slider_home.business_category_id
			WHERE is_active = 1 AND is_admit = 1
			AND start_date <= NOW() AND finish_date >= NOW()
			ORDER BY business_category_id ASC, sort ASC");
		// old : CURDATE()
	}

	function slider_product()
	{
		return $query = $this->db->query("
			SELECT slider_product.*, business_category.name as business_category_name, business_category.description as business_category_description, business_category.link as business_category_link FROM slider_product
			JOIN business_category ON business_category.id = slider_product.business_category_id
			WHERE is_active = 1 AND is_admit = 1
			AND start_date <= NOW() AND finish_date >= NOW()
			ORDER BY business_category_id ASC, sort ASC");
		// old : CURDATE()
	}

	function business_category($id = null)
	{
		if ($id != null) {
			$this->db->from("business_category");
			$this->db->where('id', $id);
			$query = $this->db->get();

			return $query->row();
		} else {
			return $query = $this->db->query("SELECT * FROM business_category");
		}
	}

	function business_category_count()
	{
		return $query = $this->db->count_all('business_category');
	}

	function about_us()
	{
		return $query = $this->db->query("
		SELECT title1, title2, title3, title4, description
		FROM about_us WHERE is_active = 1 AND is_admit = 1 AND is_posting = 1");
	}

	function data_slide_to($business_category_id)
	{
		return $query = $this->db->query("
		SELECT COUNT(id) AS sort FROM slider_home 
		WHERE business_category_id =  $business_category_id
		AND is_active = 1 
		AND is_admit = 1
		AND start_date <= NOW() 
		AND finish_date >= NOW()");
	}

	function latest_news($cat){
		return $query = $this->db->query("
		SELECT news.*, news_category.name AS news_category_name, news_category.description AS news_category_description,
		news_category.link AS news_category_link FROM news
		JOIN news_category ON news_category.id = news.news_category_id
		WHERE news.is_active = 1 AND news.is_admit = 1 AND start_date <= NOW() AND finish_date >= NOW() AND news_category.name = '".$cat."' ORDER BY news.id DESC LIMIT 4");
	}
}
