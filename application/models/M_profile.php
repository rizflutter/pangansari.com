<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_profile extends CI_Model
{

    public function update($where, $data)
    {
        $this->db->update('users', $data, $where);
        return $this->db->affected_rows();
    }

    public function get_by_id($id)
    {
        $this->db->from('users');
        $this->db->where('id', $id);
        $query = $this->db->get();

        return $query->row();
    }

    public function upload()
    {
        // konfigurasi
        $config['upload_path']          = FCPATH . 'assets/images/profiles/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 2000;
        $config['max_width']            = 500;
        $config['max_height']           = 500;
        $config['encrypt_name']         = true;

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('img')) {
            $return = array(
                'result' => 'success',
                'file' => $this->upload->data(),
                'error' => ''
            );

            return $return;
        } else {
            // Jika gagal :
            $return = array(
                'result' => 'failed',
                'file' => '',
                'error' => $this->upload->display_errors()
            );
            return $return;
        }
    }
}
