<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ads extends CI_Model {

	function parallax1(){
		return $query = $this->db->query("SELECT * FROM parallax WHERE `index` = 1 AND is_active = 1 AND is_admit = 1 AND start_date <= CURDATE() AND finish_date >= CURDATE() ORDER BY RAND() LIMIT 1");
	}

	function parallax2(){
		return $query = $this->db->query("SELECT * FROM parallax WHERE `index` = 2 AND is_active = 1 AND is_admit = 1 AND start_date <= CURDATE() AND finish_date >= CURDATE() ORDER BY RAND() LIMIT 1");
	}

	function parallax3(){
		return $query = $this->db->query("SELECT * FROM parallax WHERE `index` = 3 AND is_active = 1 AND is_admit = 1 AND start_date <= CURDATE() AND finish_date >= CURDATE() ORDER BY RAND() LIMIT 1");
	}

	function parallax4(){
		return $query = $this->db->query("SELECT * FROM parallax WHERE `index` = 4 AND is_active = 1 AND is_admit = 1 AND start_date <= CURDATE() AND finish_date >= CURDATE() ORDER BY RAND() LIMIT 1");
	}
}
