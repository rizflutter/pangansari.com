<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_news_category extends CI_Model
{
    var $table = 'news_category_user';
    var $column_order = array('sort', 'name', 'description', null, null, null, 'post_by', 'is_active', 'is_admit', 'created_at',null); //set column field database for datatable orderable
    var $column_search = array('name', 'description', 'created_at'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('id' => 'desc'); // default order 

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query()
    {

        $this->db->from($this->table);

        $i = 0;

        foreach ($this->column_search as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function get_all($table)
    {
        return $this->db->get($table)->result();
    }

    public function get_by_id($id)
    {
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $query = $this->db->get();

        return $query->row();
    }

    public function save($data)
    {
        $this->db->insert('news_category', $data);
        return $this->db->insert_id();
    }

    public function update($where, $data)
    {
        $this->db->update('news_category', $data, $where);
        return $this->db->affected_rows();
    }

    public function delete_by_id($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('news_category');
    }

    private function _config_upload()
    {
        // konfigurasi
        $config['upload_path']          = FCPATH . 'assets/images/news/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 2000;
        $config['max_width']            = 500;
        $config['max_height']           = 500;

        return $config;
    }

    public function upload_active()
    {
        $config = $this->_config_upload();
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('img_active')) {
            $return = array(
                'result' => 'success',
                'file' => $this->upload->data(),
                'error' => ''
            );

            return $return;
        } else {
            // Jika gagal :
            $return = array(
                'result' => 'failed',
                'file' => '',
                'error' => $this->upload->display_errors()
            );
            return $return;
        }
    }

    public function upload_nonactive()
    {
        $config = $this->_config_upload();
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('img_nonactive')) {
            $return = array(
                'result' => 'success',
                'file' => $this->upload->data(),
                'error' => ''
            );

            return $return;
        } else {
            // Jika gagal :
            $return = array(
                'result' => 'failed',
                'file' => '',
                'error' => $this->upload->display_errors()
            );
            return $return;
        }
    }
}
