<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_career extends CI_Model {

	function career($from, $per_page, $s = FALSE){
		if($s == TRUE){
			return $query = $this->db->query("SELECT * FROM career WHERE is_active = 1 AND is_admit = 1 AND start_date <= CURDATE() AND finish_date >= CURDATE() AND (career.title LIKE '%$s%' OR career.description LIKE '%$s%') ORDER BY career.created_at DESC LIMIT $from, $per_page");
		} else {
			return $query = $this->db->query("SELECT * FROM career WHERE is_active = 1 AND is_admit = 1 AND start_date <= CURDATE() AND finish_date >= CURDATE() ORDER BY career.created_at DESC LIMIT $from, $per_page");
		}
	}

	function career_all($s = FALSE){
		if($s == TRUE){
			return $query = $this->db->query("SELECT * FROM career WHERE is_active = 1 AND is_admit = 1 AND start_date <= CURDATE() AND finish_date >= CURDATE() AND (career.title LIKE '%$s%' OR career.description LIKE '%$s%') ORDER BY career.created_at DESC");
		} else {
			return $query = $this->db->query("SELECT * FROM career WHERE is_active = 1 AND is_admit = 1 AND start_date <= CURDATE() AND finish_date >= CURDATE() ORDER BY career.created_at DESC");
		}
	}
}
